\documentclass[12pt]{article}

\usepackage{amsmath, amssymb, amsthm}
\usepackage{stmaryrd}
\usepackage{tex/iris}
\usepackage{mathpartir}
\usepackage{todonotes}
\usepackage{semantic}

\title{Expected Cost Analysis in Iris}
\date{}

\newcommand{\highlight}[1]{\colorbox{yellow}{$\displaystyle #1$}}
\newcommand{\pota}{p}
\newcommand{\potb}{\mathfrak{P}}
\newcommand{\pot}{p}
\newcommand{\cost}{c}
\newcommand{\expecCost}{expecCost}
%\newcommand{\wpP}[][3]{\$#1~ \wpre{#2}{#3} }
\newcommand{\supp}[1]{supp(#1)}
\newcommand{\distr}{\mu}
\newcommand{\distrf}{\kappa}
\newcommand{\bind}[2]{bind(#1, #2)}
\newcommand{\dirac}[1]{\delta_{#1}}
\newcommand{\ChooseUniform}[1]{ChooseUniform~ #1}
\newcommand{\ChooseWeighted}[1]{ChooseWeighted~#1}
\newcommand{\Tick}[1]{Tick~#1}


\NewDocumentCommand\wpP{m O{} m O{} m}%
{\$#1~ \textlog{wp}^{#2}_{#4}\spac#3\spac{\left\{#5\right\}}}


\newtheorem{lemma}{Lemma}
\newtheorem{thm}{Theorem}

\begin{document}
	\maketitle	
	\section{Probabilistic Language}
	Given a primitive reduction relation:
	\begin{align*}
	(-, - \rightarrow_t -) \subseteq (Expr \times State) \times \mathbb{D}(Expr \times State \times \mathbb{Q} \times List(Expr))
	\end{align*}
	
	We define threadpool reduction:
	\begin{align*}
		(-, -, - \rightarrow_{tp} -) \subseteq (Expr \times State) \times \mathbb{D}(Expr \times State \times \mathbb{Q} \times List(Expr))
	\end{align*}
	\begin{align*}
	\infer{e, \sigma \rightarrow_t \distr}{\tpool \dplus [e] \dplus \tpool', \sigma, c \rightarrow_{tp} map(\distr, \lambda (e', \sigma', c', \vec{e}).~(\tpool \dplus [e'] \dplus \tpool' \dplus \vec e), \sigma', c + c'))}
	\end{align*}
	
	
	and compose it for multiple steps, adding up the cost:
	\begin{align*}
		(-, -, - \rightarrow_{tp}^n -) \subseteq (Expr \times State) \times \mathbb{D}(Expr \times State \times \mathbb{Q} \times List(Expr))
	\end{align*}
	\begin{align*}
	\infer{}{\expr, \state, \cost \rightarrow_{tp}^n \dirac{(\expr, \state, \cost, [])}}
	\end{align*}
	\begin{align*}
	\infer{\expr \rightarrow_t \distr \qquad \forall (T, \sigma', \cost) \in \supp{\distr}~ T, \sigma, \cost \tpstep^{n-1} \distrf((T, \sigma', \cost))}{\expr, \sigma' \tpstep^n \bind{\distr}{\distrf}}
	\end{align*}

	
	\section{Probabilistic Weakest Precondition}
	
	Compared to the current Iris version, we drop support for prophecy and later credits.

	
	\begin{align*}
		\textdom{wp}(\stateinterp, \pred_F, \stuckness) \eqdef{}& \MU \textdom{wp\any rec}. \Lam \mask, \expr, \pred, \highlight{\pota, \potb}. \\
		& (\Exists\val. \toval(\expr) = \val \land \pvs[\mask] \pred(\val) \wedge \highlight{\pota \geq \potb(v))} \lor {}\\ 
		& \Bigl(\toval(\expr) = \bot \land \All \state, n_s, n_t. \stateinterp(\state, n_s, n_t)  \vsW[\mask][\emptyset] {} * \\ 
		&(s = \NotStuck \Ra \red(\expr, \state)) * p \geq 0 * \highlight{\All \distr. (\expr, \state, \step \distr)} \wand \\
		&(\pvs[\emptyset]\later\pvs[\emptyset])^{n_\rhd(n_s)+1} \highlight{\exists \vec{\pota}.~\mathbb{E}[Cost(\distr)] + \mathbb{E}_L[\vec{\pota}] \leq \pota} * \\ 
		&\highlight{\bigwedge\limits_{p'; (\expr', \state', \vec{\expr}, \_) \in \vec{p}; \supp \distr}
			\pvs[\emptyset][\mask]\exists \pota', \vec{pt}.~ \pota'' + \sum \vec{pt} \leq \pota' ~ \wedge } \\
		&\qquad   \stateinterp( n_s + 1, \state', n + |\vec{\expr}|) * \textdom{wp\any rec}(\mask, \expr', \pred, \highlight{\pota'', \potb}) * {}\\
		&\qquad \Sep_{\expr''; \pota''' \in \vec\expr; \vec{pt}} \textdom{wp\any rec}(\top, \expr'', \pred_F, \highlight{\pota''', 0})\Bigr) \\
		\wpP{\pota}[\stateinterp;\pred_F]{\expr}[\stuckness;\top]{\pred, \potb} \eqdef& ~\textdom{wp}(\stateinterp, \pred_F, \stuckness)~\expr~\pred~\pota~\potb \\
		\wpre[\stateinterp;\pred_F]{\expr}[\stuckness;\top]{\pred} \eqdef&
		 ~\wpP {0}[\stateinterp;\pred_F]{\expr}[\stuckness;top]{\pred, \mathbf{0}}
	\end{align*}
	
	\section{Adequacy}
	
	\newcommand\metaprop{\varphi}
	\newcommand\consstate{C}
	
	
	The adequacy theorem consists of three main parts: proving a weakest precondition shows that:
	\begin{enumerate}
		\item the postcondition holds for all threads that have terminated in values
		\item progress: the program does not get stuck
		\item the expected sum of cost and postcondition potential does not exceed the initial sum of potential and cost. This also implies that the expected cost is bounded by the sum of initial potential and initial cost.
	\end{enumerate}

	All three proofs proceed by induction over $\expr, \state, \cost \tpstep^n \distr$.
	For (3), the induction step follows from a variant of the law of total expectation:
	
	\begin{lemma}
		\begin{align*}
		\left(\forall (\pota', (\expr, \state, \cost)) \in zip(\vec{\pota}, \supp \distr).~ \mathbb{E}[Cost(\distrf (\expr, \state, \cost) + \vec{\potb})] \leq p' + c\right) \rightarrow  \\
		\mathbb{E}[Cost(bind(\distr, \distrf) + \vec{\potb})] \leq \mathbb{E}[Cost(\distr)] + \mathbb{E}_L[\vec{p}]
		\end{align*}
	\end{lemma}
	
	%\begin{lemma}[Postconditions]
	%	\begin{align*}
	%		&\vec{\expr}, \state, \cost \rightarrow_{tp}^n \distr \rightarrow  \\
	%		&S(\state, n_s, n_t) \wand \sum \vec{p} \leq p * \\
	%		\Sep_{\expr';}
	%	\end{align*}
	%\end{lemma}
	
	The three parts are, together with the soundness result of the Iris' base logic, composed into a full adequacy theorem.
	
	\begin{thm}[Adequacy]
		Assume we are given some $\vec\expr$, $\state$, $\cost$, $n$, $\distr$ such that $(\vec\expr, \state, \cost) \tpstep^n \distr$.
		Moreover, assume we are given , a stuckness parameter $\stuckness$ and \textit{meta-level} property $\metaprop$ that we want to show.
		To verify that $\metaprop$ holds, it is sufficient to show that there exists lists $\vec{\pota}, \vec{\potb}$ and $(\tpool_2, \state_2, \cost_2) \in \supp \distr$ such that the following Iris entailment holds:
		\begin{align*}
			&\proves \pvs[\top] \Exists \stateinterp, \vec\pred, \pred_F. \stateinterp(\state_1,0,0) * \left(\Sep_{\expr;\pred;\pota;\potb \in \vec\expr,\vec\pred;\vec{\pot},\vec{\potb}} \wpP{\pota}[\stateinterp;\pred_F]{\expr}[\stuckness;\top]{\pred, \potb}\right) * \left(\consstate^{\stateinterp;\vec\pred;\pred_F}_{\stuckness;\vec{\pota};\vec{\potb};c}(\distr, \tpool_2, \state_2) \vs[\top][\emptyset] \hat{\metaprop}\right)
		\end{align*}
		where 
		\begin{align*}
			\consstate^{\stateinterp;\vec\pred;\pred_F}_{\stuckness;\vec{\pota};\vec{\potb};c}(\distr, \tpool_2, \state_2) \eqdef{}&\Exists \vec\expr_2, \tpool_2'. \tpool_2 = \vec\expr_2 \dplus \tpool_2' * {}\\
			&\quad |\vec\expr_1| = |\vec\expr_2| *{}\\
			&\quad (s = \NotStuck \Ra \All \expr \in \tpool_2. \toval(\expr) \neq \bot \lor \red(\expr, \state_2) ) *{}\\
			&\quad\exists n', n' \leq n.~ \stateinterp(\state_2, n', |\tpool_2'|) *{}\\
			&\quad \mathbb{E}[Cost(\distr + \vec{\potb})] \leq \cost + \sum \vec{\pota} ~ *\\
			&\quad \left(\Sep_{\expr,\pred \in \vec\expr_2,\vec\pred} \toval(\expr) \ne \bot \wand \pred(\toval(\expr))\right) *{}\\
			&\quad \left(\Sep_{\expr \in \tpool_2'} \toval(\expr) \ne \bot \wand \pred_F(\toval(\expr))\right)
		\end{align*}
		The $\hat\metaprop$ here arises because we need a way to talk about $\metaprop$ inside Iris.
		To this end, we assume that the signature $\Sig$ contains some assertion $\hat{\metaprop}$:
		\[ \hat{\metaprop} : \Prop \in \SigFn \]
		Furthermore, we assume that the \textit{interpretation} $\Sem{\hat{\metaprop}}$ of $\hat{\metaprop}$ reflects $\metaprop$ :
		\[\begin{array}{rcl}
			\Sem{\hat{\metaprop}} &:& \Sem\Prop \\
			\Sem{\hat{\metaprop}} &\eqdef& \Lam \any. \setComp{n}{\metaprop}
		\end{array}\]
		The signature can of course state arbitrary additional properties of $\hat{\metaprop}$, as long as they are proven sound.
	\end{thm}
	
	
	\section{Reasoning Rules} 
	
	\subsection{Iris}
	
	
	\begin{align*}
	\infer[wp-bind]
	{\wpP \pota \expr {v.~\wpP {\potb'(v)} {K[v]} {\pred, \potb}, \potb' v}}
	{\wpP \pota {K[e]} {\pred, \potb}} \qquad
	\infer[wp-value]
	{\potb~ v \leq \pota \qquad \pred~ v}{\wpP \pota v {\pred, \potb}}
	\end{align*}
	\begin{align*}
	\infer[wp-add-pot]
	{\pota' \geq 0 \qquad \wpP \pota \expr {\pred, \potb}}
	{\wpP {(\pota + \pota')} \expr {\pred, \lambda v.~ \potb v + \pota'}} \qquad
	\infer[upd-wp] 
	{\pvs \wpP p \expr {\pred, \potb} }
	{\wpP p \expr {\pred, \potb}}
	\end{align*}
	\begin{align*}
	\infer[wp-mono]
	{\pota \leq \pota' \qquad \forall v.~ \potb v \geq \potb' v \qquad \forall v.~ \pred v \wand \Psi v \qquad \wpP \pota \expr {\pred, \potb}}
	{\wpP {\pota'} \expr {\Psi, \potb'}}
	\end{align*}

	\subsection{Instatiation: Probabilistic Heap Lang}

	\begin{align*}
		\infer[wp-choose-uniform]
		{l \neq [] \qquad \forall v \in l.~ \pred~ v \qquad \sum (map~ \potb~ l) \leq \pota \cdot length~ l }
		{\wpP {\pota} {\ChooseUniform{l}} {\pred, \potb}} \qquad
		\infer[wp-tick]
		{c \geq 0 \qquad \later \pred~ ()}
		{\wpP c {\Tick c} {\pred, \mathbf{0}}}
	\end{align*}
	\begin{align*}
		\infer[wp-choose-weighted]
		{l \neq [] \qquad \forall v \in l, \exists r, v'.~ v = (r, v') * r > 0 * \Phi~ v' \qquad \frac{\sum map (\lambda~ (r, v).~ r \cdot \potb~ v)~ l}{\sum map~ fst~ l} \leq p}
		{\wpP \pota {\ChooseWeighted l} {\Phi , \potb}} 
	\end{align*} \todo{make type coersion explicit?}

	\begin{align*}
		\infer[wp-fork]
		{p \geq 0 \qquad \later \pred~ () \qquad \later \wpP p e {\TRUE, \mathbf{0}}}
		{\wpP p {Fork~ e} {\pred, \mathbf{0}}}
	\end{align*}

	Furthermore, all rules for deterministic primitives (except for the prophecy) stay valid and can be generalized to an arbitrary nonnegative potential using the rule WP-ADD-POT.

	
\end{document}