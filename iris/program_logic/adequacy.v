From iris.algebra Require Import gmap auth agree gset coPset.
From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import wsat.
From iris.program_logic Require Export weakestpre.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.
Import uPred.
Require Import Lra.
Require Import Setoid.

(** This file contains the adequacy statements of the Iris program logic. First
we prove a number of auxilary results. *)

Section adequacy.
Context `{!irisGS_gen Λ Σ}.
Implicit Types e : expr Λ.
Implicit Types P Q : iProp Σ.
Implicit Types Φ : val Λ → iProp Σ.
Implicit Types p : R. 
Implicit Types pl : list R. 
Implicit Types vp : val Λ → R.
Implicit Types Φs : list ((val Λ → iProp Σ) * (val Λ → R)).

Definition wptp s t p Φs := (∃ pl,
 ⌜(R_list_sum pl <= p)%R ∧ length pl = length Φs ⌝ ∗
  [∗ list] e;pΦ ∈ t;zip pl Φs, $(pΦ.1) WP e @ s; ⊤ {{ pΦ.2.1 , pΦ.2.2 }})%I.

Lemma wptp_length s t p Φs : wptp s t p Φs -∗ ⌜ length t = length Φs ⌝.
Proof.
  unfold wptp. iIntros "(%pl & [_ %Hlen] & Hefs)". 
  iDestruct (big_sepL2_length with "Hefs") as "%Hlen'".
  iPureIntro. by rewrite zip_with_length_r_eq in Hlen'.
Qed.

Lemma wptp_singleton s e p Φ vp : $p WP e @ s; ⊤ {{ Φ , vp }} -∗  wptp s [e] p [(Φ, vp)].
Proof.
  iIntros "Hwp". iExists [p]. iSplit; simpl; iFrame.
  iPureIntro. split; auto. lra.
Qed.

Local Lemma wp_step s e1 σ1 ns D nt Φ p vp :
  prim_step e1 σ1 D →
  state_interp σ1 ns nt -∗
  $p WP e1 @ s; ⊤ {{ Φ, vp }}
    ={⊤,∅}=∗ ⌜ (p>=0)%R ⌝  ∗ |={∅}▷=>^(S $ num_laters_per_step ns)
    ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∧ 
    [∧ list] i ↦ d; p' ∈ (domD D); pl,
    ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
    ∃ p1 p2, ⌜(p1 + p2 <= p')%R ⌝  ∗
    |={∅,⊤}=>state_interp σ2 (S ns) (nt + length efs) ∗ $p1 WP e2 @ s; ⊤ {{ Φ, vp }} ∗
    wptp s efs p2 (replicate (length efs) (fork_post, λ _ : val Λ , R0)).
Proof.
  rewrite {1}wp_unfold /wp_pre. iIntros (?) "Hσ H".
  rewrite (val_stuck e1 σ1 D) //.
  iMod ("H" $! σ1 ns with "Hσ") as "(% & _ & H)". iModIntro.
  iDestruct ("H" $! D with "[//]") as "H".
  iSplitR; auto.
  iApply (step_fupdN_wand with "H ").
  iIntros "(%pl & % & H)". iExists pl. iSplitR; auto. 
  iApply (big_andL2_impl); iFrame.
  iIntros (k d p' Hdom Hpl') "H" . iIntros (e2 σ2 p'' efs ->).
  iDestruct ("H" $! e2 σ2 p'' efs with "[//]") as "(%p''' & %pl' & % & H)".
  iExists p''', (R_list_sum pl'). iSplitR; auto.
  iMod "H" as "(Hσ & Hwp & Hefs)". iModIntro.
  rewrite Nat.add_comm. iFrame. iExists pl'.
  iDestruct (big_sepL2_length with "Hefs") as "%Hlen". iSplitR.
    - iPureIntro. rewrite replicate_length. split; auto; lra.
    - rewrite Hlen. rewrite zip_with_replicate_r; last done. iApply big_sepL2_fmap_r.
      done.
Qed.

Local Lemma wptp_step s es1 σ1 c D ns Φs nt p:
  step (es1,σ1,c) D →
  state_interp σ1 ns nt -∗
  wptp s es1 p Φs -∗ 
        |={⊤,∅}=> |={∅}▷=>^(S $ num_laters_per_step$ ns)
        ∃ pl, ⌜(expec_cost2 D + expecL D pl <= p + c)%R ⌝ ∧ 
        [∧ list] i ↦ d; p' ∈ (domD D); pl,
        ∀ es2 σ2 c2, ⌜d = (es2, σ2, c2)⌝  -∗ 
        |={∅,⊤}=> ∃ nt', state_interp σ2 (S ns) (nt + nt') ∗
        wptp s es2 p' (Φs ++ replicate nt' (fork_post, λ _, R0)).
Proof.
  iIntros (Hstep) "Hσ [%pl_threads [%H Ht]]".
  destruct Hstep as [e1' σ1' D' t2' t3 c' Hstep HD]. simplify_eq/=.
  iDestruct (big_sepL2_app_inv_l with "Ht") as (pΦs1 pΦs2 Hzip) "[Hpl1 Ht]".
  iDestruct (big_sepL2_cons_inv_l with "Ht") as ([p' [Φ vp]] pΦs3 ->) "[Ht Hpl3]".
  simpl. iMod (wp_step with "Hσ Ht") as "[% H]"; first done. 
  iIntros "!>". iApply (step_fupdN_wand with "H").
  iIntros "(%pl_distr & % & H)".
  iExists (map (λ x, (x +  (p-p'))%R ) pl_distr).
  iDestruct (big_andL2_length with "H") as "%Hlen".
  apply zip_with_app_inv in Hzip.
  destruct Hzip as (pl1&Φs1&pl2&Φs2&Hzip1&Hzip2&->&->&Hlen').
  symmetry in Hzip2. apply zip_with_cons_inv in Hzip2.
  destruct Hzip2 as (?&?&pl3&Φs3&H2&->&->&->). symmetry in H2.
  simplify_eq. destruct H as [Hsum Hlen3].
  rewrite !app_length in Hlen3. rewrite Hlen' in Hlen3.
  apply Plus.plus_reg_l_stt in Hlen3. simpl in Hlen3.
  injection Hlen3 as Hlen3.
  iSplitR.
    - iPureIntro. rewrite <- expecL_map. rewrite expecL_add; auto.
      rewrite expec_cost2_add.  lra. 
    - rewrite distr_map_dom. rewrite !big_andL2_fmap_l.
      rewrite big_andL2_fmap_r.
      iApply big_andL2_impl. iFrame. iIntros (k [? [[[? ?] ?] ?]] ? Hlook1 Hlook2) "H".
      iIntros (es2 σ2 c). simpl. iIntros. simplify_eq. 
      iDestruct ("H" with "[//]") as "(%p1 & %p2 & %Hsum' & >H)". 
      iModIntro. iExists (length l). iFrame.
      iDestruct "H" as "(Hσ & Hwp & Hefs)" .
      iDestruct "Hefs" as (pl2 [Hsum'' Hlen'']) "Hefs". iFrame. 
      iExists (pl1 ++ p1::pl3 ++ pl2). iSplitR.
        + iPureIntro. split.
          * rewrite R_list_sum_app. simpl. rewrite R_list_sum_app.
            rewrite R_list_sum_app in Hsum. simpl in Hsum. lra.
          * rewrite !app_length. simpl. rewrite app_length. lia.
        + rewrite -(assoc_L app). rewrite zip_with_app; auto.
          rewrite -app_comm_cons. simpl. rewrite zip_with_app; first last; auto.
          iApply (big_sepL2_app with "Hpl1"). iApply big_sepL2_cons. simpl. iFrame.
Qed.

(* The total number of laters used between the physical steps number
   [start] (included) to [start+ns] (excluded). *)
Local Fixpoint steps_sum (num_laters_per_step : nat → nat) (start ns : nat) : nat :=
  match ns with
  | O => 0
  | S ns =>
    S $ num_laters_per_step start + steps_sum num_laters_per_step (S start) ns
  end.

(* TODO Not the right place *)
Lemma lookup_lt_length {A B} (l1 : list A) (l2 : list B) k: length l1 ≤ length l2 -> is_Some (l1 !! k) -> is_Some (l2 !! k).
Proof.
  intros. apply lookup_lt_is_Some_2. enough (k < length l1) by lia.
  by apply lookup_lt_is_Some.
Qed.

Local Lemma elem_concat {A} l (a : A): a ∈ concat l → ∃ l', l' ∈ l ∧ a ∈ l'.
Proof.
  induction l as [|a' l IHl]; simpl; first set_solver. intros. destruct (@elem_of_app A a' (concat l) a) as [[] _]; auto.
    - exists a'. split; auto. set_solver.
    - destruct IHl as [l' Hl']; auto. exists l'. set_solver.
Qed.
(*--*)

Fixpoint good_steps n ρ D p (s:stuckness) Φs (ns nt: nat) (Hstep : @nsteps Λ n ρ D) : iProp Σ.
Proof using irisGS_gen0 Λ Σ.
  destruct Hstep.
    - exact (|={∅,⊤}=> state_interp ρ.1.2 ns nt ∗ wptp s ρ.1.1 p Φs)%I.
    - exact ( ∃ (pl : list R),  ⌜length pl = length (domD Dρ2)⌝ ∗ 
      ⌜(expec_cost2 Dρ2 + expecL Dρ2 pl <= p + ρ1.2)%R ⌝ ∗ 
      ∀ (k : nat) (ρ2 : cfg Λ) (p' : R) (Hdom : ρ2 ∈ (domD Dρ2)), 
      ⌜ (domD Dρ2) !! k = Some ρ2 ⌝ -∗ ⌜ pl !! k = Some p' ⌝ -∗ 
      |={∅}=> ∃ nt', |={∅}▷=>^(S $ num_laters_per_step$ (S ns)) 
      good_steps n ρ2 (f ρ2) p' s (Φs ++ replicate nt' (fork_post, λ _ : val Λ, R0)) (S ns) (nt + nt') (n0 ρ2 Hdom))%I.
Defined.

Local Lemma wptp_good_steps s n ρ D ns Φs p nt Hstep :
  state_interp ρ.1.2 ns nt -∗
  wptp s ρ.1.1 p Φs -∗ 
  |={⊤,∅}=> |={∅}▷=>^(S $ num_laters_per_step $ ns)
      good_steps n ρ D p s Φs ns nt Hstep.
Proof.
  induction Hstep in ns, nt, Φs, p |- *.
  - iIntros "Hσ Hwp"; simpl. 
  rewrite -step_fupdN_intro; [|done]. rewrite -bi.laterN_intro.
  iApply fupd_mask_intro; first set_solver.
  iIntros "H". iIntros "!> !>". iFrame. done.
  - simpl. destruct ρ1 as [[es2 σ2] c2]. simpl.
  iIntros "Hσ Hwp". simpl.
  iDestruct (wptp_step with "Hσ Hwp") as ">H"; first exact s0.
  iModIntro. iApply (step_fupdN_wand with "H").
  iIntros "(%plD1 & %Hexp1 & H)".
  rewrite big_andL2_forall.
  iDestruct "H" as "(%Hlen & H)". 
  iExists plD1. repeat iSplitR; auto. iIntros (k ρ2 p' Hdom Hlook1 Hlook2).
  iSpecialize ("H" with "[//] [//]").
  iDestruct ("H" $! ρ2.1.1 ρ2.1.2 ρ2.2 _) as ">(%nt' & Hσ2 & Hwp)".
  iDestruct (H with "Hσ2 Hwp") as ">H". iModIntro.
  iExists nt'.
  iApply (step_fupdN_wand with "H"). iIntros "?". done.
  Unshelve. by destruct ρ2 as [[]].
  Qed.

Definition pot_post vp e := match to_val e with 
  | Some v => vp v
  | None => 0%R
end.

Definition pot_post_sum Φs_p es := R_list_sum (map (λ d, pot_post d.1 d.2) (zip Φs_p es)).

Lemma Forall2_Forall3_l {A B C} f (l1: list A) (l2: list B) (l3 : list C): 
  Forall3 f l1 l2 l3 → Forall2 (λ x1 x2, f x1.1 x1.2 x2) (zip l1 l2) l3.
Proof.
  induction l1 in l2, l3 |- *; inversion 1; simplify_eq; first done.
  simpl. apply Forall2_cons. split; first done. by apply IHl1.
Qed.

Lemma pot_post_sum_leq Φs_p es pl p:
(R_list_sum pl <= p)%R ∧
Forall3 (λ vp e p', (pot_post vp e <= p')%R) Φs_p es pl  →
(pot_post_sum Φs_p es <= p)%R.
Proof.
  intros [Hsum Hall]. unfold pot_post_sum. eapply Rle_trans; last exact Hsum. apply R_list_sum_mono. rewrite Forall2_fmap_l. apply Forall2_Forall3_l in Hall.
  eapply Forall2_impl; first exact Hall. done.
Qed.

Definition expec_cost_plus_post (D : @distribution (cfg Λ)) Φs_p :=
  expec D (λ d, (d.2 + pot_post_sum Φs_p d.1.1)%R).

Local Lemma expec_mono (D : @distribution (cfg Λ )) Φs_p pl:
(Forall2 (λ p1 p2, (p1 >= p2)%R) pl (map (λ d, pot_post_sum Φs_p d.1.1 )(domD D))) → 
(expecL D pl >= expec D (λ d, pot_post_sum Φs_p d.1.1))%R.
Proof.
  intros H%(expecL_mono _ _ D). by rewrite expecL_expec in H. 
Qed.

Local Lemma wp_pot_post nt e σ ns Φ p vp s:
  state_interp σ ns nt -∗ $p WP e @ s; ⊤ {{Φ , vp}} ={⊤, ∅ }=∗ ⌜ (pot_post vp e <= p)%R ⌝ .
Proof.
  rewrite wp_unfold /wp_pre. iIntros "Hσ H". unfold pot_post.
  destruct (to_val e) as [v|] eqn:?;
  first by iMod "H" as "[% _]". 
  iMod ("H" with "Hσ ") as "[% _]". iPureIntro. lra.
Qed.

Local Lemma Forall3_cons_and {A B C} (Φall : A → B → C → Prop) x1 x2 x3 l1 l2 l3:
  Φall x1 x2 x3 ∧ Forall3 Φall l1 l2 l3 → Forall3 Φall (x1::l1) (x2::l2) (x3::l3).
Proof.
  intros. constructor; tauto.
Qed.

Local Lemma wptp_pot_post s es p Φs ns nt σ :
  state_interp σ ns nt -∗ wptp s es p Φs ={⊤, ∅}=∗ ⌜ (pot_post_sum Φs.*2 es <= p)%R ⌝.
Proof.
  iIntros "Hσ H". unfold wptp. iDestruct "H" as "(%pl & [%Hsum  %Hlen] & Hefs)".
  iMod (fupd_mask_subseteq ∅) as "Hclose"; first set_solver.
  iApply pure_mono; first apply pot_post_sum_leq.
  iSplitR; first done. clear Hsum.
  iInduction es as [|n] "IH" forall (pl Φs Hlen); destruct pl, Φs; try done.
  {iPureIntro. apply Forall3_nil. }
  iApply fupd_mono.
  { iApply pure_mono. apply Forall3_cons_and. }
  iDestruct "Hefs" as "[Hwp Hefs]". simpl.
  setoid_rewrite pure_and.
  iApply fudp_plain_and_r. iSplit.
  - iMod "Hclose". iApply (wp_pot_post with "Hσ Hwp").
  - iApply ("IH" with "[] Hσ Hefs Hclose"). by injection Hlen.
Qed.

Local Lemma wp_not_stuck nt e σ ns Φ p vp:
  state_interp σ ns nt -∗ $p WP e {{ Φ, vp }} ={⊤, ∅}=∗ ⌜not_stuck e σ ⌝.
Proof.
  rewrite wp_unfold /wp_pre /not_stuck. iIntros "Hσ H".
  destruct (to_val e) as [v|] eqn:?.
  { iApply fupd_mask_intro; first set_solver. iIntros. by iLeft. }
  iMod ("H" $! σ ns with "Hσ") as "[_ [% H]]". eauto.
Qed.

(** The adequacy statement consists of three parts:
      (1) the postcondition for all threads that have terminated in values
      (2) progress (i.e., after n steps the program is not stuck).
    For an n-step execution of a thread pool, the two parts are given by
    [wptp_strong_adequacy] and [wptp_progress] below.
      (3) the expected cost is bounded by the initial potential
*)

Local Lemma wptp_postconditions Φs s n es1 σ1 c ns nt D p:
  nsteps n (es1, σ1, c) D →
  state_interp σ1 ns nt -∗
  wptp s es1 p Φs -∗ 
    ∀ es2 σ2 c2, ⌜ (es2, σ2, c2) ∈ (domD D) ⌝
    ={⊤,∅}=∗ |={∅}▷=>^(steps_sum num_laters_per_step ns (S n)) |={∅,⊤}=> 
      ∃ nt' ns', ⌜ ns' <= n ⌝ ∗ 
      state_interp σ2 (ns + ns') (nt + nt') ∗
      [∗ list] e;Φ ∈ es2;Φs.*1 ++ replicate nt' fork_post, from_option Φ True (to_val e).
Proof.
  iIntros (Hstep) "Hσ He". 
  iDestruct ((wptp_good_steps _ _ (es1, σ1, c) D _ _ _ _ Hstep) with "[Hσ] He") as ">Hwp"; first done. iInduction Hstep as [|n] "IH" forall (ns p Φs nt).
  - simpl. iIntros (es2 σ2 c2 Hdom). rewrite domD_single in Hdom.
    apply elem_of_list_singleton in Hdom. rewrite <- Hdom. simpl.
    iModIntro. rewrite Nat.iter_add. iApply (step_fupdN_wand with "Hwp").
    iModIntro. iIntros "H". iApply step_fupdN_intro; first set_solver.
    iApply laterN_intro. iMod "H". iExists 0, 0. simpl.
    rewrite Nat.add_0_r right_id_L. iDestruct "H" as "($  & %pl & [_ %] & H)".
    clear Hdom. iDestruct (big_sepL2_length with "H") as "%Hlen".
    iSplitR; first auto with lia.
    iApply big_sepL2_fupd.
    iInduction Φs as [|Φs] "IH2" forall (pl es2 H Hlen); destruct pl; destruct es2; try by contradict H; simpl. 1:done.
    simpl. iDestruct "H" as "[H1 H2]". iSplitR "H2".
      + destruct (to_val e) as [v2|] eqn:He2'; last done. 
      apply of_to_val in He2' as <-. simpl. iDestruct (wp_value_fupd with "H1") as ">[_ H]"; done.
      + simpl in H, Hlen. injection H. injection Hlen. intros H' Hlen'. iApply "IH2"; auto. 
  - simpl. iIntros (es2 σ2 c2 Hdom). iModIntro.
    rewrite step_fupdN_add. iApply (step_fupdN_wand with "Hwp").
    iIntros "!> (% & %Hlen & _ & H)". rewrite bind_dom in Hdom. 
    destruct (elem_concat _ _ Hdom) as [l' [Hel1 Hel2]].
    apply elem_of_list_fmap in Hel1. destruct Hel1 as [ρ2 [-> Hρ2]].
    apply elem_of_list_lookup in Hρ2 as Hlook. destruct Hlook as [k Hlook].
    destruct (lookup_lt_length (domD Dρ2) pl k) as [p' H]; [by rewrite Hlen | by exists ρ2 |]. simpl.
    iDestruct ("H" $! k ρ2 p' Hρ2 with "[//] [//]") as ">[%nt' H]". 
    iSpecialize ("IH" with "H"). iSpecialize ("IH" $! es2 σ2 c2 with "[//]").
    iMod "IH". iApply (step_fupdN_wand with "IH"). 
    iIntros "!> >[%nt'' [%ns' [%Hn Hefs]]]". iExists (nt' + nt''), (S ns'). 
    rewrite Nat.add_assoc. rewrite replicate_add. rewrite fmap_app.
    rewrite app_assoc. setoid_rewrite fmap_replicate. 
    replace (S ns + ns') with (ns + S ns') by lia. iFrame. auto with lia.
Qed.

Local Lemma wptp_progress Φs n es1 σ1 c D ns nt p es2 σ2 c2 e2:
  nsteps n (es1, σ1, c) D →
  (es2, σ2, c2) ∈ (domD D) → 
  e2 ∈ es2 → 
  state_interp σ1 ns nt -∗
  wptp NotStuck es1 p Φs 
  ={⊤,∅}=∗ |={∅}▷=>^(steps_sum num_laters_per_step ns (S n)) |={∅}=> ⌜not_stuck e2 σ2⌝.
Proof.
  iIntros (Hstep Hdom Hel) "Hσ He". 
  iDestruct ((wptp_good_steps _ _ (es1, σ1, c) D _ _ _ _ Hstep) with "[Hσ] He") as ">Hwp"; first done.
  iInduction Hstep as [|n] "IH" forall (ns p Φs nt es2 σ2 c2 Hdom Hel); iModIntro.
  - rewrite domD_single in Hdom.
    apply elem_of_list_singleton in Hdom. rewrite <- Hdom. simpl.
    rewrite Nat.iter_add. iApply (step_fupdN_wand with "Hwp").
    iIntros "!> H". iApply step_fupdN_intro; first set_solver.
    iApply laterN_intro. iMod "H" as "(Hσ & %pl & [_ %Hlen] & Hefs)". 
    eapply elem_of_list_lookup in Hel as [i Hlook].
    iDestruct (big_sepL2_length with "Hefs") as "%Hlen'".
    destruct (lookup_lt_length es2 (zip pl Φs) i) as [pΦ H]; [by rewrite Hlen'| auto |].
    iDestruct (big_sepL2_lookup with "Hefs") as "Hefs"; [done..|].
    by iApply (wp_not_stuck with "Hσ").
  - simpl. rewrite step_fupdN_add. iApply (step_fupdN_wand with "Hwp").
    iIntros "!> (% & %Hlen & _ & H)". rewrite bind_dom in Hdom.
    destruct (elem_concat _ _ Hdom) as [l' [Hel1 Hel2]].
    apply elem_of_list_fmap in Hel1. destruct Hel1 as [ρ2 [-> Hρ2]].
    apply elem_of_list_lookup in Hρ2 as Hlook. destruct Hlook as [k Hlook].
    destruct (lookup_lt_length (domD Dρ2) pl k) as [p' H]; [by rewrite Hlen | by exists ρ2 |]. simpl.
    iDestruct ("H" $! k ρ2 p' Hρ2 with "[//] [//]") as ">[%nt' H]". 
    iSpecialize ("IH" with "[//] [//] H"). by iMod ("IH").
Qed.

Lemma post_zero D Φs_p n Φ : expec_cost_plus_post D Φs_p = expec_cost_plus_post D (Φs_p ++ replicate n (λ _, R0)).
Proof.
  unfold expec_cost_plus_post. rewrite !expec_lin. f_equal.
  unfold pot_post_sum. apply expec_ext. intros [[es σ] c].
  induction Φs_p in es |-* ; simpl.
  - rewrite (R_list_sum_same 0); first lra. rewrite Forall_map. induction es in n |- *;
    simpl; first by rewrite zip_with_nil_r.
    destruct n; simpl; first done. rewrite Forall_cons. simpl. split; auto.
    unfold pot_post. destruct (to_val a); auto.
  - destruct es; simpl; auto. f_equal. apply IHΦs_p.
Qed.


Lemma law_total_expectation (D : @distribution (cfg Λ) ) f Φs_p pl:
Forall2 (λ p' d, (expec_cost_plus_post (f d) Φs_p <= p' + d.2)%R) pl (domD D) → 
(expec_cost_plus_post (bind D f) Φs_p <= expec_cost2 D + expecL D pl)%R.
Proof.
  unfold expec_cost_plus_post. rewrite expec_lin. unfold expec_cost2. unfold expecL.
  unfold expec. unfold domD. destruct D as [di g0 sum1]. simpl. clear sum1. apply decide_geq0_1 in g0.
  intros Hall. apply Forall2_length in Hall as Hlen. rewrite map_length in Hlen.
  induction di in g0, pl, Hall, Hlen |- *; simpl; first by lra.
  destruct pl; try done.
  repeat rewrite map_app. repeat rewrite map_map. simpl. repeat rewrite R_list_sum_app.
  simpl in Hall. apply Forall2_cons in Hall as [Hsum Hall]. injection Hlen as Hlen.
  apply Forall_cons in g0 as [Ha g0]. specialize (IHdi g0 pl Hall Hlen). destruct a as [r' d]. simpl.
  erewrite map_ext; first last.
    { intros. rewrite Rmult_comm. rewrite -Rmult_assoc. setoid_rewrite Rmult_comm  at 2. reflexivity. }
  rewrite R_list_sum_mult. simpl in *.
  setoid_rewrite map_ext at 4; first last.
  { intros. rewrite Rmult_comm. rewrite -Rmult_assoc. setoid_rewrite Rmult_comm at 2. reflexivity. }
  rewrite R_list_sum_mult. simpl in *.
  erewrite map_ext in Hsum; first last.
  {intros. rewrite Rmult_plus_distr_l. reflexivity. }
  rewrite R_list_sum_add in Hsum. nra.
Qed.


Local Lemma wptp_cost Φs n s ρ D ns nt p:
  nsteps n ρ D →
  state_interp ρ.1.2  ns nt -∗
  wptp s ρ.1.1 p Φs
  ={⊤,∅}=∗ |={∅}▷=>^(S$ steps_sum num_laters_per_step ns (S n)) |={∅}=>
  ⌜ (expec_cost_plus_post D Φs.*2 <= p + ρ.2 )%R ⌝.
Proof.
 iIntros (Hstep) "Hσ Hwp".
  iDestruct ((wptp_good_steps _ _ ρ  D _ _ _ _ Hstep) with "[Hσ] [Hwp]") as ">Hwp"; try by simplify_eq.
  simpl. rewrite Nat.iter_add. iModIntro. iApply (step_fupdN_wand with "Hwp").
  iIntros "!> !> !> !> H". iInduction Hstep as [|n] "IH" forall (ns p nt Φs); simpl.
  - iApply step_fupdN_intro; first set_solver.
  iModIntro. iMod "H" as "[Hσ Hwp]". unfold expec_cost_plus_post.
  rewrite init_single_expec. simplify_eq. simpl. 
  iMod (wptp_pot_post with "Hσ Hwp") as "%H". iPureIntro. lra. 
  - iDestruct "H" as "(%pl & %Hlen & %Hexpec & H)". simplify_eq. simpl in *. 
  iAssert (|={∅}▷=>^(S $ num_laters_per_step (S ns) + steps_sum num_laters_per_step (S (S ns)) n)
    ∀ k p' d, ⌜ pl !! k = Some p' ⌝  → ⌜ (domD Dρ2) !! k = Some d ⌝ → 
     ⌜ (expec_cost_plus_post (f d) Φs.*2 <= p' + d.2)%R⌝ )%I
    with "[H]" as "H"; last first.
  { iApply (step_fupdN_wand with "H"). iIntros "%H". iPureIntro. eapply Rle_trans; last exact Hexpec.
    apply law_total_expectation. eapply Forall2_same_length_lookup_2; auto. }
  iApply step_fupdN_plain_forall. iIntros (k).
  iApply step_fupdN_plain_forall. iIntros (p').
  iApply step_fupdN_plain_forall. iIntros (d).
  iApply step_fupdN_plain_impl_1. iIntros (Hlook1).
  iApply step_fupdN_plain_impl_1. iIntros (Hlook2).
  simpl. iMod ("H" with "[//] [//]") as "[%nt' H]".
  iApply step_fupdN_S_fupd. simpl. rewrite Nat.iter_add. 
  iApply (step_fupdN_wand with "H").
  iIntros "!> H". iSpecialize ("IH" with "H").
  iApply (step_fupdN_wand with "IH"). iIntros ">%H !>".
  iPureIntro. erewrite post_zero; last exact fork_post.
  rewrite fmap_app in H. setoid_rewrite fmap_replicate in H. done.
  Unshelve. apply elem_of_list_lookup. eauto.
Qed.

End adequacy.

Local Lemma wptp_progress_gen Σ Λ `{!invGpreS Σ} es σ1 c n p D σ2 t2 c2 e2 Φs_p
        (num_laters_per_step : nat → nat) :
  (∀ `{Hinv : !invGS_gen HasNoLc Σ},
      ⊢ |={⊤}=> ∃
         (stateI : state Λ → nat → nat → iProp Σ)
         (Φs_Φ : list (val Λ → iProp Σ))
         (fork_post : val Λ → iProp Σ)
         state_interp_mono,
       let _ : irisGS_gen Λ Σ := IrisG Hinv stateI fork_post num_laters_per_step
                                  state_interp_mono
       in
       stateI σ1 0 0 ∗
       ⌜ length Φs_p = length Φs_Φ ⌝ ∗ 
       wptp NotStuck es p (zip Φs_Φ Φs_p) ) → 
    nsteps n (es, σ1, c) D → 
    (t2, σ2, c2) ∈ (domD D) → 
    e2 ∈ t2 → 
    not_stuck e2 σ2.
Proof.
  intros Hwp ???.
  eapply pure_soundness.
  eapply (step_fupdN_soundness_gen _ HasNoLc (steps_sum num_laters_per_step 0 (S n))
  (steps_sum num_laters_per_step 0 (n))).
  iIntros (Hinv) "_".
  iMod Hwp as (stateI Φ fork_post state_interp_mono) "(Hσ & _ & Hwp)".
  iDestruct (wptp_length with "Hwp") as %Hlen'.
  iMod (@wptp_progress _ _
  (IrisG Hinv stateI fork_post num_laters_per_step state_interp_mono) _
   with "[Hσ]  Hwp") as "H"; try done. 
  iAssert (|={∅}▷=>^(steps_sum num_laters_per_step 0 (S n)) |={∅}=> ⌜not_stuck e2 σ2⌝)%I
    with "[-]" as "H".
  {iApply (step_fupdN_wand with "H"). iIntros "$". }
  destruct steps_sum; first done. by iApply step_fupdN_S_fupd.
Qed.

Local Lemma wptp_cost_gen Σ Λ `{!invGpreS Σ} s es σ1 c n p D Φs_p
    (num_laters_per_step : nat → nat) :
    (∀ `{Hinv : !invGS_gen HasNoLc Σ},
        ⊢ |={⊤}=> ∃
           (stateI : state Λ → nat → nat → iProp Σ)
           (Φs_Φ : list (val Λ → iProp Σ))
           (fork_post : val Λ → iProp Σ)
           state_interp_mono,
         let _ : irisGS_gen Λ Σ := IrisG Hinv stateI fork_post num_laters_per_step
                                    state_interp_mono
         in
         stateI σ1 0 0 ∗
         ⌜ length Φs_p = length Φs_Φ ⌝ ∗ 
         wptp s es p (zip Φs_Φ Φs_p) ) → 
    nsteps n (es, σ1, c) D → 
    (expec_cost_plus_post D Φs_p <= p + c )%R.
Proof.
    intros Hwp ?.
    eapply pure_soundness.
    eapply (step_fupdN_soundness_gen _ HasNoLc (S $ steps_sum num_laters_per_step 0 (S n))
    (steps_sum num_laters_per_step 0 (n))).
    iIntros (Hinv) "_".
    iMod Hwp as (stateI Φ fork_post state_interp_mono) "(Hσ & %Hlen & Hwp)".
    iDestruct (wptp_length with "Hwp") as %Hlen'.
    iMod (@wptp_cost _ _
    (IrisG Hinv stateI fork_post num_laters_per_step state_interp_mono) _
     with "[Hσ] [Hwp]") as "H"; try done. 
    iAssert (|={∅}▷=>^(S $ steps_sum num_laters_per_step 0 (S n)) |={∅}=> ⌜(expec_cost_plus_post D Φs_p <= p + c )%R⌝)%I
      with "[-]" as "H".
    { iApply (step_fupdN_wand with "H"). rewrite snd_zip; last lia.
    iIntros "$". }  
    iModIntro. by iApply step_fupdN_S_fupd. 
  Qed.


(** Iris's generic adequacy result *)
Lemma wp_strong_adequacy Σ Λ `{!invGpreS Σ} s es σ1 c n p D σ2 t2 c2 Φs_p φ
        (num_laters_per_step : nat → nat) :
  (* WP *)
  (∀ `{Hinv : !invGS_gen HasNoLc Σ},
      ⊢ |={⊤}=> ∃
         (stateI : state Λ → nat → nat → iProp Σ)
         (Φs_Φ : list ((val Λ → iProp Σ)))
         (fork_post : val Λ → iProp Σ)
         (* Note: existentially quantifying over Iris goal! [iExists _] should
         usually work. *)
         state_interp_mono,
       let _ : irisGS_gen Λ Σ := IrisG Hinv stateI fork_post num_laters_per_step
                                  state_interp_mono
       in
       stateI σ1 0 0 ∗
       ⌜ length Φs_Φ = length Φs_p ⌝ ∗ 
       wptp s es p (zip Φs_Φ Φs_p) ∗ 
       (∀ es' t2',
         (* es' is the final state of the initial threads, t2' the rest *)
         ⌜ t2 = es' ++ t2' ⌝ -∗
         (* es' corresponds to the initial threads *)
         ⌜ length es' = length es ⌝ -∗
         (* If this is a stuck-free triple (i.e. [s = NotStuck]), then all
         threads in [t2] are not stuck *)
         ⌜ ∀ e2, s = NotStuck → e2 ∈ t2 → not_stuck e2 σ2 ⌝ -∗
         (* The expected cost is bounded by the initial cost and potential*)
         ⌜(expec_cost_plus_post D Φs_p <= p + c )%R⌝ -∗ 
         (* The state interpretation holds for [σ2] *)
         (∃ n', ⌜ n' ≤ n ⌝ ∗  stateI σ2 n' (length t2')) -∗
         (* If the initial threads are done, their post-condition [Φ] holds *)
         ([∗ list] e;Φ ∈ es';Φs_Φ, from_option Φ True (to_val e)) -∗
         (* For all forked-off threads that are done, their postcondition
            [fork_post] holds. *)
         ([∗ list] v ∈ omap to_val t2', fork_post v) -∗
         (* Under all these assumptions, and while opening all invariants, we
         can conclude [φ] in the logic. After opening all required invariants,
         one can use [fupd_mask_subseteq] to introduce the fancy update. *)
         |={⊤,∅}=> ⌜ φ ⌝)) →
  nsteps n (es, σ1, c) D →
  (t2, σ2, c2) ∈ (domD D) → 
  (* Then we can conclude [φ] at the meta-level. *)
  φ.
Proof.
  intros Hwp Hsteps Hdom.
  eapply pure_soundness.
  eapply (step_fupdN_soundness_gen _ HasNoLc (steps_sum num_laters_per_step 0 (S n))
    (steps_sum num_laters_per_step 0 n)).
  iIntros (Hinv) "_".
  iMod Hwp as (stateI Φ fork_post state_interp_mono) "(Hσ & %Hlen & Hwp & Hφ)".
  iDestruct (wptp_length with "Hwp") as %Hlen1.
  iMod (@wptp_postconditions _ _
       (IrisG Hinv stateI fork_post num_laters_per_step state_interp_mono) _
    with "Hσ Hwp [//]") as "H"; first done.
  iAssert (|={∅}▷=>^(steps_sum num_laters_per_step 0 (S n)) |={∅}=> ⌜φ⌝)%I
    with "[-]" as "H"; last first.
  { destruct steps_sum; [done|]. by iApply step_fupdN_S_fupd. }
  iApply (step_fupdN_wand with "H").
  iIntros ">(%nt' & %ns' & %Hleq & Hσ & Hes) /=".
  iDestruct (big_sepL2_app_inv_r with "Hes") as (es' t2' ->) "[Hes' Ht2']".
  iDestruct (big_sepL2_length with "Ht2'") as %Hlen2.
  rewrite replicate_length in Hlen2; subst.
  iDestruct (big_sepL2_length with "Hes'") as %Hlen3.
  rewrite map_length in Hlen3.
  iApply ("Hφ" with "[//] [%] [ ] [ ] [Hσ] [Hes']"). 
  + by rewrite Hlen1 Hlen3.
  + (* At this point in the adequacy proof, we use a trick: we effectively run the
    user-provided WP proof again (i.e., instantiate the `invGS_gen` and execute the
    program) by using the lemma [wp_progress_gen]. In doing so, we can obtain
    the progress and cost part of the adequacy theorem.
  *)
  iPureIntro. intros e2 -> Hel.
  eapply (wptp_progress_gen); [done| | done | done|done ].
  iIntros (?). clear stateI Φ Hlen fork_post state_interp_mono Hlen1 Hlen3.
  iMod Hwp as (stateI Φ fork_post state_interp_mono) "(Hσ & <- & Hwp & Hφ)".
  iModIntro. iExists _, _, _, _. by iFrame.
  + iPureIntro. eapply (wptp_cost_gen); [done| | done].
  iIntros (?). clear stateI Φ Hlen fork_post state_interp_mono Hlen1 Hlen3.
  iMod Hwp as (stateI Φ fork_post state_interp_mono) "(Hσ & <- & Hwp & Hφ)".
  iModIntro. iExists _, _, _, _. by iFrame.
  + iExists _. iSplit; auto with lia.
  + rewrite fst_zip; auto with lia.
  + by rewrite big_sepL2_replicate_r // big_sepL_omap.
Qed.

Global Arguments wp_strong_adequacy _ _ {_}.

(** Since the full adequacy statement is quite a mouthful, we prove some more
intuitive and simpler corollaries. *)
Record adequate {Λ} (s : stuckness) (e1 : expr Λ) (σ1 : state Λ) c1
    (φ : val Λ → state Λ → Prop) p (vp : val Λ → R) := {
  adequate_result t2 σ2 v2 D c2 n:
   nsteps n ([e1], σ1, c1) D → 
   (of_val v2 :: t2, σ2, c2) ∈ domD D → 
   φ v2 σ2;
  adequate_not_stuck t2 σ2 e2 D c2 n:
   s = NotStuck →
   nsteps n ([e1], σ1, c1) D →
   (t2, σ2, c2) ∈ domD D → 
   e2 ∈ t2 → 
   not_stuck e2 σ2;
   adequate_cost D n: 
   nsteps n ([e1], σ1, c1) D → 
   (expec_cost_plus_post D [vp] <= p + c1)%R;
}.

Lemma adequate_alt {Λ} s e1 σ1 c1 (φ : val Λ → state Λ → Prop) p vp:
  adequate s e1 σ1 c1 φ p vp ↔ ∀ D n,
    nsteps n ([e1], σ1, c1) D →
    (expec_cost_plus_post D [vp] <= p + c1)%R ∧ 
    (∀ t2 σ2 c, (t2, σ2, c) ∈ domD D → 
      (∀ v2 t2', t2 = of_val v2 :: t2' → φ v2 σ2) ∧
      (∀ e2, s = NotStuck → e2 ∈ t2 → not_stuck e2 σ2)).
Proof.
  split.
  - intros []; naive_solver.
  - constructor; naive_solver.
Qed.

Theorem adequate_tp_safe {Λ} (e1 : expr Λ) t2 σ1 σ2 c1 c2 D p vp φ n:
  adequate NotStuck e1 σ1 c1 φ p vp →
  nsteps n ([e1], σ1, c1) D → 
  (t2, σ2, c2) ∈ domD D →
  Forall (λ e, is_Some (to_val e)) t2 ∨ ∃ D2, step (t2, σ2, c2) D2.
Proof.
  intros Had ? ?.
  destruct (decide (Forall (λ e, is_Some (to_val e)) t2)) as [|Ht2]; [by left|].
  apply (not_Forall_Exists _), Exists_exists in Ht2; destruct Ht2 as (e2&?&He2).
  destruct (adequate_not_stuck NotStuck e1 σ1 c1 φ p vp Had t2 σ2 e2 D c2 n) as  [?|(D2&?)];
    rewrite ?eq_None_not_Some; auto.
  { exfalso. eauto. }
  destruct (elem_of_list_split t2 e2) as (t2'&t2''&->); auto.
  right. eexists. econstructor; done.
Qed.

(** This simpler form of adequacy requires the [irisGS] instance that you use
everywhere to syntactically be of the form
{|
  iris_invGS := ...;
  state_interp σ _ κs _ := ...;
  fork_post v := ...;
  num_laters_per_step _ := 0;
  state_interp_mono _ _ _ _ := fupd_intro _ _;
|}
In other words, the state interpretation must ignore [ns] and [nt], the number
of laters per step must be 0, and the proof of [state_interp_mono] must have
this specific proof term.
*)
Lemma wp_adequacy Σ Λ `{!invGpreS Σ} s e σ c φ p vp:
  (∀ `{Hinv : !invGS_gen HasNoLc Σ},
     ⊢ |={⊤}=> ∃
         (stateI : state Λ → iProp Σ)
         (fork_post : val Λ → iProp Σ),
       let _ : irisGS_gen Λ Σ :=
           IrisG Hinv (λ σ _ _, stateI σ ) fork_post (λ _, 0)
                 (λ _ _ _ ,  fupd_intro _ _ )
       in
       stateI σ ∗ $p WP e @ s; ⊤ {{ v, ⌜φ v⌝ , (vp v) }} ) →
  adequate s e σ c (λ v _, φ v) p vp.
Proof.
  intros Hwp. apply adequate_alt. intros D n ?.
  assert (∃ t2 σ2 c2, (t2, σ2, c2) ∈ domD D) as (t2 & σ2 & c2 & H).
  { destruct (domD D) as [|[[t2 σ2] c2]] eqn:H;
    last set_solver. by specialize (domD_notnil D). }
  split; [| clear t2 σ2 c2 H; intros t2 σ2 c2 H]; eapply (wp_strong_adequacy Σ _) with (Φs_p := [vp]) (p:=p);
  [| done | done | | done | done];
  intros Hinv; iMod Hwp as (stateI fork_post) "[Hσ Hwp]";
  iExists (λ σ _  _, stateI σ), [(λ v, ⌜φ v⌝%I)], fork_post, _ => /=;
  iApply fupd_mask_intro_discard; [done| | done |]; iFrame;
   iSplit. 1,3: done. 1,2: iSplitL.
   1,3: by iApply wptp_singleton. 
  1,2: iIntros (e2 t2' -> ? ? ?) "_ H _ ";
  iApply fupd_mask_intro_discard. 1,3: done.
  + iPureIntro. done.
  + iDestruct (big_sepL2_cons_inv_r with "H") as (e' ? ->) "[Hwp H]".
  iDestruct (big_sepL2_nil_inv_r with "H") as %->. iSplit; last done.
  iIntros (v2 t2'' [= -> <-]). by rewrite to_of_val.
Qed.

Global Arguments wp_adequacy _ _ {_}.

Lemma wp_invariance Σ Λ `{!invGpreS Σ} s e1 σ1 c1 D t2 σ2 c2 p vp φ n:
  (∀ `{Hinv : !invGS_gen HasNoLc Σ} ,
     ⊢ |={⊤}=> ∃
         (stateI : state Λ → nat → iProp Σ)
         (fork_post : val Λ → iProp Σ),
       let _ : irisGS_gen Λ Σ := IrisG Hinv (λ σ _ nt, stateI σ nt) fork_post
              (λ _, 0) (λ _ _ _, fupd_intro _ _) in
       stateI σ1 0 ∗ $p WP e1 @ s; ⊤ {{ v , True , vp v }} ∗
       (stateI σ2 (pred (length t2)) -∗ ∃ E, |={⊤,E}=> ⌜φ⌝)) →
  nsteps n ([e1], σ1, c1) D → 
  (t2, σ2, c2) ∈ domD D →
  φ.
Proof.
  intros Hwp ?.
  eapply (wp_strong_adequacy Σ) with (Φs_p := [vp]); [done| |done]=> ?.
  iMod (Hwp _) as (stateI fork_post) "(Hσ & Hwp & Hφ)".
  iExists (λ σ _, stateI σ), [(λ _, True)%I], fork_post, _ => /=.
  iApply fupd_mask_intro_discard; first set_solver.
  iFrame. iSplitR; auto. iSplitL "Hwp"; first by iApply wptp_singleton.
  iIntros (e2 t2' -> _ _) "_ Hσ H _ /=". 
  iDestruct "Hσ" as "(%n'&_&Hσ)".
  iDestruct (big_sepL2_cons_inv_r with "H") as (? ? ->) "[_ H]".
  iDestruct (big_sepL2_nil_inv_r with "H") as %->.
  iDestruct ("Hφ" with "Hσ") as (E) ">Hφ".
  iApply fupd_mask_intro_discard; set_solver.
Qed.

Global Arguments wp_invariance _ _ {_}.
