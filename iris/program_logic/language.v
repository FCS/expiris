From iris.algebra Require Export ofe.
From iris.prelude Require Import distributions.
From iris.bi Require Export weakestpre.
From iris.prelude Require Import options.
Require Export Coq.Reals.RIneq.
Require Import Lra.
From stdpp Require Import tactics.

Section language_mixin.
  Context {expr val state observation : Type}.
  Context (of_val : val → expr).
  Context (to_val : expr → option val).
  Context (prim_step : expr → state →  @distribution (expr * state * R * list expr) → Prop).

  Record LanguageMixin := {
    mixin_to_of_val v : to_val (of_val v) = Some v;
    mixin_of_to_val e v : to_val e = Some v → of_val v = e;
    mixin_val_stuck e σ D : prim_step e σ D → to_val e = None;
    mixin_cost_nonneg e σ D: prim_step e σ D → ForallD (λ x, (x.1.2 >= 0)%R) D
  }.
End language_mixin.

Structure language := Language {
  expr : Type;
  val : Type;
  state : Type;
  of_val : val → expr;
  to_val : expr → option val;
  prim_step : expr → state →  @distribution (expr * state * R * list expr) → Prop;
  language_mixin : LanguageMixin of_val to_val prim_step
}.

Bind Scope expr_scope with expr.
Bind Scope val_scope with val.

Global Arguments Language {_ _ _ _ _ _ } _.
Global Arguments of_val {_} _.
Global Arguments to_val {_} _.
Global Arguments prim_step {_} _ _ _.

Canonical Structure stateO Λ := leibnizO (state Λ).
Canonical Structure valO Λ := leibnizO (val Λ).
Canonical Structure exprO Λ := leibnizO (expr Λ).

(*TODO move those definitions/lemmata*)

Definition map_ctx {Λ : language} (K : expr Λ → expr Λ) (D : @distribution (expr Λ * state Λ * R * list (expr Λ))) := distr_map D (fun d => (K d.1.1.1, d.1.1.2, d.1.2, d.2)).

Lemma map_ctx_id {Λ : language} (D : @distribution (expr Λ * state Λ * R * list (expr Λ))) : @map_ctx Λ (λ x, x) D = D.
Proof.
  unfold map_ctx. rewrite <- distr_map_id. apply distr_map_ext. by intros [[[]]].
Qed.

Definition expec_cost {Λ : language} (D : @distribution (expr Λ * state Λ * R * list (expr Λ))) := expec D (λ x, x.1.2).

Lemma expec_cost_map_ctx {Λ : language} (D : @distribution (expr Λ * state Λ * R * list (expr Λ)) ) K : expec_cost (map_ctx K D) = expec_cost D.
Proof.
  unfold expec_cost. unfold map_ctx. by rewrite expec_map.
Qed.

Definition cfg (Λ : language) := (list (expr Λ) * state Λ * R)%type.

Definition expec_cost2 {Λ : language} (D : @distribution (cfg Λ)) := expec D (λ x, x.2).

Lemma expec_cost2_add {Λ} D f g c: @expec_cost2 Λ (distr_map D
      (λ d, (f d, g d, (c + d.1.2)%R))) = (@expec_cost Λ D + c)%R.
Proof.
  unfold expec_cost2. unfold expec_cost. rewrite expec_map. simpl.
  rewrite expec_add. ring.
Qed.

Class LanguageCtx {Λ : language} (K : expr Λ → expr Λ) := {
  fill_not_val e :
    to_val e = None → to_val (K e) = None;
  fill_step e1 σ1 D:
    prim_step e1 σ1 D →
    prim_step (K e1) σ1 (map_ctx K D);
  fill_step_inv e1' σ1 D:
    to_val e1' = None → prim_step (K e1') σ1 D →
    ∃ D', D = (map_ctx K D') ∧ prim_step e1' σ1 D'
}.

Global Instance language_ctx_id Λ : LanguageCtx (@id (expr Λ)).
Proof. constructor; auto; intros.
  - by rewrite map_ctx_id.
  - intros. exists D. simpl in *. rewrite map_ctx_id. tauto.  
Qed.

Inductive atomicity := StronglyAtomic | WeaklyAtomic.

Definition stuckness_to_atomicity (s : stuckness) : atomicity :=
  if s is MaybeStuck then StronglyAtomic else WeaklyAtomic.

Section language.
  Context {Λ : language}.
  Implicit Types v : val Λ.
  Implicit Types e : expr Λ.

  Lemma to_of_val v : to_val (of_val v) = Some v.
  Proof. apply language_mixin. Qed.
  Lemma of_to_val e v : to_val e = Some v → of_val v = e.
  Proof. apply language_mixin. Qed.
  Lemma val_stuck e σ D : prim_step e σ D → to_val e = None.
  Proof. apply language_mixin. Qed.
  Lemma cost_nonneg e σ D : prim_step e σ D → ForallD (λ x, (x.1.2 >= 0)%R) D. 
  Proof. apply language_mixin. Qed.

  Definition reducible (e : expr Λ) (σ : state Λ) :=
    ∃ D, prim_step e σ D.
  Definition irreducible (e : expr Λ) (σ : state Λ) :=
    ∀ D, ¬prim_step e σ D.
  Definition stuck (e : expr Λ) (σ : state Λ) :=
    to_val e = None ∧ irreducible e σ.
  Definition not_stuck (e : expr Λ) (σ : state Λ) :=
    is_Some (to_val e) ∨ reducible e σ. 

  (* [Atomic WeaklyAtomic]: This (weak) form of atomicity is enough to open
     invariants when WP ensures safety, i.e., programs never can get stuck.  We
     have an example in lambdaRust of an expression that is atomic in this
     sense, but not in the stronger sense defined below, and we have to be able
     to open invariants around that expression.  See `CasStuckS` in
     [lambdaRust](https://gitlab.mpi-sws.org/FP/LambdaRust-coq/blob/master/theories/lang/lang.v).

     [Atomic StronglyAtomic]: To open invariants with a WP that does not ensure
     safety, we need a stronger form of atomicity.  With the above definition,
     in case `e` reduces to a stuck non-value, there is no proof that the
     invariants have been established again. *)
  Class Atomic (a : atomicity) (e : expr Λ) : Prop :=
    atomic σ D:
      prim_step e σ D →
      ∀ e' σ' c efs, (e', σ', c, efs) ∈ (domD D) → if a is WeaklyAtomic then irreducible e' σ' else is_Some (to_val e').

  Inductive step (ρ1 : cfg Λ) (Dρ2 : @distribution (cfg Λ)) : Prop :=
    | step_atomic e1 σ1 D t1 t2 c:
       ρ1 = (t1 ++ e1 :: t2, σ1, c) →
       prim_step e1 σ1 D →
       Dρ2 = distr_map D (fun d => (t1 ++ d.1.1.1 :: t2 ++ d.2, d.1.1.2, (c + d.1.2)%R)) →
       step ρ1 Dρ2.
  Local Hint Constructors step : core.

  (* At most n steps *)
  Inductive nsteps : nat → cfg Λ → @distribution (cfg Λ) → Type :=
    | nsteps_base n ρ :
       nsteps n ρ (init_single ρ) 
    | nsteps_l n ρ1 Dρ2 f:
       step ρ1 Dρ2 →
       (∀ ρ2, ρ2 ∈ (domD Dρ2) →  nsteps n ρ2 (f ρ2)) →
       nsteps (S n) ρ1 (bind Dρ2 f).
  Local Hint Constructors nsteps : core.

  Lemma of_to_val_flip v e : of_val v = e → to_val e = Some v.
  Proof. intros <-. by rewrite to_of_val. Qed.

  Lemma not_reducible e σ : ¬reducible e σ ↔ irreducible e σ.
  Proof. unfold reducible, irreducible. naive_solver. Qed.
  Lemma reducible_not_val e σ : reducible e σ → to_val e = None.
  Proof. intros (?&?); eauto using val_stuck. Qed.
  Lemma val_irreducible e σ : is_Some (to_val e) → irreducible e σ.
  Proof. intros [??] ? ?%val_stuck. by destruct (to_val e). Qed.
  Global Instance of_val_inj : Inj (=) (=) (@of_val Λ).
  Proof. by intros v v' Hv; apply (inj Some); rewrite -!to_of_val Hv. Qed.
  Lemma not_not_stuck e σ : ¬not_stuck e σ ↔ stuck e σ.
  Proof.
    rewrite /stuck /not_stuck -not_eq_None_Some -not_reducible.
    destruct (decide (to_val e = None)); naive_solver.
  Qed.

  Lemma strongly_atomic_atomic e a :
    Atomic StronglyAtomic e → Atomic a e.
  Proof.
     unfold Atomic. destruct a; first eauto using val_irreducible.
     intros H. intros. apply val_irreducible. by eapply H.
  Qed.

  Lemma reducible_fill `{!@LanguageCtx Λ K} e σ :
    reducible e σ → reducible (K e) σ.
  Proof. unfold reducible in *. naive_solver eauto using fill_step. Qed.
  Lemma reducible_fill_inv `{!@LanguageCtx Λ K} e σ :
    to_val e = None → reducible (K e) σ → reducible e σ.
  Proof.
    intros ? (D&Hstep); unfold reducible.
    apply fill_step_inv in Hstep as (D' & _ & Hstep); eauto.
  Qed.
  Lemma irreducible_fill `{!@LanguageCtx Λ K} e σ :
    to_val e = None → irreducible e σ → irreducible (K e) σ.
  Proof. rewrite -!not_reducible. naive_solver eauto using reducible_fill_inv. Qed.
  Lemma irreducible_fill_inv `{!@LanguageCtx Λ K} e σ :
    irreducible (K e) σ → irreducible e σ.
  Proof. rewrite -!not_reducible. naive_solver eauto using reducible_fill. Qed.

  Lemma not_stuck_fill_inv K `{!@LanguageCtx Λ K} e σ :
    not_stuck (K e) σ → not_stuck e σ.
  Proof.
    rewrite /not_stuck -!not_eq_None_Some. intros [?|?].
    - auto using fill_not_val.
    - destruct (decide (to_val e = None)); eauto using reducible_fill_inv.
  Qed.

  Lemma stuck_fill `{!@LanguageCtx Λ K} e σ :
    stuck e σ → stuck (K e) σ.
  Proof. rewrite -!not_not_stuck. eauto using not_stuck_fill_inv. Qed.

  (* TODO findout where we need this lemma to find suitable probabilistic version

  Lemma step_Permutation (t1 t1' t2 : list (expr Λ)) σ1 σ2 :
    t1 ≡ₚ t1' → step (t1,σ1) (t2,σ2) → ∃ t2', t2 ≡ₚ t2' ∧ step (t1',σ1) (t2',σ2).
  Proof.
    intros Ht [e1 σ1' e2 σ2' efs tl tr ?? Hstep]; simplify_eq/=.
    move: Ht; rewrite -Permutation_middle (symmetry_iff (≡ₚ)).
    intros (tl'&tr'&->&Ht)%Permutation_cons_inv_r.
    exists (tl' ++ e2 :: tr' ++ efs); split; [|by econstructor].
    by rewrite -!Permutation_middle !assoc_L Ht.
  Qed.*)

  Lemma step_insert i t2 σ2 e D c:
    t2 !! i = Some e →
    prim_step e σ2 D →
    step (t2, σ2, c) (distr_map D (λ '(e', σ3, c', efs), ((<[i:=e']>t2 ++ efs), σ3, (c+c')%R))).
  Proof.
    intros.
    edestruct (elem_of_list_split_length t2) as (t21&t22&?&?);
      first (by eauto using elem_of_list_lookup_2); simplify_eq.
    econstructor; eauto. apply distr_map_ext. intros [[[e' σ'] c'] efs].
    by rewrite insert_app_r_alt // Nat.sub_diag /= -assoc_L.
  Qed.

  Record pure_step (e1 e2 : expr Λ) := {
    pure_step_safe σ1 : reducible e1 σ1;
    pure_step_det σ1 D :
      prim_step e1 σ1 D → D = (init_single (e2, σ1, R0, []))
  }.

  Notation pure_steps_tp := (Forall2 (rtc pure_step)).

  (* TODO: Exclude the case of [n=0], either here, or in [wp_pure] to avoid it
  succeeding when it did not actually do anything. *)
  Class PureExec (φ : Prop) (n : nat) (e1 e2 : expr Λ) :=
    pure_exec : φ → relations.nsteps pure_step n e1 e2.

  Lemma pure_step_ctx K `{!@LanguageCtx Λ K} e1 e2 :
    pure_step e1 e2 →
    pure_step (K e1) (K e2).
  Proof.
    intros [Hred Hstep]. split.
    - unfold reducible in *. naive_solver eauto using fill_step.
    - intros σ1 D Hpstep.
      destruct (fill_step_inv e1 σ1 D) as (D' & -> & ?); [|exact Hpstep|].
      + destruct (Hred σ1) as (? & ? ); eauto using val_stuck.
      + rewrite (Hstep σ1 D'); auto. unfold map_ctx. rewrite distr_map_single.
        f_equal.
  Qed.

  Lemma pure_step_nsteps_ctx K `{!@LanguageCtx Λ K} n e1 e2 :
    relations.nsteps pure_step n e1 e2 →
    relations.nsteps pure_step n (K e1) (K e2).
  Proof. eauto using nsteps_congruence, pure_step_ctx. Qed.

  Lemma rtc_pure_step_ctx K `{!@LanguageCtx Λ K} e1 e2 :
    rtc pure_step e1 e2 → rtc pure_step (K e1) (K e2).
  Proof. eauto using rtc_congruence, pure_step_ctx. Qed.

  (* We do not make this an instance because it is awfully general. *)
  Lemma pure_exec_ctx K `{!@LanguageCtx Λ K} φ n e1 e2 :
    PureExec φ n e1 e2 →
    PureExec φ n (K e1) (K e2).
  Proof. rewrite /PureExec; eauto using pure_step_nsteps_ctx. Qed.

  (* This is a family of frequent assumptions for PureExec *)
  Class IntoVal (e : expr Λ) (v : val Λ) :=
    into_val : of_val v = e.

  Class AsVal (e : expr Λ) := as_val : ∃ v, of_val v = e.
  (* There is no instance [IntoVal → AsVal] as often one can solve [AsVal] more
  efficiently since no witness has to be computed. *)
  Global Instance as_vals_of_val vs : TCForall AsVal (of_val <$> vs).
  Proof.
    apply TCForall_Forall, Forall_fmap, Forall_true=> v.
    rewrite /AsVal /=; eauto.
  Qed.

  Lemma as_val_is_Some e :
    (∃ v, of_val v = e) → is_Some (to_val e).
  Proof. intros [v <-]. rewrite to_of_val. eauto. Qed.

  Lemma prim_step_not_stuck e σ D :
    prim_step e σ D → not_stuck e σ.
  Proof. rewrite /not_stuck /reducible. eauto 10. Qed.

  Lemma rtc_pure_step_val `{!Inhabited (state Λ)} v e :
    rtc pure_step (of_val v) e → to_val e = Some v.
  Proof.
    intros ?; rewrite <- to_of_val.
    f_equal; symmetry; eapply rtc_nf; first done.
    intros [e' [Hstep _]].
    destruct (Hstep inhabitant) as (?&Hval%val_stuck).
    by rewrite to_of_val in Hval.
  Qed.

  Lemma expec_cost_nonneg e σ D : prim_step e σ D → (expec_cost D >= 0)%R.
  Proof.
    intros Hstep. unfold expec_cost. apply cost_nonneg in Hstep. 
    by apply expec_geq_mono.
  Qed.

End language.

Global Hint Mode PureExec + - - ! - : typeclass_instances.

Global Arguments step_atomic {Λ ρ1 Dρ2}.

Notation pure_steps_tp := (Forall2 (rtc pure_step)).