From iris.prelude Require Import distributions.
From iris.proofmode Require Import base proofmode classes.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Export language.
(* FIXME: If we import iris.bi.weakestpre earlier texan triples do not
   get pretty-printed correctly. *)
From iris.bi Require Export weakestpre.
From iris.prelude Require Import options.
From Coq Require Import Lra.
Import uPred.

Class irisGS_gen (Λ : language) (Σ : gFunctors) := IrisG {
  iris_invGS :> invGS_gen HasNoLc Σ;

  (** The state interpretation is an invariant that should hold in
  between each step of reduction. Here [Λstate] is the global state,
  the first [nat] is the number of steps already performed by the
  program, and the
  last [nat] is the number of forked-off threads (not the total number
  of threads, which is one higher because there is always a main
  thread). *)
  state_interp : state Λ → nat → nat → iProp Σ;

  (** A fixed postcondition for any forked-off thread. For most languages, e.g.
  heap_lang, this will simply be [True]. However, it is useful if one wants to
  keep track of resources precisely, as in e.g. Iron. *)
  fork_post : val Λ → iProp Σ;

  (** The number of additional logical steps (i.e., later modality in the
  definition of WP) and later credits per physical step is
  [S (num_laters_per_step ns)], where [ns] is the number of physical steps
  executed so far. We add one to [num_laters_per_step] to ensure that there
  is always at least one later and later credit for each physical step. *)
  num_laters_per_step : nat → nat;

  (** When performing pure steps, the state interpretation needs to be
  adapted for the change in the [ns] parameter.

  Note that we use an empty-mask fancy update here. We could also use
  a basic update or a bare magic wand, the expressiveness of the
  framework would be the same. If we removed the modality here, then
  the client would have to include the modality it needs as part of
  the definition of [state_interp]. Since adding the modality as part
  of the definition [state_interp_mono] does not significantly
  complicate the formalization in Iris, we prefer simplifying the
  client. *)
  state_interp_mono σ ns nt:
    state_interp σ ns nt ⊢ |={∅}=> state_interp σ (S ns) nt
}.
Global Opaque iris_invGS.
Global Arguments IrisG {Λ Σ}.

Notation irisGS := irisGS_gen. 

(** The predicate we take the fixpoint of in order to define the WP. *)
(** In the step case, we both provide [S (num_laters_per_step ns)]
  later credits, as well as an iterated update modality that allows
  stripping as many laters, where [ns] is the number of steps already taken.
  We have both as each of these provides distinct advantages:
  - Later credits do not have to be used right away, but can be kept to
    eliminate laters at a later point.
  - The step-taking update composes well in parallel: we can independently
    compose two clients who want to eliminate their laters for the same
    physical step, which is not possible with later credits, as they
    can only be used by exactly one client.
  - The step-taking update can even be used by clients that opt out of
    later credits, e.g. because they use [BiFUpdPlainly]. *)
Definition wp_pre `{!irisGS_gen Λ Σ} (s : stuckness)
    (wp : coPset -d> expr Λ -d>  R -d> (val Λ -> R) -d> (val Λ -d> iPropO Σ) -d> iPropO Σ) :
    coPset -d> expr Λ -d> R -d> (val Λ -> R) -d> (val Λ -d> iPropO Σ) -d> iPropO Σ := λ E e1 p vp Φ,
  match to_val e1 with
  | Some v =>  |={E,∅}=>  ⌜(vp v <= p)%R ⌝ ∗ |={∅, E}=> Φ v 
  | None => ∀ σ1 ns nt,
     state_interp σ1 ns nt ={E,∅}=∗
       ⌜ (p >= 0)%R ⌝ ∗ 
       ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
       ∀ D, ⌜prim_step e1 σ1 D⌝ 
         ={∅}▷=∗^(S $ num_laters_per_step ns) 
         ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∗ 
         [∧ list] i ↦ d; p' ∈ (domD D); pl,
         ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
         ∃ p'' pl', ⌜ (p'' + R_list_sum pl' <= p')%R ⌝  ∗ 
         |={∅,E}=> state_interp σ2 (S ns) (length efs + nt) ∗
         wp E e2 p'' vp Φ ∗
         [∗ list] i ↦ ef; p''' ∈ efs; pl', wp ⊤ ef p''' (λ _, R0) fork_post
  end%I. 

Local Instance wp_pre_contractive `{!irisGS_gen Λ Σ} s : Contractive (wp_pre s).
Proof.
  rewrite /wp_pre /= => n wp wp' Hwp E e1 p1 p2 Φ.
  do 15 (f_contractive || f_equiv).
  (* FIXME : simplify this proof once we have a good definition and a
     proper instance for step_fupdN. *)
  induction num_laters_per_step as [|k IH]; simpl.
  - repeat (f_contractive || f_equiv); apply Hwp.
  - f_equiv. by rewrite -IH.
Qed.

Local Definition wp_def `{!irisGS_gen Λ Σ} : Wp (iProp Σ) (expr Λ) (val Λ) stuckness :=
  λ s : stuckness, fixpoint (wp_pre s).
Local Definition wp_aux : seal (@wp_def). Proof. by eexists. Qed.
Definition wp' := wp_aux.(unseal).
Global Arguments wp' {Λ Σ _}.
Global Existing Instance wp'.
Local Lemma wp_unseal `{!irisGS_gen Λ Σ} : wp = @wp_def Λ Σ _.
Proof. rewrite -wp_aux.(seal_eq) //. Qed.

Section wp.
Context `{!irisGS_gen Λ Σ}.
Implicit Types s : stuckness.
Implicit Types P : iProp Σ.
Implicit Types Φ : val Λ → iProp Σ.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.
Implicit Types p : R.
Implicit Types vp : val Λ → R.

(* Weakest pre *)
Lemma wp_unfold s E e p vp Φ:
  $p WP e @ s; E {{ Φ, vp }} ⊣⊢ wp_pre s (wp (PROP:=iProp Σ) s) E e p vp Φ.
Proof. rewrite wp_unseal. apply (fixpoint_unfold (wp_pre s)). Qed.

Global Instance wp_ne s E e n p vp:
  Proper (pointwise_relation _ (dist n) ==> dist n) (wp (PROP:=iProp Σ) s E e p vp).
Proof.
  revert e p vp. induction (lt_wf n) as [n _ IH]=> e p vp Φ Ψ HΦ.
  rewrite !wp_unfold /wp_pre /=.
  (* FIXME: figure out a way to properly automate this proof *)
  (* FIXME: reflexivity, as being called many times by f_equiv and f_contractive
  is very slow here *)
  do 17 (f_contractive || f_equiv).
  (* FIXME : simplify this proof once we have a good definition and a
     proper instance for step_fupdN. *)
  induction num_laters_per_step as [|k IHk]; simpl; last by rewrite IHk.
  do 23 (f_contractive || f_equiv).
  rewrite IH; [done|lia|]. intros v. eapply dist_le; [apply HΦ|lia].
Qed.
Global Instance wp_proper s E e p vp:
  Proper (pointwise_relation _ (≡) ==> (≡)) (wp (PROP:=iProp Σ) s E e p vp).
Proof.
  by intros Φ Φ' ?; apply equiv_dist=>n; apply wp_ne=>v; apply equiv_dist.
Qed.
Global Instance wp_contractive s E e n p vp:
  TCEq (to_val e) None →
  Proper (pointwise_relation _ (dist_later n) ==> dist n) (wp (PROP:=iProp Σ) s E e p vp).
Proof.
  intros He Φ Ψ HΦ. rewrite !wp_unfold /wp_pre He /=.
  do 16 (f_contractive || f_equiv).
  (* FIXME : simplify this proof once we have a good definition and a
     proper instance for step_fupdN. *)
  induction num_laters_per_step as [|k IHk]; simpl; last by rewrite IHk.
  by do 25 f_equiv.
Qed.

Lemma wp_value_fupd' s E Φ v p vp: $p WP of_val v @ s; E {{ Φ, vp }} ⊣⊢ |={E, ∅}=> ⌜(vp v <= p)%R ⌝ ∗  |={∅, E}=>  Φ v .
Proof. rewrite wp_unfold /wp_pre to_of_val. auto. Qed.

Lemma wp_strong_mono s1 s2 E1 E2 e Φ Ψ p p' vp vp':
  s1 ⊑ s2 → E1 ⊆ E2 → (p <= p')%R →
  $p WP e @ s1; E1 {{ Φ, vp }} -∗ (∀ v, Φ v -∗ ⌜ (vp v >= vp' v)%R⌝)  -∗  (∀ v, Φ v ={E2}=∗ Ψ v) -∗ $p' WP e @ s2; E2 {{ Ψ, vp' }}.
Proof.
  iIntros (? HE Hp) "H Hvp HΦ". iLöb as "IH" forall (e E1 E2 HE Φ Ψ p p' Hp vp vp') "Hvp".
  rewrite !wp_unfold /wp_pre /=. 
  destruct (to_val e) as [v|] eqn:?. 
    - iMod (fupd_mask_subseteq E1) as "HE"; auto. iMod "H" as "[%Hvp' >H]".
      iPoseProof ("Hvp" $! v with "H")  as "%Hvp".
      iApply fupd_mask_intro; first set_solver.
      iIntros "Hclose".
      iSplitR.
        + iPureIntro. lra.
        + iMod "Hclose". iMod "HE". by iApply "HΦ".
    -  (* iDestruct "H" as "[%Hp0 H]". iSplitR; first by (iPureIntro; lra). *)
      iIntros (σ1 ns nt) "Hσ".
      iMod (fupd_mask_subseteq E1) as "Hclose"; first done.
      iMod ("H" with "[$]") as "[% [% H]]".
      iModIntro. iSplitR; first (iPureIntro; lra).
      iSplit; first by destruct s1, s2. iIntros (D Hstep).
      iSpecialize ("H" $! D Hstep).
      iMod "H". iIntros "!> !>".  iMod "H". iModIntro.
      iApply (step_fupdN_wand with "[H]"); first by iApply "H".
      iIntros "(%pl & % & H)".
      iExists pl. iSplit.
       {iPureIntro; lra. }
      iApply (big_andL2_impl). iFrame.
      iIntros (k d p'') "_ _ H".
      iIntros "%e2 %σ2 %c %efs ->".
      iDestruct ("H" $! e2 σ2 c efs with "[//]") as "(%p''' & %pl' & % & H)".
      iExists p''', pl'. iSplitR; auto.
      iMod "H" as "(Hσ & Hwp & Hefs)".
      iMod "Hclose" as "_". 
      iFrame. iSplitR "Hefs".
      + assert (p''' <= p''')%R as Hp1 by lra. iApply ("IH" with "[//] [//] Hwp HΦ "). done.
      + iApply (big_sepL2_impl with "Hefs"). iModIntro. iIntros "!>" (k' ef r) "_ _ H".
        assert (r <= r)%R as ? by lra. iApply ("IH" with "[] [] H"); auto. 
        iIntros. iPureIntro. lra.
Qed.

Lemma fupd_wp s E e p vp Φ :  (|={E}=> $p WP e @ s; E {{ Φ, vp }}) ⊢ $p WP e @ s; E {{ Φ, vp }}.
Proof.
  rewrite wp_unfold /wp_pre. iIntros "H". destruct (to_val e) as [v|] eqn:?.
  { by iMod "H". }
  iIntros (σ1 ns nt) "Hσ1". iMod "H". by iApply "H".
Qed.

Lemma wp_fupd s E e Φ p vp: $p WP e @ s; E {{ v, |={E}=> Φ v , vp v }} ⊢ $p WP e @ s; E {{ Φ , vp}}.
Proof. iIntros "H". iApply (wp_strong_mono s s E with "H"); auto; first lra.
 iIntros. iPureIntro. lra. Qed.

Lemma wp_atomic s E1 E2 e Φ `{!Atomic (stuckness_to_atomicity s) e} p vp:
  (|={E1,E2}=> $p WP e @ s; E2 {{ v, |={E2,E1}=> Φ v , vp v }}) ⊢ $p WP e @ s; E1 {{ Φ , vp }}.
Proof.
  iIntros "H". rewrite !wp_unfold /wp_pre.
  destruct (to_val e) as [v|] eqn:He.
  { iMod "H" as ">[$ >>H]". iMod (fupd_mask_subseteq ∅ ) as "Hclose"; first set_solver.
  iModIntro. iMod "Hclose". done. }
  iIntros (σ1 ns nt) "Hσ". iMod "H". iMod ("H" $! σ1 with "Hσ") as "[$ [$ H]]".
  iModIntro. iIntros (D Hstep). iDestruct ("H" $! D Hstep) as "H".
  iFrame. iApply (step_fupdN_wand with "[H]"); first by iApply "H".
  iIntros "(%pl & %Hexpec & H)". iExists pl. iSplitR; auto.
  iApply big_andL2_impl. iFrame. iIntros "%k %d %r %Hdom ? H".
  iIntros "%e2 %σ2 %c %efs ->".
  iDestruct ("H" $! e2 σ2 c efs with "[//]") as "(%p''' & %pl' & % & H)".
  iExists p''', pl'. iSplitR; auto.
  iMod "H" as "(Hσ & H & Hefs)".
  destruct s.
  - rewrite !wp_unfold /wp_pre. destruct (to_val e2) as [v2|] eqn:He2.
    + iDestruct "H" as " >[%Hvp >>H']". repeat iModIntro. iFrame.
    iMod (fupd_mask_subseteq ∅ ) as "Hclose"; first set_solver.
    iModIntro. iSplitR; auto.
    + iMod ("H" $! _ _ with "[$]") as "[_ [%H' _]]".
      contradict H'. apply not_reducible. specialize (atomic _ _ Hstep) as H''.
      apply (H'' e2 σ2 c efs). by eapply elem_of_list_lookup_2.
  - specialize (atomic _ _ Hstep) as H'. 
    apply elem_of_list_lookup_2 in Hdom.
    unfold is_Some in H'.
    destruct (H' e2 σ2 c efs Hdom) as [v Hv].
    rewrite <- (of_to_val _ _ Hv). rewrite wp_value_fupd'. iFrame.
    iMod "H" as "[%Hvp >>H]". 
    rewrite wp_value_fupd'. iApply fupd_frame_l. iModIntro.
    iSplitR; auto. iMod (fupd_mask_subseteq ∅ ) as "Hclose"; first set_solver.
    iModIntro. by iMod "Hclose".
Qed.

(** In this stronger version of [wp_step_fupdN], the masks in the
   step-taking fancy update are a bit weird and somewhat difficult to
   use in practice. Hence, we prove it for the sake of completeness,
   but [wp_step_fupdN] is just a little bit weaker, suffices in
   practice and is easier to use.

   See the statement of [wp_step_fupdN] below to understand the use of
   ordinary conjunction here. *)
Lemma wp_step_fupdN_strong n s E1 E2 e P Φ p vp:
  TCEq (to_val e) None → E2 ⊆ E1 → 
  (∀ σ ns nt, state_interp σ ns nt
       ={E1,∅}=∗ ⌜n ≤ S (num_laters_per_step ns)⌝) ∧
  ((|={E1,E2}=> |={∅}▷=>^n |={E2,E1}=> P) ∗
    $p WP e @ s; E2 {{ v, P ={E1}=∗ Φ v , vp v }}) -∗
  $p WP e @ s; E1 {{ Φ , vp }}.
Proof.
  destruct n as [|n].
  { iIntros (? ?) "/= [_ [HP Hwp]]".
    iApply (wp_strong_mono with "Hwp"); auto.
      + lra.
      + iIntros. iPureIntro. lra.
      + iIntros (v) "H". iApply ("H" with "[>HP]"). by do 2 iMod "HP". }
  rewrite !wp_unfold /wp_pre /=. iIntros (-> ?) "H". 
  iIntros (σ1 ns nt) "Hσ".
  destruct (decide (n ≤ num_laters_per_step ns)) as [Hn|Hn]; first last.
  { iDestruct "H" as "[Hn _]".  iMod ("Hn" with "Hσ") as %?. lia. }
  iDestruct "H" as "[_ [>HP Hwp]]". iMod ("Hwp" with "[$]") as "[$ [$ H]]". iMod "HP".
  iIntros "!>" (D Hstep). iSpecialize ("H" $! D Hstep).
  revert n Hn. generalize (num_laters_per_step ns)=>n0 n Hn.
  iInduction n as [|n] "IH" forall (n0 Hn).
  -iApply (step_fupdN_wand with "H"). iModIntro. iIntros "(%pl & %Hexpec & H)".
  iExists pl. iSplitR; auto. iApply big_andL2_impl. iFrame. iIntros (k d c Hdom Hpl) "H".
  iIntros "%e2 %σ2 %c' %efs ->". 
  iDestruct ("H" $! e2 σ2 c' efs with "[//]") as "(%p''' & %pl' & % & H)".
  iExists p''', pl'. iSplitR; auto. iMod "HP".
  iMod "H" as "(Hσ & H & Hefs)". iMod "HP". 
  iIntros "!>". iFrame.
  iApply (wp_strong_mono with "H"); auto.
      + lra.
      + iIntros. iPureIntro. lra.
      + iIntros (v) "HΦ". iApply ("HΦ" with "HP").
  - destruct n0 as [|n0]; [lia|]=>/=. iMod "H". iIntros "!> !>". iMod "HP". iMod "HP". iMod "H". iIntros "!>". iApply ("IH" with "[] HP H"). auto with lia.
Qed.

Lemma wp_bind K `{!LanguageCtx K} s E e Φ p vp vp':
  $p WP e @ s; E {{ v, $(vp' v) WP K (of_val v) @ s; E {{ Φ , vp}} , vp' v }} ⊢ $p WP K e @ s; E {{ Φ , vp }}.
Proof.
  iIntros "H". iLöb as "IH" forall (E e Φ p vp). rewrite wp_unfold /wp_pre.
  destruct (to_val e) as [v|] eqn:He.
  { apply of_to_val in He as <-. 
  iApply fupd_wp; auto. iMod "H" as "[%Hvp >H]".
  iModIntro. iApply (wp_strong_mono with "H"); auto. 
  iIntros. iPureIntro. lra. }
  rewrite wp_unfold /wp_pre fill_not_val /=; [|done].
  iIntros (σ1 step n) "Hσ". iMod ("H" with "[$]") as "[$ [% H]]".
  iModIntro; iSplit.
  { destruct s; eauto using reducible_fill. }
  iIntros (D Hstep).
  destruct (fill_step_inv e σ1 D) as (D'&->&H0); auto.
  iSpecialize ("H" $! D' H0).
  iApply (step_fupdN_wand with "H").
  iModIntro. iIntros "(%pl & % & H)". iExists pl. iSplitR.
  { iPureIntro. unfold map_ctx. unfold expec_cost. rewrite <- expecL_map.
      rewrite expec_map. simpl. done. }
  unfold map_ctx. rewrite distr_map_dom. setoid_rewrite big_andL2_fmap_l at 2.
  iApply (big_andL2_impl). iFrame.
  iIntros (k [[[e3 σ3] c3 ] efs3] p' Hdom Hpl) "H" .
  iIntros (e2 σ2 c efs Hd) .
  injection Hd as <- -> -> ->.
  iDestruct ("H" with "[//]") as "(%p''' & %pl' & % & H)".
  iExists p''', pl'. iSplitR; auto.
  iMod "H" as "(Hσ & H & Hefs)".
  iModIntro. iFrame. by iApply "IH".
Qed.

Lemma wp_pot_add s E e Φ p1 p2 vp : (p2 >= 0)%R → $p1 WP e @ s; E {{Φ, vp}} ⊢ $(p1+p2)%R WP e @ s; E {{v, Φ v, (vp v + p2)%R}}.
Proof.
  intros geq0. iLöb as "IH" forall (E e Φ p1 vp). rewrite !wp_unfold /wp_pre /=.
  destruct (to_val e) as [v|] eqn:He.
    {apply of_to_val in He as <-. iIntros ">[%Hvp H]". iFrame. iPureIntro. lra. }
  iIntros "H %σ1 %ns %nt Hσ". iMod ("H" $! σ1 ns nt with "Hσ") as "[% [$ H]]".
  iModIntro. iSplitR; first (iPureIntro;lra). iIntros (D Hstep). iSpecialize ("H" $! D Hstep).
  iApply (step_fupdN_wand with "H"). iModIntro.
  iIntros "(%pl & % & H)". iDestruct (big_andL2_length with "H") as "%Hlen".
  iExists (map (λ p, (p + p2)%R) pl). iSplitR.
  {rewrite expecL_add; auto. iPureIntro. lra.  }
  rewrite big_andL2_fmap_r. iApply big_andL2_impl. iFrame.
  iIntros (k [[[]] c2] p' Hdom Hpl) "H".
  iIntros (e2 σ2 c efs ->).
  iDestruct ("H"  with "[//]") as "(%p''' & %pl' & % & H)".
  iExists (p''' +p2)%R , pl'. iSplitR.
  {iPureIntro. repeat split; auto; lra. }
  iMod "H" as "(Hσ & H & Hefs)".
  iFrame. by iApply "IH".
Qed.

Lemma wp_pot_mult s E e Φ p vp r: (r >= 1)%R → $p WP e @ s; E {{Φ, vp}} ⊢ $(p * r)%R WP e @ s; E {{v, Φ v, (vp v * r)%R}}.
Proof.
  intros ge0. iLöb as "IH" forall (E e Φ p vp). rewrite !wp_unfold /wp_pre /=.
  destruct (to_val e) as [v|] eqn:He.
    {apply of_to_val in He as <-. iIntros ">[%Hvp H]". iFrame. iPureIntro. nra. }
  iIntros "H %σ1 %ns %nt Hσ". iMod ("H" $! σ1 ns nt with "Hσ") as "[% [$ H]]".
  iModIntro. iSplitR; first (iPureIntro;nra). iIntros (D Hstep). iSpecialize ("H" $! D Hstep).
  iApply (step_fupdN_wand with "H"). iModIntro.
  iIntros "(%pl & % & H)". iDestruct (big_andL2_length with "H") as "%Hlen".
  iExists (map (λ p, (p * r)%R) pl). iSplitR.
  {rewrite expecL_mult; auto. iPureIntro. specialize (expec_cost_nonneg e σ1 D Hstep) as Hgeq0.
  nra. }
  rewrite big_andL2_fmap_r. iApply big_andL2_impl. iFrame.
  iIntros (k [[[]] c2] p' Hdom Hpl) "H".
  iIntros (e2 σ2 c efs ->).
  iDestruct ("H"  with "[//]") as "(%p''' & %pl' & % & H)".
  iExists (p''' * r)%R , (map (λ p, (p * r)%R ) pl'). iSplitR.
  { iPureIntro. rewrite R_list_sum_mult. rewrite map_id. nra. }
  iMod "H" as "(Hσ & H & Hefs)".
  iFrame. iSplitR "Hefs".
    - by iApply "IH".
    - iApply big_sepL2_fmap_r. iApply (big_sepL2_impl with "Hefs").
      iIntros "!> !>" (?????) "Hwp". 
      iSpecialize ("IH" with "Hwp"). by replace (R0 * r)%R with R0 by lra.
Qed.


(** * Derived rules *)
Lemma wp_mono s E e Φ Ψ p vp: (∀ v, Φ v ⊢ Ψ v) → $p WP e @ s; E {{ Φ, vp }} ⊢ $p WP e @ s; E {{ Ψ, vp }}.
Proof.
  iIntros (HΦ) "H"; iApply (wp_strong_mono with "H"); auto.
    - lra.
    - iIntros. iPureIntro. lra.
    - iIntros (v) "?". by iApply HΦ.
Qed.
Lemma wp_pot_mono1 s E e Φ p p' vp: (p <= p')%R → $p WP e @ s; E {{ Φ, vp }} ⊢ $p' WP e @ s; E {{ Φ, vp }}.
Proof.
  iIntros (Hp) "H". iApply (wp_strong_mono with "H"); auto.
  iIntros. iPureIntro. lra.
Qed.
Lemma wp_pot_mono2 s E e Φ p vp vp': (∀ v, vp v >= vp' v)%R → $p WP e @ s; E {{ Φ, vp }} ⊢ $p WP e @ s; E {{ Φ, vp' }}.
Proof.
  iIntros (Hvp) "H". iApply (wp_strong_mono with "H"); auto.
  lra. 
Qed.
Lemma wp_wand_pot s E e Φ Ψ p p' vp vp':
 $p WP e @ s; E {{Φ , vp}} -∗
 (∀ v, Φ v -∗  Ψ v) -∗ 
 ⌜ (p <= p')%R ⌝  -∗ 
 (∀ v, Φ v -∗ ⌜ (vp v >= vp' v)%R ⌝ ) -∗ 
   $p' WP e @ s; E {{Ψ, vp'}}.
Proof.
  iIntros "Hwp HΨ %hp Hvp".
  iApply (wp_strong_mono with "Hwp Hvp"); auto.
  iIntros (?) "?". by iApply "HΨ".
Qed.

Lemma wp_stuck_mono s1 s2 E e Φ p vp:
  s1 ⊑ s2 → $p WP e @ s1; E {{ Φ, vp }} ⊢ $p WP e @ s2; E {{ Φ , vp}}.
Proof. iIntros (?) "H". iApply (wp_strong_mono with "H"); auto.
  - lra.
  - iIntros. iPureIntro. lra.
Qed.
Lemma wp_stuck_weaken s E e Φ :
  WP e @ s; E {{ Φ }} ⊢ WP e @ E ?{{ Φ }}.
Proof. apply wp_stuck_mono. by destruct s. Qed.
Lemma wp_mask_mono s E1 E2 e Φ : E1 ⊆ E2 → WP e @ s; E1 {{ Φ }} ⊢ WP e @ s; E2 {{ Φ }}.
Proof. iIntros (?) "H"; iApply (wp_strong_mono with "H"); auto.
  - lra.
  - iIntros. iPureIntro. lra.
Qed.
Global Instance wp_mono' s E e p vp:
  Proper (pointwise_relation _ (⊢) ==> (⊢)) (wp (PROP:=iProp Σ) s E e p vp).
Proof. by intros Φ Φ' ?; apply wp_mono. Qed.
Global Instance wp_flip_mono' s E e p vp:
  Proper (pointwise_relation _ (flip (⊢)) ==> (flip (⊢))) (wp (PROP:=iProp Σ) s E e p vp).
Proof. by intros Φ Φ' ?; apply wp_mono. Qed.

Lemma wp_value_fupd s E Φ e v p vp: IntoVal e v → $p WP e @ s; E {{ Φ, vp }} ⊣⊢ |={E, ∅}=> ⌜(vp v <= p)%R ⌝ ∗ |={∅, E}=> Φ v.
Proof. intros <-. apply wp_value_fupd'. Qed.
Lemma wp_value' s E Φ v p vp: (vp v <= p)%R → Φ v ⊢  $p WP (of_val v) @ s; E {{ Φ , vp}}.
Proof.
 intros ?. rewrite wp_value_fupd'. iIntros. iApply fupd_frame_l. iSplitR; auto.
 iMod (fupd_mask_subseteq ∅ ) as "Hclose"; first set_solver. iModIntro. by iMod "Hclose".
 Qed.
 
Lemma wp_value s E Φ e v p vp: IntoVal e v → (vp v <= p)%R → Φ v ⊢ $p WP e @ s; E {{ Φ , vp}}.
Proof. intros <-. apply wp_value'. Qed.

Lemma wp_frame_l s E e Φ R p vp: R ∗ $p WP e @ s; E {{ Φ, vp }} ⊢ $p WP e @ s; E {{ v, R ∗ Φ v , vp v }}.
Proof. iIntros "[? H]". iApply (wp_strong_mono with "H"); auto with iFrame.
  - lra.
  - iIntros. iPureIntro. lra.
Qed.
Lemma wp_frame_r s E e Φ R p vp : $p WP e @ s; E {{ Φ, vp }} ∗ R ⊢ $p WP e @ s; E {{ v, Φ v ∗ R , vp v }}.
Proof. iIntros "[H ?]". iApply (wp_strong_mono with "H"); auto with iFrame.
  - lra.
  - iIntros. iPureIntro. lra.
Qed.

(** This lemma states that if we can prove that [n] laters are used in
   the current physical step, then one can perform an n-steps fancy
   update during that physical step. The resources needed to prove the
   bound on [n] are not used up: they can be reused in the proof of
   the WP or in the proof of the n-steps fancy update. In order to
   describe this unusual resource flow, we use ordinary conjunction as
   a premise. *)
Lemma wp_step_fupdN n s E1 E2 e P Φ p vp:
  TCEq (to_val e) None → E2 ⊆ E1 →
  (∀ σ ns nt, state_interp σ ns nt
       ={E1,∅}=∗ ⌜n ≤ S (num_laters_per_step ns)⌝) ∧
  ((|={E1∖E2,∅}=> |={∅}▷=>^n |={∅,E1∖E2}=> P) ∗
    $p WP e @ s; E2 {{ v, P ={E1}=∗ Φ v , vp v}}) -∗
  $p WP e @ s; E1 {{ Φ, vp }}.
Proof.
  iIntros (??) "H". iApply (wp_step_fupdN_strong with "[H]"); [done |].
  iApply (and_mono_r with "H"). apply sep_mono_l. iIntros "HP".
  iMod fupd_mask_subseteq_emptyset_difference as "H"; [|iMod "HP"]; [set_solver|].
  iMod "H" as "_". replace (E1 ∖ (E1 ∖ E2)) with E2; last first.
  { set_unfold=>x. destruct (decide (x ∈ E2)); naive_solver. }
  iModIntro. iApply (step_fupdN_wand with "HP"). iIntros "H".
  iApply fupd_mask_frame; [|iMod "H"; iModIntro]; [set_solver|].
  by rewrite difference_empty_L (comm_L (∪)) -union_difference_L.
Qed.
Lemma wp_step_fupd s E1 E2 e P Φ p vp:
  TCEq (to_val e) None → E2 ⊆ E1 →
  (|={E1}[E2]▷=> P) -∗ $p WP e @ s; E2 {{ v, P ={E1}=∗ Φ v, vp v }} -∗ $p WP e @ s; E1 {{ Φ , vp }}.
Proof.
  iIntros (??) "HR H".
  iApply (wp_step_fupdN_strong 1 _ E1 E2 with "[-]"); [done|..]. iSplit.
  - iIntros (???) "_". iMod (fupd_mask_subseteq ∅) as "_"; [set_solver+|].
    auto with lia.
  - iFrame "H". iMod "HR" as "$". auto.
Qed.

Lemma wp_frame_step_l s E1 E2 e Φ R p vp:
  TCEq (to_val e) None → E2 ⊆ E1 →
  (|={E1}[E2]▷=> R) ∗ $p WP e @ s; E2 {{ Φ, vp }} ⊢ $p WP e @ s; E1 {{ v, R ∗ Φ v , vp v}}.
Proof.
  iIntros (??) "[Hu Hwp]". iApply (wp_step_fupd with "Hu"); try done.
  iApply (wp_mono with "Hwp"). by iIntros (?) "$$".
Qed.
Lemma wp_frame_step_r s E1 E2 e Φ R p vp:
  TCEq (to_val e) None → E2 ⊆ E1 →
  $p WP e @ s; E2 {{ Φ, vp }} ∗ (|={E1}[E2]▷=> R) ⊢ $p WP e @ s; E1 {{ v, Φ v ∗ R , vp v}}.
Proof.
  rewrite [($_ WP _ @ _; _ {{ _ , _}} ∗ _)%I]comm; setoid_rewrite (comm _ _ R).
  apply wp_frame_step_l.
Qed.
Lemma wp_frame_step_l' s E e Φ R p vp:
  TCEq (to_val e) None → ▷ R ∗ $p WP e @ s; E {{ Φ, vp }} ⊢ $p WP e @ s; E {{ v, R ∗ Φ v , vp v}}.
Proof. iIntros (?) "[??]". iApply (wp_frame_step_l s E E); try iFrame; eauto. Qed.
Lemma wp_frame_step_r' s E e Φ R p vp:
  TCEq (to_val e) None → $p WP e @ s; E {{ Φ, vp }} ∗ ▷ R ⊢ $p WP e @ s; E {{ v, Φ v ∗ R , vp v }}.
Proof. iIntros (?) "[??]". iApply (wp_frame_step_r s E E); try iFrame; eauto. Qed.

Lemma wp_wand s E e Φ Ψ p vp:
  $p WP e @ s; E {{ Φ , vp }} -∗ (∀ v, Φ v -∗ Ψ v) -∗ $p WP e @ s; E {{ Ψ , vp}}.
Proof.
  iIntros "Hwp H". iApply (wp_strong_mono with "Hwp"); auto.
    -lra.
    - iIntros. iPureIntro. lra. 
    - iIntros (?) "?". by iApply "H".
Qed.
Lemma wp_wand_l s E e Φ Ψ p vp:
  (∀ v, Φ v -∗ Ψ v) ∗ $p WP e @ s; E {{ Φ , vp}} ⊢ $p WP e @ s; E {{ Ψ , vp}}.
Proof. iIntros "[H Hwp]". iApply (wp_wand with "Hwp H"). Qed.
Lemma wp_wand_r s E e Φ Ψ p vp:
  $p WP e @ s; E {{ Φ, vp }} ∗ (∀ v, Φ v -∗ Ψ v) ⊢ $p WP e @ s; E {{ Ψ , vp}}.
Proof. iIntros "[Hwp H]". iApply (wp_wand with "Hwp H"). Qed.
Lemma wp_frame_wand s E e Φ R p vp:
  R -∗ $p WP e @ s; E {{ v, R -∗ Φ v, vp v }} -∗ $p WP e @ s; E {{ Φ , vp}}.
Proof.
  iIntros "HR HWP". iApply (wp_wand with "HWP").
  iIntros (v) "HΦ". by iApply "HΦ".
Qed.
End wp.

(** Proofmode class instances *)
Section proofmode_classes.
  Context `{!irisGS_gen Λ Σ}.
  Implicit Types P Q : iProp Σ.
  Implicit Types Φ : val Λ → iProp Σ.
  Implicit Types v : val Λ.
  Implicit Types e : expr Λ.

  Global Instance frame_wp pp s E e R Φ Ψ p vp:
    (∀ v, Frame pp R (Φ v) (Ψ v)) →
    Frame pp R ($p WP e @ s; E {{ Φ , vp}}) ($p WP e @ s; E {{ Ψ, vp }}) | 2.
  Proof. rewrite /Frame=> HR. rewrite wp_frame_l. apply wp_mono, HR. Qed.

  Global Instance is_except_0_wp s E e Φ p vp: IsExcept0 ($p WP e @ s; E {{ Φ, vp }}).
  Proof.
  rewrite /IsExcept0. by rewrite /IsExcept0 -{2}fupd_wp -except_0_fupd -fupd_intro. Qed.



  Global Instance elim_modal_bupd_wp pp s E e P Φ p vp:
    ElimModal True pp false (|==> P) P ($p WP e @ s; E {{ Φ , vp }}) ($p WP e @ s; E {{ Φ , vp }}).
  Proof.
    by rewrite /ElimModal intuitionistically_if_elim
      (bupd_fupd E) fupd_frame_r wand_elim_r fupd_wp.
  Qed.

  Global Instance elim_modal_fupd_wp pp s E e P Φ p vp:
    ElimModal True pp false (|={E}=> P) P ($p WP e @ s; E {{ Φ, vp }}) ($p WP e @ s; E {{ Φ, vp }}).
  Proof.
    by rewrite /ElimModal intuitionistically_if_elim
      fupd_frame_r wand_elim_r fupd_wp.
  Qed.

  Global Instance elim_modal_fupd_wp_atomic pp s E1 E2 e P Φ p vp:
    ElimModal (Atomic (stuckness_to_atomicity s) e) pp false
            (|={E1,E2}=> P) P
            ($p WP e @ s; E1 {{ Φ, vp }}) ($p WP e @ s; E2 {{ v, |={E2,E1}=> Φ v , vp v}})%I | 100.
  Proof.
    intros ?. by rewrite intuitionistically_if_elim
      fupd_frame_r wand_elim_r wp_atomic.
  Qed.

  Global Instance add_modal_fupd_wp s E e P Φ p vp:
    AddModal (|={E}=> P) P ($p WP e @ s; E {{ Φ , vp}}).
  Proof. by rewrite /AddModal fupd_frame_r wand_elim_r fupd_wp. Qed.

  Global Instance elim_acc_wp_atomic {X} E1 E2 α β γ e s Φ p vp:
    ElimAcc (X:=X) (Atomic (stuckness_to_atomicity s) e)
            (fupd E1 E2) (fupd E2 E1)
            α β γ ($p WP e @ s; E1 {{ Φ , vp}})
            (λ x, $p WP e @ s; E2 {{ v, |={E2}=> β x ∗ (γ x -∗? Φ v) , vp v}})%I | 100.
  Proof.
    iIntros (?) "Hinner >Hacc". iDestruct "Hacc" as (x) "[Hα Hclose]".
    iApply (wp_wand with "(Hinner Hα)").
    iIntros (v) ">[Hβ HΦ]". iApply "HΦ". by iApply "Hclose".
  Qed.

  Global Instance elim_acc_wp_nonatomic {X} E α β γ e s Φ p vp:
    ElimAcc (X:=X) True (fupd E E) (fupd E E)
            α β γ ($p WP e @ s; E {{ Φ , vp }})
            (λ x, $p WP e @ s; E {{ v, |={E}=> β x ∗ (γ x -∗? Φ v) , vp v }})%I.
  Proof.
    iIntros (_) "Hinner >Hacc". iDestruct "Hacc" as (x) "[Hα Hclose]".
    iApply wp_fupd.
    iApply (wp_wand with "(Hinner Hα)").
    iIntros (v) ">[Hβ HΦ]". iApply "HΦ". by iApply "Hclose".
  Qed.
End proofmode_classes.
