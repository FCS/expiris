(** Some derived lemmas for ectx-based languages *)
From iris.proofmode Require Import proofmode.
From iris.program_logic Require Export ectx_language weakestpre lifting.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.

Section wp.
Context {Λ : ectxLanguage} `{!irisGS_gen Λ Σ} {Hinh : Inhabited (state Λ)}.
Implicit Types s : stuckness.
Implicit Types P : iProp Σ.
Implicit Types Φ : val Λ → iProp Σ.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.
Implicit Types p : R.
Implicit Types vp: val Λ → R.
Local Hint Resolve head_prim_reducible head_reducible_prim_step : core.
Local Definition reducible_not_val_inhabitant e := reducible_not_val e inhabitant.
Local Hint Resolve reducible_not_val_inhabitant : core.
Local Hint Resolve head_stuck_stuck : core.

Lemma wp_lift_head_step_fupd {s E Φ} e1 p vp:
to_val e1 = None →
( ∀ σ1 ns nt,
state_interp σ1 ns nt ={E,∅}=∗
  ⌜ (p >= 0)%R ⌝ ∗ 
  ⌜head_reducible e1 σ1⌝ ∗
    ▷ ∀ D, ⌜head_step e1 σ1 D⌝ 
    ={∅}=∗  ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∗ 
    [∧ list] i ↦ d; p' ∈ (domD D); pl,
    ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
    ∃ p'' pl', ⌜(p'' + R_list_sum pl' <= p')%R⌝ ∗
    |={∅,E}=> state_interp σ2 (S ns) (length efs + nt) ∗ 
    $p'' WP e2 @ s; E {{ Φ, vp }} ∗
    [∗ list] i ↦ ef; p''' ∈ efs; pl', $p''' WP ef @ s; ⊤ {{ fork_post, (λ _, R0) }} )
⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof.
  iIntros (?) "H". iApply wp_lift_step_fupd=>//. iIntros (σ1 ns nt) "Hσ".
  iMod ("H" with "Hσ") as "[% [% H]]"; iModIntro. iSplit; auto.
  iSplit; first by destruct s; eauto. iIntros (D ?).
  iApply "H"; eauto.
Qed.

(* Lemma wp_lift_head_step {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 ns κ κs nt, state_interp σ1 ns (κ ++ κs) nt ={E,∅}=∗
    ⌜head_reducible e1 σ1⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ -∗ £ 1 ={∅,E}=∗
      state_interp σ2 (S ns) κs (length efs + nt) ∗
      WP e2 @ s; E {{ Φ }} ∗
      [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ fork_post }})
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_head_step_fupd; [done|]. iIntros (?????) "?".
  iMod ("H" with "[$]") as "[$ H]". iIntros "!>" (e2 σ2 efs ?) "Hcred !> !>". by iApply "H".
Qed. *)

Lemma wp_lift_head_stuck E Φ e p vp:
  (p >= 0)%R → 
  to_val e = None →
  sub_redexes_are_values e →
  (∀ σ ns nt, state_interp σ ns nt ={E,∅}=∗ ⌜head_stuck e σ⌝)
  ⊢ $p WP e @ E ?{{ Φ , vp }}.
Proof.
  iIntros (???) "H". iApply wp_lift_stuck; auto.
  iIntros (σ ns nt) "Hσ". iMod ("H" with "Hσ") as "%". by auto.
Qed.

Lemma wp_lift_pure_head_stuck E Φ e p vp:
  (p >= 0)%R → 
  to_val e = None →
  sub_redexes_are_values e →
  (∀ σ, head_stuck e σ) →
  ⊢ $p WP e @ E ?{{ Φ, vp }}.
Proof using Hinh.
  iIntros (??? Hstuck). iApply wp_lift_head_stuck; auto.
  iIntros (σ ns nt) "_". iApply fupd_mask_intro; by auto with set_solver.
Qed.

(* Lemma wp_lift_atomic_head_step_fupd {s E1 E2 Φ} e1 :
  to_val e1 = None →
  (∀ σ1 ns nt, state_interp σ1 ns  nt ={E1}=∗
    ⌜head_reducible e1 σ1⌝ ∗
    ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ -∗ £ 1 ={E1}[E2]▷=∗
      state_interp σ2 (S ns) κs (length efs + nt) ∗
      from_option Φ False (to_val e2) ∗
      [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ fork_post }})
  ⊢ WP e1 @ s; E1 {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_step_fupd; [done|].
  iIntros (σ1 ns κ κs nt) "Hσ1". iMod ("H" with "Hσ1") as "[% H]"; iModIntro.
  iSplit; first by destruct s; auto. iIntros (e2 σ2 efs Hstep).
  iApply "H"; eauto.
Qed.

Lemma wp_lift_atomic_head_step {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 ns κ κs nt, state_interp σ1 ns (κ ++ κs) nt ={E}=∗
    ⌜head_reducible e1 σ1⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ -∗ £ 1 ={E}=∗
      state_interp σ2 (S ns) κs (length efs + nt) ∗
      from_option Φ False (to_val e2) ∗
      [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ fork_post }})
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_step; eauto.
  iIntros (σ1 ns κ κs nt) "Hσ1". iMod ("H" with "Hσ1") as "[% H]"; iModIntro.
  iSplit; first by destruct s; auto. iNext. iIntros (e2 σ2 efs Hstep).
  iApply "H"; eauto.
Qed.

Lemma wp_lift_atomic_head_step_no_fork_fupd {s E1 E2 Φ} e1 :
  to_val e1 = None →
  (∀ σ1 ns κ κs nt, state_interp σ1 ns (κ ++ κs) nt ={E1}=∗
    ⌜head_reducible e1 σ1⌝ ∗
    ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ -∗ £ 1 ={E1}[E2]▷=∗
      ⌜efs = []⌝ ∗ state_interp σ2 (S ns) κs nt ∗ from_option Φ False (to_val e2))
  ⊢ WP e1 @ s; E1 {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_head_step_fupd; [done|].
  iIntros (σ1 ns κ κs nt) "Hσ1". iMod ("H" $! σ1 with "Hσ1") as "[$ H]"; iModIntro.
  iIntros (v2 σ2 efs Hstep) "Hcred".
  iMod ("H" $! v2 σ2 efs with "[# //] Hcred") as "H".
  iIntros "!> !>". iMod "H" as "(-> & ? & ?) /=". by iFrame.
Qed.

Lemma wp_lift_atomic_head_step_no_fork {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 ns κ κs nt, state_interp σ1 ns (κ ++ κs) nt ={E}=∗
    ⌜head_reducible e1 σ1⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜head_step e1 σ1 κ e2 σ2 efs⌝ -∗ £ 1 ={E}=∗
      ⌜efs = []⌝ ∗ state_interp σ2 (S ns) κs nt ∗ from_option Φ False (to_val e2))
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_head_step; eauto.
  iIntros (σ1 ns κ κs nt) "Hσ1". iMod ("H" $! σ1 with "Hσ1") as "[$ H]"; iModIntro.
  iNext; iIntros (v2 σ2 efs Hstep) "Hcred".
  iMod ("H" $! v2 σ2 efs with "[//] Hcred") as "(-> & ? & ?) /=". by iFrame.
Qed. *)

Lemma wp_lift_pure_det_head_step_no_fork {s E E' Φ} e1 e2 p vp:
(p >= 0)%R → 
(∀ σ1, head_reducible e1 σ1) →
(∀ σ1 D, head_step e1 σ1 D → D = init_single (e2, σ1, R0, [])) → 
( |={E}[E']▷=> $p WP e2 @ s; E {{ Φ ,vp }}) ⊢ $p WP e1 @ s; E {{ Φ, vp }}.
Proof using Hinh.
  intros. rewrite -(wp_lift_pure_det_step_no_fork e1 e2); eauto.
  destruct s; by auto.
Qed.

Lemma wp_lift_det_head_step_no_fork `{!Inhabited (state Λ)} s E E' Φ e1 p vp:
(p >= 0)%R → 
(to_val e1 = None) → 
(∀ σ ns nt, state_interp σ ns nt -∗ ⌜ head_reducible e1 σ⌝  ∗
⌜ ∀ D, head_step e1 σ D → ∃ e2 σ2,  D = init_single (e2, σ2, R0, [])⌝  ∗ 
|={E}[E']▷=> ∀ e2 σ2, ⌜ head_step e1 σ (init_single (e2, σ2, R0, [])) ⌝  -∗ 
|={E}=> state_interp σ2 (S ns) nt ∗ $p WP e2 @ s; E {{ Φ , vp }})
⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof using Hinh.
  intros. rewrite -(wp_lift_det_step_no_fork s E); eauto.
  - iIntros "H" (σ ns nt) "Hσ". 
    iDestruct ("H" with "Hσ") as "[% [% H]]". 
    iSplit; first (destruct s; eauto).
    iSplit.
    {iPureIntro. intros. apply H2. eauto. }
    iApply (step_fupd_wand with "H").
    iIntros "H" (e2 σ2 Hstep). iApply "H". eauto.
Qed.

Lemma wp_lift_pure_det_head_step_no_fork' {s E Φ} e1 e2 p vp:
(p >= 0)%R → 
(∀ σ1, head_reducible e1 σ1) →
(∀ σ1 D, head_step e1 σ1 D → D = init_single (e2, σ1, R0, [])) → 
( ▷ $p WP e2 @ s; E {{ Φ ,vp }}) ⊢ $p WP e1 @ s; E {{ Φ, vp }}.
Proof using Hinh.
  intros. rewrite -[($p WP e1 @ s; _ {{ _, _ }})%I]wp_lift_pure_det_head_step_no_fork //.
  rewrite -step_fupd_intro //.
Qed.
End wp.
