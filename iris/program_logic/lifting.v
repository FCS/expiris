(** The "lifting lemmas" in this file serve to lift the rules of the operational
semantics to the program logic. *)

From iris.proofmode Require Import proofmode.
From iris.program_logic Require Export weakestpre.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.
Require Import Lra.

Section lifting.
Context `{!irisGS_gen Λ Σ}.
Implicit Types s : stuckness.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.
Implicit Types σ : state Λ.
Implicit Types P Q : iProp Σ.
Implicit Types Φ : val Λ → iProp Σ.

Lemma wp_lift_step_fupdN s E Φ e1 p vp :
  to_val e1 = None →
  ( ∀ σ1 ns nt,
     state_interp σ1 ns nt ={E,∅}=∗
       ⌜ (p >= 0)%R ⌝ ∗ 
       ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
        ∀ D, ⌜prim_step e1 σ1 D⌝ 
         ={∅}▷=∗^(S $ num_laters_per_step ns) 
         ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∗ 
         [∧ list] i ↦ d; p' ∈ (domD D); pl,
         ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
         ∃ p'' pl', ⌜ (p'' + R_list_sum pl' <= p')%R ⌝  ∗ 
         |={∅,E}=> state_interp σ2 (S ns) (length efs + nt) ∗
         $p'' WP e2 @ s; E {{ Φ , vp }} ∗
         [∗ list] i ↦ ef; p''' ∈ efs; pl', $p''' WP ef @ s; ⊤ {{  fork_post , (λ _, R0) }} )
  ⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof. by rewrite wp_unfold /wp_pre=>->. Qed.


Lemma wp_lift_step_fupd s E Φ e1 p vp:
  to_val e1 = None →
  ( ∀ σ1 ns nt,
  state_interp σ1 ns nt ={E,∅}=∗
    ⌜ (p >= 0)%R ⌝ ∗ 
    ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
      ▷ ∀ D, ⌜prim_step e1 σ1 D⌝ 
      ={∅}=∗  ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∗ 
      [∧ list] i ↦ d; p' ∈ (domD D); pl,
      ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
      ∃ p'' pl', ⌜(p'' + R_list_sum pl' <= p')%R⌝ ∗
      |={∅,E}=> state_interp σ2 (S ns) (length efs + nt) ∗ 
      $p'' WP e2 @ s; E {{ Φ, vp }} ∗
      [∗ list] i ↦ ef; p''' ∈ efs; pl', $p''' WP ef @ s; ⊤ {{ fork_post, (λ _, R0) }} )
  ⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof.
  iIntros (?) "Hwp". rewrite -wp_lift_step_fupdN; [|done].
  iIntros (???) "Hσ". iMod ("Hwp" with "Hσ") as "($ & $ & Hwp)".
  iIntros "!>" (??) . 
  simpl. iModIntro. iNext.
  iSpecialize ("Hwp" $! D with "[//]").
  simpl. iMod "Hwp".
  rewrite -step_fupdN_intro; [|done]. rewrite -bi.laterN_intro.
  repeat iModIntro. done. 
Qed.

Lemma wp_lift_stuck E Φ e p vp:
  (p >= 0)%R → 
  to_val e = None → 
  (∀ σ ns nt, state_interp σ ns nt ={E,∅}=∗ ⌜stuck e σ⌝)
  ⊢ $p WP e @ E ?{{ Φ , vp }}.
Proof.
  intros Hp.
  rewrite wp_unfold /wp_pre=>->. iIntros "H" (σ1 ns nt) "Hσ".
  iMod ("H" with "Hσ") as %[? Hirr]. iModIntro. repeat (iSplit; first done).
  iIntros (D Hstep). unfold irreducible in *. by contradict Hstep.
Qed.

(** Derived lifting lemmas. *)

(* TODO find out how this is used *)
(* Lemma wp_lift_step s E Φ e1 p vp:
to_val e1 = None →
( ∀ σ1 ns nt,
state_interp σ1 ns nt ={E,∅}=∗
  ⌜ (p >= 0)%R ⌝ ∗ 
  ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
    ▷ ∀ D, ⌜prim_step e1 σ1 D⌝ -∗ £ 1 
    ={∅}=∗ ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∗ 
    [∧ list] i ↦ d; p' ∈ (domD D); pl,
    ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
    ∃ p'' pl', ⌜(p'' + R_list_sum pl' <= p')%R⌝ ∗
    |={∅,E}=> state_interp σ2 (S ns) (length efs + nt) ∗ 
    $p'' WP e2 @ s; E {{ Φ, vp }} ∗
    [∗ list] i ↦ ef; p''' ∈ efs; pl', $p''' WP ef @ s; ⊤ {{ fork_post, (λ _, R0) }} )
⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof.
  iIntros (?) "H". iApply wp_lift_step_fupd; [done|]. iIntros (???) "Hσ".
  iMod ("H" with "Hσ") as "[$ [$ H]]". iIntros "!> * % Hcred ". iApply "H".
Qed. *)

Lemma wp_lift_det_step_no_fork `{!Inhabited (state Λ)} s E E' Φ e1 p vp:
(p >= 0)%R → 
(to_val e1 = None) → 
(∀ σ ns nt, state_interp σ ns nt -∗ 
 ⌜ if s is NotStuck then reducible e1 σ else to_val e1 = None ⌝ ∗ 
 ⌜ (∀ D, prim_step e1 σ D → ∃ e2 σ2,  D = init_single (e2, σ2, R0, [])) ⌝ ∗ 
|={E}[E']▷=> ∀ e2 σ2, ⌜ prim_step e1 σ (init_single (e2, σ2, R0, [])) ⌝  -∗ 
|={E}=> state_interp σ2 (S ns) nt ∗ $p WP e2 @ s; E {{ Φ , vp }})
⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof.
  iIntros (Hp Hv) "H". iApply wp_lift_step_fupd; auto.
  iIntros (σ1 ns nt) "Hσ". iDestruct ("H" with "Hσ") as "[% [%Hstep H]]".
  iMod "H". iApply fupd_mask_intro; first set_solver. iIntros "Hclose". iSplit; first done. iSplit.
  { iPureIntro. destruct s; done. }
  iNext. iIntros (D Hs).
  destruct (Hstep D Hs) as [e2 [σ2 ->]].
  iModIntro. iExists [p]. iSplitR.
  { unfold expec_cost. rewrite init_single_expec. rewrite init_single_expecL.
  iPureIntro. simpl. lra. }
  simpl. iSplit; auto. iIntros (? ? ? ? ?). simplify_eq.
  simpl. iExists p, []. iSplit.
  {iPureIntro. simpl. lra. }
  iMod "Hclose" as "_". iMod "H".
  iMod ("H" $! _ _ Hs) as "[$ $]". 
  naive_solver.
Qed.

Lemma wp_lift_pure_step_no_fork `{!Inhabited (state Λ)} s E E' Φ e1 p vp:
  (p >= 0)%R → 
  (∀ σ1, if s is NotStuck then reducible e1 σ1 else to_val e1 = None) →
  (∀ σ1 D, prim_step e1 σ1 D → ∃ e2,  D = init_single (e2, σ1, R0, [])) → 
  (|={E}[E']▷=> ∀ e2 σ, ⌜ prim_step e1 σ (init_single (e2, σ, R0, [])) ⌝  -∗ $p WP e2 @ s; E {{ Φ , vp }})
  ⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof.
  iIntros (Hp Hsafe Hstep) "H". iApply wp_lift_det_step_no_fork; auto.
  { specialize (Hsafe inhabitant). destruct s; eauto using reducible_not_val. }
  iIntros (σ ns nt) "Hσ".
  iSplit; first naive_solver.
  iSplit; first (iPureIntro; naive_solver).
  iApply step_fupd_fupd.
  iApply (step_fupd_wand with "H"). iIntros "H".
  iApply (fupd_mask_mono ∅); first set_solver.
  iMod (state_interp_mono with "Hσ") as "Hσ ".
  iIntros "!>" (e2 σ2 Hstep'). 
  destruct (Hstep _ _ Hstep') as [? ?].
  simplify_eq. iFrame. by iApply "H".
Qed.

Lemma wp_lift_pure_stuck `{!Inhabited (state Λ)} E Φ e p vp:
  (p >= 0)%R → 
  (∀ σ, stuck e σ) →
  True ⊢ $p WP e @ E ?{{ Φ , vp }}.
Proof.
  iIntros (Hp Hstuck) "_". iApply wp_lift_stuck.
  - done.
  - destruct(to_val e) as [v|] eqn:He; last done.
    rewrite -He. by case: (Hstuck inhabitant).
  - iIntros (σ ns nt) "_". iApply fupd_mask_intro; auto with set_solver.
Qed.

(* TODO find out how this is used Atomic steps don't need any mask-changing business here, one can
   use the generic lemmas here.
Lemma wp_lift_atomic_step_fupd {s E1 E2 Φ} e1 p vp:
  to_val e1 = None →
  (∀ σ1 ns nt, state_interp σ1 ns nt ={E1}=∗
  ⌜ (p >= 0)%R ⌝ ∗ 
  ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
  ∀ D, ⌜prim_step e1 σ1 D⌝ -∗ 
    ▷ |={E1}=>
    ∃ pl, ⌜(expec_cost D + expecL D pl <= p)%R ⌝ ∗  
    [∧ list] i ↦ d; p' ∈ (domD D); pl,
    ∀ e2 σ2 c efs, ⌜d = (e2, σ2, c, efs)⌝  -∗ 
    ∃ p'' pl', ⌜ (p'' + R_list_sum pl' <= p')%R ⌝  ∗ 
    |={∅,E2}=> state_interp σ2 (S ns) (length efs + nt) ∗
    from_option Φ False (to_val e2) ∗
    [∗ list] i ↦ ef; p''' ∈ efs; pl', $p''' WP ef @ s; ⊤ {{ fork_post,  (λ _, R0) }} )
  ⊢ $p WP e1 @ s; E1 {{ Φ,vp }}.
Proof.
  iIntros (?) "H".
  iApply (wp_lift_step_fupd s E1 _ e1)=>//; iIntros (σ1 ns nt) "Hσ1".
  iMod ("H" $! σ1 with "Hσ1") as "[$ [$ H]]".
  iMod ("H" $! D with "[#]") as "H"; [done|].
  iApply fupd_mask_intro; first set_solver.
  iIntros "Hclose" (D ?).
  
  iMod "Hclose" as "_".
  
  iApply fupd_mask_intro; first set_solver. iIntros "Hclose !>".
  iMod "Hclose" as "_". iMod "H" as "($ & HQ & $)".
  destruct (to_val e2) eqn:?; last by iExFalso.
  iApply wp_value; last done. by apply of_to_val.
Qed.

Lemma wp_lift_atomic_step {s E Φ} e1 :
  to_val e1 = None →
  (∀ σ1 ns κ κs nt, state_interp σ1 ns (κ ++ κs) nt ={E}=∗
    ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
    ▷ ∀ e2 σ2 efs, ⌜prim_step e1 σ1 κ e2 σ2 efs⌝ -∗ £ 1 ={E}=∗
      state_interp σ2 (S ns) κs (length efs + nt) ∗
      from_option Φ False (to_val e2) ∗
      [∗ list] ef ∈ efs, WP ef @ s; ⊤ {{ fork_post }})
  ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  iIntros (?) "H". iApply wp_lift_atomic_step_fupd; [done|].
  iIntros (?????) "?". iMod ("H" with "[$]") as "[$ H]".
  iIntros "!> *". iIntros (Hstep) "Hcred !> !>".
  by iApply "H".
Qed. *)

Lemma wp_lift_pure_det_step_no_fork `{!Inhabited (state Λ)} {s E E' Φ} e1 e2 p vp:
  (p >= 0)%R → 
  (∀ σ1, if s is NotStuck then reducible e1 σ1 else to_val e1 = None) →
  (∀ σ1 D, prim_step e1 σ1 D → D = init_single (e2, σ1, R0, [])) → 
  ( |={E}[E']▷=> $p WP e2 @ s; E {{ Φ ,vp }}) ⊢ $p WP e1 @ s; E {{ Φ, vp }}.
Proof.
  iIntros (Hp ? Hpuredet) "H". iApply (wp_lift_pure_step_no_fork s E E'); try done.
  {intros ? ? Hstep. eexists. by eapply Hpuredet. }
  iApply (step_fupd_wand with "H"); iIntros "H".
  iIntros (e' σ Hstep); auto.
  specialize (Hpuredet σ ((init_single (e', σ, R0, []))) Hstep).
  simplify_eq. by iApply "H".
Qed.

Lemma wp_pure_step_fupd `{!Inhabited (state Λ)} s E E' e1 e2 φ n Φ p vp:
  (p >= 0)%R → 
  PureExec φ n e1 e2 →
  φ →
  (|={E}[E']▷=>^n $p WP e2 @ s; E {{ Φ, vp }}) ⊢ $p WP e1 @ s; E {{ Φ , vp }}.
Proof.
  iIntros (Hp Hexec Hφ) "Hwp". specialize (Hexec Hφ).
  iInduction Hexec as [e|n e1 e2 e3 [Hsafe ?]] "IH"; simpl.
  { iMod lc_zero as "Hz". by iApply "Hwp". }
  iApply wp_lift_pure_det_step_no_fork; auto.
  - intros σ. specialize (Hsafe σ). destruct s; eauto using reducible_not_val.
  - by iApply "IH". 
Qed.

Lemma wp_pure_step_later `{!Inhabited (state Λ)} s E e1 e2 φ n Φ p vp:
  (p >= 0)%R → 
  PureExec φ n e1 e2 →
  φ →
  ▷^n ($p WP e2 @ s; E {{ Φ , vp }}) ⊢ $p WP e1 @ s; E {{ Φ, vp }}.
Proof.
  intros Hp Hexec ?. rewrite -wp_pure_step_fupd //; auto. clear Hexec.
  enough (∀ P, ▷^n P ⊢ |={E}▷=>^n P) as Hwp by apply Hwp. intros ?.
  induction n as [|n IH]; by rewrite //= -step_fupd_intro // IH.
Qed.
End lifting.
