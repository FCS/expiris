Require Export Coq.Reals.RIneq.
From stdpp Require Import tactics list.
From iris.prelude Require Import options.
Require Import Lra.
Require Eqdep_dec.

Local Open Scope R_scope.

(* Sums over Lists of Reals *)
Definition R_list_sum := foldr Rplus 0. 

Lemma R_list_sum_div l p :
    R_list_sum (map (λ q, q / p) l) = (R_list_sum l) / p.
Proof.
    induction l as [|x L IH]; simpl; lra.
Qed.

Lemma R_list_sum_mult {A} l r (f : A -> R):
    R_list_sum (map (λ r', f r' * r) l) = (R_list_sum (map f l)) * r. 
Proof.
    induction l as [|x L IH]; simpl; lra.
Qed. 

Lemma Rlist_geq_0 l: l <> [] -> Forall (fun r => r > 0) l -> R_list_sum l > 0.
Proof.
    induction l as [| r' l']. 
        - done.
        - intros _ H. inversion H. simpl. apply Rplus_pos_nneg; auto. destruct l'.
        + simpl. lra.
        + apply Rgt_ge. apply IHl'; auto.
Qed.

Lemma Rlist_0 l: Forall (λ r, r >= 0) l → R_list_sum l >= 0.
Proof.
    induction l as [| r' l']; simpl. 1:lra. rewrite Forall_cons_iff.
    intros []. enough (R_list_sum l' >= 0) by lra. tauto.
Qed.

Lemma R_list_sum_same q l :
    (Forall (fun x => x = q) l) → R_list_sum l = INR (length l) * q.
Proof.
    intros H. induction l as [|x l IH].
    - simpl. lra.
    - simpl in *. inversion H. rewrite H2. rewrite IH; auto. destruct l; simpl; lra.
Qed.

Lemma R_list_sum_app l1 l2:
    R_list_sum (l1 ++ l2) = R_list_sum l1 + R_list_sum l2.
Proof.
    induction l1; simpl; lra.
Qed.

Lemma R_list_sum_leq r l : Forall (λ r', (r' >= 0)%R) l → (R_list_sum l <= r)%R → Forall (λ r', (r' <= r)) l. 
Proof.
    induction l in r |-*; auto. simpl. intros H_forall Hsum.
    rewrite Forall_cons_iff in *. destruct H_forall as [Ha H_forall].
    split. 2:apply IHl; auto. 2:lra.
    enough (R_list_sum l >= 0) by lra. by apply Rlist_0.
Qed.

Lemma R_list_sum_mono l1 l2: 
Forall2 (λ x1 x2, x1 <= x2) l1 l2 → 
R_list_sum l1 <= R_list_sum l2.
Proof.
    intros Hall. specialize (Forall2_length _ _ _ Hall) as Hlen.
    induction l1 in l2, Hall, Hlen |-*; destruct l2; try done. 1:lra.
    rewrite Forall2_cons in Hall. destruct Hall.
    simpl. apply Rplus_le_compat; auto.
Qed.

Lemma R_list_sum_mono2 {A} (l : list A) f1 f2:
 (∀ a, a ∈ l → f1 a <= f2 a) → 
 R_list_sum (map f1 l) <= R_list_sum (map f2 l).
 Proof.
    intros Hall. induction l; simpl. 1: lra.
    apply Rplus_le_compat.
    - apply Hall. left.
    - apply IHl. intros. apply Hall. by right.
Qed.

Lemma R_list_sum_add {A} (l: list A) f1 f2:
R_list_sum (map (λ x, f1 x + f2 x) l) = R_list_sum (map f1 l) + R_list_sum (map f2 l).
Proof.
    induction l; simpl; lra.
Qed.

Lemma R_list_sum_perm (l1 l2: list R)  : l1 ≡ₚ l2 → R_list_sum l1 = R_list_sum l2.
Proof.
    intros H. induction H; simpl; lra.
Qed.


(*TODO Those lemmata should already exist somewhere else*)
Lemma map_id {A} (l : list A) : map (λ x, x) l = l.
Proof.
    induction l; auto. simpl. rewrite IHl. done.
Qed.

Lemma map_lookup_Some {A B} (l : list A) k (f : A -> B) b : map f l !! k = Some b → ∃ a, l !! k = Some a ∧ b = f a.
Proof.
    induction l  in k |- *. 1:done. simpl. destruct k; simpl. 2:apply IHl.
    exists a. by injection H.
Qed.

(* Use boolean decider to avoid assuming proof irrelevance*)

Fixpoint decide_geq0 {A} (l : list (R*A)) :=
    match l with 
        | [] => true 
        | (r, a)::l => if (Rgt_le_dec r 0) then decide_geq0 l else false
    end.

Lemma decide_geq0_equiv {A} (l : list (R*A)) : decide_geq0 l = true ↔ Forall (fun d => d.1 > 0) l. 
Proof.
    induction l as [|[r a] l [IH1 IH2]]. 1:done. simpl. rewrite Forall_cons. simpl. split; destruct (Rgt_le_dec r 0); auto.
        + discriminate.
        + intros [_ H]. by apply IH2.
        + intros [H _]. lra.
Qed.

Lemma decide_geq0_1 {A} (l : list (R*A)) : decide_geq0 l = true -> Forall (fun d => d.1 > 0) l. 
Proof. apply decide_geq0_equiv. Qed. 

Lemma decide_geq0_2 {A} (l : list (R*A)) : Forall (fun d => d.1 > 0) l -> decide_geq0 l = true.
Proof. apply decide_geq0_equiv. Qed. 


(* Distributions *)

Record distribution {A} := mkDst {
    distr : list (R * A) ;
    g0 : decide_geq0 distr = true;
    sums_to_one : R_list_sum (map fst distr) = 1
}.

Definition domD {A} (D : @distribution A) := map snd (distr D).

Lemma domD_notnil {A} (D : @distribution A) : domD D ≠ [].
Proof.
    destruct D as [l ? sum1]. unfold domD. simpl. destruct l; auto.
    by contradict sum1.
Qed.

Local Instance g0_proof_irrel {A} (l : list (R * A)) : ProofIrrel (decide_geq0 l = true).
Proof.
    intros H H'.  apply Eqdep_dec.eq_proofs_unicity. intros [] []; lia.
Qed.

Local Instance sum1_proof_irrel {A} (l : list (R * A)) : ProofIrrel (R_list_sum (map fst l) = 1).
Proof.
 intros H H'. apply Eqdep_dec.eq_proofs_unicity. intros. apply Req_dec.
Qed.

Lemma distr_eq {A} (D D' : @distribution A) : (distr D) = (distr D') -> D = D'.
Proof.
    destruct D, D'. simpl in *. intros ->.  rewrite (proof_irrel g1 g2).
    rewrite (proof_irrel sums_to_one0 sums_to_one1). done.
Qed.

Program Definition distr_map {A B} (D: distribution) (f: A -> B) := mkDst B (map (fun d => (d.1, f d.2)) D.(distr)) _ _.
Next Obligation.
 destruct D as [distr g0 sum1]. intros f. rewrite decide_geq0_2; auto. apply Forall_map. eapply Forall_impl. 
    - apply decide_geq0_1. exact g0.
    - simpl in *. intros [r a]. done.
Qed.
Next Obligation. 
    destruct D as [distr gt0 sum1]. simpl. intros. rewrite <- sum1. clear. induction distr as [| [p a]IH].
    - done.
    - simpl. f_equal. done.
Qed.

Lemma distr_map_id {A} (D : @distribution A) : distr_map D (λ x, x) = D.
Proof.
    unfold distr_map. destruct D. apply distr_eq. simpl. rewrite <- map_id.
    apply map_ext. intros []. done.
Qed.

Lemma distr_map_ext {A B} (D : @distribution A) (f g : A -> B) : (∀ x, f x = g x) -> distr_map D f = distr_map D g. 
Proof.
    intros H. unfold distr_map. apply distr_eq. simpl. apply map_ext. intros [r a].
    simpl. by rewrite (H a). 
Qed.

Lemma distr_map_map {A B C} (D : @distribution A) (f : A -> B) (g : B -> C) : distr_map (distr_map D f) g = distr_map D (fun x => g (f x)).
Proof.
    unfold distr_map. apply distr_eq. simpl. by rewrite map_map.
Qed.

Lemma distr_map_len {A B} (D : @distribution A) (f : A → B) :
 length (domD (distr_map D f)) = length (domD D).
 Proof.
    unfold distr_map. unfold domD. simpl. by repeat rewrite map_length.
Qed. 

Lemma distr_map_dom {A B} (D : @distribution A) (f : A → B) : (domD (distr_map D f)) = map f (domD D).
Proof.
    unfold domD. unfold distr_map. simpl. repeat rewrite map_map. simpl. done.
Qed.

Lemma distr_map_in {A B} (D : @distribution A) (f : A → B) b k: (domD (distr_map D f)) !! k = Some b → ∃ a, (domD D) !! k = Some a ∧ b = f a.
Proof. rewrite distr_map_dom. apply map_lookup_Some. Qed.


(* Program Definition distr_zip {A B} (D : @distribution A) (l : list B) (leneq : length l = length (distr D)) := mkDst (A*B) (map (fun x => (x.1.1, (x.1.2, x.2))) (zip (distr D) l)) _ _ .
Next Obligation.
    destruct D as [distr g0 sum1]. simpl. intros l Hl. rewrite decide_geq0_2; auto.
    simpl. apply decide_geq0_1 in g0. apply Forall_map. simpl. induction distr in g0, l, Hl |- *; destruct l; try discriminate; auto. simpl. apply Forall_cons.
    apply Forall_cons in g0; split. 1:tauto.  apply IHdistr; auto. apply g0.
Qed.
Next Obligation.
    destruct D as [distr g0 sum1]. simpl. intros l Hl. rewrite map_map. simpl.
    rewrite <- sum1. induction distr in Hl, l |- *; auto. destruct l; try discriminate. simpl. rewrite IHdistr; auto.
Qed. 

Lemma in_distr_zip {A B} (D : @distribution A) (l: list B) (leneq : length l = length (distr D)) a b: in_distr (distr_zip D l leneq) (a, b) -> in_distr D a ∧ In b l.
Proof.
    unfold in_distr. unfold distr_zip. simpl. rewrite map_map. simpl. remember (distr D) as d. clear Heqd. induction d in l, leneq |- *; simpl. 1:done.
    destruct l; try discriminate. simpl. 
*)


(* Expected Value of random variable*)

Definition expec {A} (D : @distribution A) X :=  R_list_sum (map (fun d => d.1*X d.2) D.(distr)).

Lemma expec_const {A} (D : @distribution A) r : expec D (λ _, r) = r. 
Proof. 
    unfold expec. destruct D as [l g0 sum1]. simpl. rewrite R_list_sum_mult. rewrite sum1. lra.
Qed.



Lemma expec_map {A B} (D : @distribution A) (f : A -> B) (g : B -> R): expec (distr_map D f) g = expec D (λ x, g (f x)).
Proof. unfold expec. unfold distr_map. simpl. by rewrite map_map. Qed.

Lemma expec_add {A} (D : @distribution A) f r: expec D (λ x, r + f x) = r + expec D f.
Proof.
    unfold expec. destruct D as [distr g1 sum1]. simpl. replace r with (1 * r) at 1 by lra. rewrite <- sum1. clear g1 sum1. induction distr; simpl. 1:lra. rewrite IHdistr. lra.
Qed.

Lemma expec_lin {A} (D : @distribution A) f g : expec D (λ x, f x + g x) = expec D f + expec D g. 
Proof.
    unfold expec. destruct D as [distr g1 sum1]. simpl. clear g1 sum1. induction distr; simpl. 1: lra. rewrite IHdistr. lra.
Qed.

Lemma expec_ext {A} (D : @distribution A) (f g : A → R): 
(∀ x, f x = g x) → expec D f = expec D g. 
Proof.
    unfold expec. destruct D as [distr g1 sum1]. simpl. clear g1 sum1. induction distr; simpl. 1: lra. intros H. rewrite (H a.2). f_equal. by apply IHdistr.
Qed. 

Fixpoint expecL' {A} (d: list (R* A)) (pl : list R) := match d, pl with 
    | x::d, r::pl => x.1 *r + expecL' d pl
    | _ , _ => 0
    end.

Definition expecL {A} (D : @distribution A) := expecL' (distr D).

Lemma expecL_map {A B} (D : @distribution A) pl (f : A -> B) : expecL D pl = expecL (distr_map D f) pl.
Proof.
    unfold expecL. unfold distr_map. simpl. induction (distr D) in pl |- *. 1:done.
    destruct pl; auto. simpl. f_equal. done.
Qed.

Lemma expecL_add {A} (D : @distribution A) l (p r:R):  length l = length (domD D) → expecL D (map (λ p, p + r) l) = expecL D l + r.
Proof.
    unfold expecL. destruct D as [distr g1 sum1]. unfold domD. simpl. 
    intros Hlen. enough (expecL' distr (map (λ p0 : R, p0 + r) l) = expecL' distr l + 1 * r) by lra. rewrite <- sum1.
    assert (distr ≠ nil) as Hnil.
        {destruct distr; auto. by contradict sum1. }
    clear g1 sum1. induction l in distr, Hlen, Hnil |- *; simpl; destruct distr; try by contradict Hlen. simpl. destruct distr0. 1:simpl;lra.
    cbn -[expecL']. rewrite IHl; auto. cbn -[expecL']. lra.
Qed. 

Lemma expecL_mult {A} (D : @distribution A) l (p r:R):  length l = length (domD D) → expecL D (map (λ p, p * r) l) = expecL D l * r.
Proof.
    unfold expecL. destruct D as [distr g1 sum1]. unfold domD. simpl. 
    intros Hlen. 
    clear g1 sum1. induction l in distr, Hlen |- *; simpl; destruct distr; try by contradict Hlen; simpl; destruct distr0.
    1, 2:simpl;nra.
    simpl. rewrite IHl; auto. cbn -[expecL']. nra.
Qed. 

Lemma expecL'_concat {A} (d1 d2 : list (R*A)) (pl1 pl2: list R) : length d1 = length pl1 → expecL' (d1 ++ d2) (pl1 ++ pl2) = expecL' d1 pl1 + expecL' d2 pl2.
Proof.
    induction d1 in d2, pl1, pl2 |- *; simpl; destruct pl1; try done.
        - intros. simpl. lra. 
        - intros [=] . simpl. rewrite IHd1; auto. lra.
Qed. 

Lemma expecL'_mult {A} (d1 : list (R * A)) d2 r: expecL' (map (λ a2, (a2.1 * r, a2.2)) d1) d2 = r * expecL' d1 d2.
Proof.
    induction d1 in d2 |- *; simpl; destruct d2; try lra. rewrite IHd1; lra. 
Qed.

Lemma expecL_mono {A} (l1 l2 : list R) (D:@distribution A): Forall2 (λ r1 r2, r1 >= r2) l1 l2 → expecL D l1 >= expecL D l2.
Proof.
    unfold expecL. intros H. destruct D as [distr g0 sum1]. simpl. apply decide_geq0_1 in g0. clear sum1.
    specialize (Forall2_length _ _ _ H) as Hlen.
    induction distr in l1, l2, H, Hlen, g0 |- *; destruct l1, l2; simpl; try by contradict Hlen. all: try lra.
    apply Forall2_cons in H as [H1 H2]. apply Forall_cons in g0 as [g0a g0]. apply Rplus_ge_compat. 1:nra.
    apply IHdistr; auto.
Qed.

Lemma expecL_expec {A} (D: @distribution A) f : expecL D (map f (domD D)) = expec D f.
Proof.
    unfold expecL. unfold expec. unfold domD. rewrite map_map. induction (distr D). 1:done. simpl.
    by rewrite IHl.
Qed.

(* Forall for Distributions *)

Definition ForallD {A} P (D : @distribution A) := Forall P (map snd (distr D)).

Lemma ForallD_weaken {A} P P' (D : @distribution A) : (∀ a, P a -> P' a) → ForallD P D → ForallD P' D.
Proof.
    unfold ForallD. repeat rewrite Forall_forall. intros H H' x H''. apply H. by apply H'.
Qed.

Lemma ForallD_forall {A} P (D : @distribution A) : ForallD P D ↔ ∀ a, In a (domD D)→ P a.
Proof.
    unfold ForallD, domD. induction (distr D).
        + rewrite Forall_nil. split; auto. intros _ ? H. contradiction.
        + simpl. rewrite Forall_cons. rewrite IHl. split; naive_solver.
Qed.

Lemma ForallD_map {A B} P (f : A → B) (D : @distribution A) : ForallD (λ x, P (f x)) D → ForallD P (distr_map D f).
Proof.
    unfold ForallD. unfold distr_map. simpl. by rewrite !Forall_map.
Qed. 

Lemma expec_geq_mono {A} (D : @distribution A) r f: ForallD (λ d, f d >= r) D → expec D f >= r.
Proof.
    destruct D as [l g0 sum1]. unfold ForallD. unfold expec. simpl.
    replace r with (r * 1) at 1 by lra. rewrite <- sum1.
    assert (l ≠ []) as Hnil.
    {destruct l; by contradict sum1. }
    apply decide_geq0_1 in g0.
    clear sum1. 
    induction l in Hnil, g0 |- *. 1:done. destruct l; simpl.
        - simpl in *. rewrite Forall_cons. rewrite Forall_nil. intros [? _].
        rewrite Forall_cons in g0. destruct g0 as [? Hall]. nra.
        - rewrite Forall_cons. intros [? Hall]. rewrite Forall_cons in g0.
        destruct g0 as [? g0]. feed specialize IHl; auto. simpl in IHl. nra.
Qed.

(* Initialization Programs and their Lemmata *)

Program Definition init_distr {A} (l : list (R * A)) (g0 : Forall (fun d => d.1 > 0) l) sum1 := mkDst A l _ sum1.
Next Obligation. intros. by apply decide_geq0_2. Qed.

Lemma expec_distr {A} (l : list (R*A)) g0 sum1 X : expec (init_distr l g0 sum1) X = R_list_sum (map (fun d => d.1*X d.2) l).
Proof.
    unfold expec. unfold init_distr. done.
Qed.

Lemma expecL_distr {A} (l: list (R*A)) g0 sum1 pl : length l = length pl -> expecL (init_distr l g0 sum1) pl = R_list_sum (map (fun d => d.1.1 * d.2) (zip l pl)).
Proof.
    intros Hlen. unfold init_distr. unfold expecL. simpl. clear g0 sum1. induction l in pl, Hlen |- *; try done.
    simpl. destruct pl.
        + by contradict Hlen.
        + simpl. f_equal. apply IHl. by injection Hlen.
Qed.

Lemma ForallD_distr {A} (l : list (R*A)) g0 sum1 P : ForallD P (init_distr l g0 sum1) = Forall P (map snd l).
Proof.
    unfold init_distr. unfold ForallD. done.
Qed.

Lemma dom_init_distr {A} (l : list (R*A)) g0 sum1 : domD (init_distr l g0 sum1) = (map snd l).
Proof. unfold domD. done. Qed.

(* Initialize Distribution with unnormalized list *)

Lemma Rlist_geq_0_2 {X} l: l <> [] -> Forall (fun d : (R * X) => d.1 > 0) l -> R_list_sum (map fst l) > 0.
Proof.
    intros notnil g0. apply Rlist_geq_0.
        + intros ?. contradict notnil. by eapply map_eq_nil.
        + apply Forall_map. eapply Forall_impl. 1:exact g0.
            intros [r' x']. done.
Qed.

Program Definition init_distr_unnormalized {A} (l : list (R * A)) (g0 : Forall (fun d => d.1 > 0) l) (notnil : l <> [])
    := mkDst A (map (fun d => ((d.1/R_list_sum (map fst l)), d.2)) l) _ _.
Next Obligation.
    intros A l g0 notnil. apply decide_geq0_2. apply Forall_map. simpl in *. eapply Forall_impl. 1:exact g0. intros [r a] r0. simpl in *. apply Rdiv_pos_pos. 1:done. by apply Rlist_geq_0_2.
Qed.
Next Obligation.
    intros A l H1 H2. replace (R_list_sum _ = 1) with (R_list_sum (map (λ r, r/R_list_sum (map fst l)) (map fst l)) = 1).
         + rewrite R_list_sum_div. apply Rdiv_diag. intros ?. assert (R_list_sum (map fst l) > 0). 1:by apply Rlist_geq_0_2. lra.
        + remember (R_list_sum (map fst l)) as s. clear.
        repeat f_equal. induction l as [|[]]. 1:done. cbn. by f_equal. 
Qed.

Lemma expec_unnormalized {A} (l : list (R*A)) g0 notnil X : expec (init_distr_unnormalized l g0 notnil) X = R_list_sum (map (fun d => d.1* X d.2) l) / (R_list_sum (map fst l)).
Proof.
    unfold expec. unfold init_distr_unnormalized. simpl. rewrite map_map. simpl.
    rewrite <- R_list_sum_div. rewrite map_map. f_equal. apply map_ext_in. intros. lra.
Qed.

Lemma expecL_unnormalized {A} (l : list (R*A)) g0 notnil pl : length l = length pl -> expecL (init_distr_unnormalized l g0 notnil) pl = R_list_sum (map (fun d => d.1.1 * d.2) (zip l pl)) / (R_list_sum (map fst l)) .
Proof.
    unfold expecL. unfold init_distr_unnormalized. simpl. 
    assert (R_list_sum (map fst l) > 0) as Hr.
        {induction l; simpl; try contradiction. rewrite Forall_cons in g0. destruct g0.
        destruct l; simpl; try lra. apply Rplus_pos_pos; auto.
        }
    remember (R_list_sum (map fst l)) as r. clear Heqr.
    induction l in Hr, r, pl |- *.
        + intros. simpl. lra.
        + destruct pl. 1:intros [=]. simpl. intros [=]. rewrite IHl; auto. 
            rewrite Rdiv_plus_distr. f_equal. lra.
Qed.

Lemma ForallD_unnormalized {A} (l : list (R*A)) g0 notnil P : ForallD P(init_distr_unnormalized l g0 notnil) = Forall P (map snd l).
Proof.
    unfold ForallD. unfold init_distr_unnormalized. simpl. rewrite map_map.
    simpl. done.
Qed.

Lemma dom_distr_unnormalized {A} (l : list (R*A)) g0 notnil: domD (init_distr_unnormalized l g0 notnil) = (map snd l).
Proof.
unfold domD. unfold init_distr_unnormalized. simpl. rewrite map_map.
simpl. done.
Qed.

(* Singleton init *)

Program Definition init_single {A} (a:A) := mkDst A [(1, a)] _ _.
Next Obligation.
   intros. apply decide_geq0_2. constructor; simpl. 1:lra. done.
Qed.
Next Obligation.
    intros. simpl. lra.
Qed.

Lemma init_single_expec {A} (a : A) X :
    expec (init_single a) X = X a.
Proof.
    cbn. lra.
Qed. 

Lemma init_single_expecL {A} (a : A) r: expecL (init_single a) [r] = r.
Proof.
    unfold expecL. unfold init_single. simpl. lra.
Qed.

Lemma ForallD_single {A} (a : A) P : ForallD P (init_single a) <-> P a.
Proof.
    unfold ForallD. unfold init_single. simpl. rewrite Forall_cons. rewrite Forall_nil. tauto.
Qed.

Lemma distr_map_single {A B} (a : A) (f: A -> B) : distr_map (init_single a) f = init_single (f a).
Proof.
    apply distr_eq. by unfold distr_map.
Qed.

Lemma distr_map_single_inv {A B} b (f: A -> B) D : distr_map D f = init_single b → 
∃ a, b = f a ∧ D = init_single a.
Proof. 
    unfold init_single. unfold distr_map. inversion 1. clear H.
    destruct (map_eq_cons _ _ H1) as [[r a] [? [Heq1 [Heq2 Heq3]]]].
    apply map_eq_nil in Heq3. simpl in *. exists a. inversion Heq2. split; auto.
    apply distr_eq; simpl. by simplify_eq.
Qed.

Lemma domD_single {A} (a : A) : domD (init_single a) = [a].
Proof. unfold domD. unfold init_single. done. Qed.
 
(* Marginals *)

Program Definition fst_marginal {X Y} (D : @distribution (X * Y))
    := mkDst X (map (λ d, (d.1, d.2.1)) D.(distr)) _ _.
Next Obligation.
    intros X Y [l g0 H1]. apply decide_geq0_2. simpl. apply Forall_map. eapply Forall_impl. 1:apply decide_geq0_1; exact g0.
    intros [r [x y]]. done.
Qed.
Next Obligation.
intros X Y [l g0 H1]. simpl. rewrite <- H1. f_equal. clear.
    induction l as [|[]]. 1:done. cbn. by f_equal.
Qed.

Program Definition snd_marginal {X Y} (D : @distribution (X * Y))
    := mkDst Y (map (fun d => (d.1, d.2.2)) D.(distr)) _ _.
Next Obligation.
    intros X Y [l g0 H1]. apply decide_geq0_2. simpl. apply Forall_map. eapply Forall_impl. 1:apply decide_geq0_1; exact g0.
    intros [r [x y]]. done.
Qed.
Next Obligation.
intros X Y [l g0 H1]. simpl. rewrite <- H1. f_equal. clear.
    induction l as [|[]]. 1:done. cbn. by f_equal.
Qed.


(* Uniform distribution *)
Program Definition uniform_distr {A} (l : list A) (notnil : l <> [])
    := mkDst A (map (λ a, (/ INR (length l), a)) l) _ _.
Next Obligation.
    intros A l Hl. apply decide_geq0_2. apply Forall_map. destruct l; auto. apply Forall_forall.
    intros. apply Rinv_pos. apply lt_0_INR. simpl. lia.
Qed.
Next Obligation.
    intros A l notnil. erewrite R_list_sum_same. 1:apply Rdiv_diag.
        + destruct l; auto. rewrite map_length. apply not_0_INR. rewrite map_length.
        done.
        + repeat apply Forall_map. apply Forall_forall. intros.
        simpl. repeat rewrite map_length. done.
Qed.

Lemma expec_uniform {A} (l : list A) X notnil: expec (uniform_distr l notnil) X = R_list_sum (map X l) / INR (length l).
Proof.
    unfold expec. unfold uniform_distr. cbn -[INR]. rewrite <- R_list_sum_div. repeat rewrite map_map. simpl. f_equal. apply map_ext_Forall. apply Forall_forall. intros. lra.
Qed. 

Lemma expecL_uniform {A} (l : list A) pl notnil: length l = length pl -> expecL (uniform_distr l notnil) pl = R_list_sum pl / INR (length pl).
Proof.
    unfold expec. unfold uniform_distr. cbn -[INR]. intros Hlen. assert(INR (length pl) > 0)%R as Hn. 
        + rewrite <- Hlen. destruct l; try contradiction. apply lt_0_INR.
        simpl. lia.
        + rewrite Hlen. remember (INR (length pl)) as n. clear Heqn.
        induction l in  n, Hn, pl, Hlen |- *. 
            - intros. destruct pl; try discriminate. simpl. lra.
            - destruct pl; try discriminate. cbn -[INR]. injection Hlen as Hlen.
                rewrite IHl; auto. lra.
Qed.

Lemma ForallD_uniform {A} (l : list A) notnil P : ForallD P (uniform_distr l notnil) = Forall P l.
Proof.
    unfold ForallD. unfold uniform_distr. simpl. rewrite map_map. simpl. by rewrite map_id.
Qed.

Lemma dom_uniform {A} (l : list A) notnil : domD (uniform_distr l notnil) = l.
Proof.
    unfold uniform_distr. unfold domD. simpl. rewrite map_map. simpl. apply map_id.
Qed. 

(* Bind / snd marginal of convolution*)

Program Definition bind {A B} (D : @distribution A) (f : A -> @distribution B) := mkDst B (foldr (fun a acc => (map (fun a2 => (a2.1*a.1, a2.2)) (distr (f a.2))) ++ acc) [] (distr D)) _ _.
Next Obligation.
    intros A B [l g0 sum1] f. simpl. clear sum1. apply decide_geq0_2. specialize (decide_geq0_1 _ g0) as g0'. clear g0. induction l. 1:done. simpl. inversion g0'. apply Forall_app. split. 2:by apply IHl. apply Forall_map.
    simpl. destruct (f a.2) as [l' g0'' sum1']. simpl. specialize (decide_geq0_1 _ g0'') as g0. clear g0''. clear sum1'. induction l'. 1:done. inversion g0. apply Forall_cons. split.
        + apply Rmult_pos_pos; done.
        + by apply IHl'.
Qed.
Next Obligation.
    intros A B [l g0 sum1] f. specialize (decide_geq0_1 _ g0) as g0'. simpl. rewrite <- sum1. clear sum1 g0. induction l. 1:done. simpl. rewrite map_app. rewrite R_list_sum_app. inversion g0'. rewrite IHl; auto. f_equal. destruct (f a.2) as [l' g0'' sum1']. simpl. 
    rewrite map_map. simpl. rewrite R_list_sum_mult. rewrite sum1'. lra.
Qed.

Lemma bind_dom {A B} (D : @distribution A) (f : A → @distribution B) : (domD (bind D f)) = concat (map (fun a => (domD (f a))) (domD D)).
Proof.
    unfold bind. unfold domD. simpl. induction (distr D); auto. simpl. rewrite map_app.
    rewrite IHl. rewrite map_map. simpl. done.
Qed.

Lemma expec_bind {A B} (D : @distribution A) (f : A -> @distribution B) X : expec (bind D f) X = foldr (fun d acc => acc + d.1 * expec (f d.2) X) 0 (distr D).
Proof.
    unfold expec. unfold bind. simpl. induction (distr D); auto. simpl in *. rewrite map_app. rewrite map_map. rewrite R_list_sum_app. rewrite IHl. simpl. rewrite Rplus_comm. f_equal.
    rewrite Rmult_comm. rewrite  <- R_list_sum_mult. f_equal. apply map_ext_in.
    intros. lra.
Qed.

Lemma expecL_bind {A B} (D : @distribution A) (f : A -> @distribution B) pl pls :
    length pl = length (domD D) → length pl = length pls →  (∀ k D' pl', (domD D) !! k = Some D' → pls !! k = Some pl' → ∃ p, pl !! k = Some p ∧ length (domD (f D')) = length pl' ∧ expecL (f D') pl' = p) → expecL (bind D f) (concat pls) = expecL D pl.
Proof.
  unfold bind. unfold expecL. unfold domD. simpl. induction (distr D) in pl, pls |- *; simpl. 1:done. intros Hlen1 Hlen2. destruct pls; destruct pl; try done.
  simpl in *. injection Hlen1 as Hlen1. injection Hlen2 as Hlen2. rewrite map_length in Hlen1. destruct a as [r' a]. simpl. intros H. rewrite expecL'_concat.
    - rewrite (IHl pl); auto.
        * f_equal. specialize (H 0%nat a l0). destruct H as [r2 [Hr2 [Hlen <-]]]; auto.
        simpl in Hr2. injection Hr2 as ->. clear. induction (distr (f a)) in l0 |- *; destruct l0; simpl; try lra. rewrite IHl. lra.
        * by rewrite map_length.
        * intros. destruct (H (S k) D' pl') as [p [? ?]]; auto.
            eauto.
    - rewrite map_length. specialize (H 0%nat a l0). simpl in H. destruct H as (?&[=]&?&?); subst; auto. by rewrite map_length in H1.
Qed.


Lemma ForallD_bind {A B} (D : @distribution A) (f : A -> @distribution B) P: ForallD P (bind D f) <-> ForallD (fun a => ForallD P (f a)) D.
Proof.
    unfold ForallD. unfold bind. simpl. induction (distr D); simpl. 1:done.
    rewrite map_app. rewrite Forall_app. rewrite Forall_cons. rewrite map_map. simpl.
    rewrite IHl. tauto.
Qed.
 