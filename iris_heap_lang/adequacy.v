From iris.algebra Require Import auth.
From iris.base_logic.lib Require Import mono_nat.
From iris.proofmode Require Import proofmode.
From iris.program_logic Require Export weakestpre adequacy.
From iris.heap_lang Require Import proofmode notation.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.

Class heapGpreS Σ := HeapGpreS {
  heapGpreS_iris :> invGpreS Σ;
  heapGpreS_heap :> gen_heapGpreS loc (option val) Σ;
  heapGpreS_inv_heap :> inv_heapGpreS loc (option val) Σ;
  heapGS_step_cnt :> mono_natG Σ;
}.

Definition heapΣ : gFunctors :=
  #[invΣ; gen_heapΣ loc (option val); inv_heapΣ loc (option val);
    mono_natΣ].
Global Instance subG_heapGpreS {Σ} : subG heapΣ Σ → heapGpreS Σ.
Proof. solve_inG. Qed.

(* TODO: The [wp_adequacy] lemma is insufficient for a state interpretation
with a non-constant step index function. We thus use the more general
[wp_strong_adequacy] lemma. The proof below replicates part of the proof of
[wp_adequacy], and it hence would make sense to see if we can prove a version
of [wp_adequacy] for a non-constant step version. *)
Definition heap_adequacy Σ `{!heapGpreS Σ} s e σ φ p vp:
  (∀ `{!heapGS Σ}, ⊢ inv_heap_inv -∗ $p WP e @ s; ⊤ {{ v, ⌜φ v⌝ , vp v}}) →
  adequate s e σ 0 (λ v _, φ v) p vp.
Proof.
  intros Hwp.
  apply adequate_alt; intros D n Hstep. 
  assert (∃ t2 σ2 c2, (t2, σ2, c2) ∈ domD D) as (t2 & σ2 & c2 & H).
  { destruct (domD D) as [|[[t2 σ2] c2]] eqn:H;
    last set_solver. by specialize (domD_notnil D). }
  split; [| clear t2 σ2 c2 H; intros t2 σ2 c2 H]; eapply (wp_strong_adequacy Σ _) with (Φs_p := [vp]) (p:=p);
  [| done | done | | done | done];
  intros Hinv;
  iMod (gen_heap_init σ.(heap)) as (?) "[Hh _]";
  iMod (inv_heap_init loc (option val)) as (?) ">Hi";
  iMod (mono_nat_own_alloc) as (γ) "[Hsteps _]";
  iDestruct (Hwp (HeapGS _ _ _ _ _ _) with "Hi") as "Hwp";
  iModIntro;
  iExists (λ σ ns nt, (gen_heap_interp σ.(heap) ∗ 
                          mono_nat_auth_own γ 1 ns))%I;
  iExists [(λ v, ⌜φ v⌝%I)], (λ _, True)%I, _ => /=;
  iFrame; iSplit; auto;
  iSplitL; [by iApply wptp_singleton| | by iApply wptp_singleton |];
  iIntros (es' t2' -> ? ?) "Hexp _ H _";
  iApply fupd_mask_intro_discard; auto.
  iSplit; auto.
  iDestruct (big_sepL2_cons_inv_r with "H") as (e' ? ->) "[Hwp H]".
  iDestruct (big_sepL2_nil_inv_r with "H") as %->. 
  iIntros (v2 t2'' [= -> <-]). by rewrite to_of_val.
Qed.
