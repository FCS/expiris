(** This file proves the basic laws of the HeapLang program logic by applying
the Iris lifting lemmas. *)

From iris.proofmode Require Import proofmode.
From iris.bi.lib Require Import fractional.
From iris.base_logic.lib Require Import mono_nat.
From iris.base_logic.lib Require Export gen_heap proph_map gen_inv_heap.
From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Import ectx_lifting.
From iris.heap_lang Require Export class_instances.
From iris.heap_lang Require Import tactics notation.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.
Require Import Lra.

Class heapGS_gen Σ := HeapGS {
  heapGS_invGS : invGS_gen HasNoLc Σ;
  heapGS_gen_heapGS :> gen_heapGS loc (option val) Σ;
  heapGS_inv_heapGS :> inv_heapGS loc (option val) Σ;
  heapGS_step_name : gname;
  heapGS_step_cnt : mono_natG Σ;
}.
Local Existing Instance heapGS_step_cnt.

Notation heapGS Σ := (heapGS_gen Σ).

Section steps.
  Context `{!heapGS_gen Σ}.

  Local Definition steps_auth (n : nat) : iProp Σ :=
    mono_nat_auth_own heapGS_step_name 1 n.

  Definition steps_lb (n : nat) : iProp Σ :=
    mono_nat_lb_own heapGS_step_name n.

  Local Lemma steps_lb_valid n m :
    steps_auth n -∗ steps_lb m -∗ ⌜m ≤ n⌝.
  Proof.
    iIntros "Hauth Hlb".
    by iDestruct (mono_nat_lb_own_valid with "Hauth Hlb") as %[_ Hle].
  Qed.

  Local Lemma steps_lb_get n :
    steps_auth n -∗ steps_lb n.
  Proof. apply mono_nat_lb_own_get. Qed.

  Local Lemma steps_auth_update n n' :
    n ≤ n' → steps_auth n ==∗ steps_auth n' ∗ steps_lb n'.
  Proof. intros Hle. by apply mono_nat_own_update. Qed.

  Local Lemma steps_auth_update_S n :
    steps_auth n ==∗ steps_auth (S n).
  Proof.
    iIntros "Hauth".
    iMod (mono_nat_own_update with "Hauth") as "[$ _]"; [lia|done].
  Qed.

  Lemma steps_lb_le n n' :
    n' ≤ n → steps_lb n -∗ steps_lb n'.
  Proof. intros Hle. by iApply mono_nat_lb_own_le. Qed.

End steps.

Global Program Instance heapGS_irisGS `{!heapGS_gen Σ} : irisGS_gen heap_lang Σ := {
  iris_invGS := heapGS_invGS;
  state_interp σ step_cnt _ :=
    (gen_heap_interp σ.(heap) ∗ steps_auth step_cnt)%I;
  fork_post _ := True%I;
  num_laters_per_step n := n;
}.
Next Obligation.
  iIntros (?? σ ns nt)  "/= ($ & H)".
  by iMod (steps_auth_update_S with "H") as "$".
Qed.

(** Since we use an [option val] instance of [gen_heap], we need to overwrite
the notations.  That also helps for scopes and coercions. *)
Notation "l ↦ dq v" := (mapsto (L:=loc) (V:=option val) l dq (Some v%V))
  (at level 20, dq custom dfrac at level 1, format "l  ↦ dq  v") : bi_scope.

(** Same for [gen_inv_heap], except that these are higher-order notations so to
make setoid rewriting in the predicate [I] work we need actual definitions
here. *)
Section definitions.
  Context `{!heapGS_gen Σ}.
  Definition inv_mapsto_own (l : loc) (v : val) (I : val → Prop) : iProp Σ :=
    inv_mapsto_own l (Some v) (from_option I False).
  Definition inv_mapsto (l : loc) (I : val → Prop) : iProp Σ :=
    inv_mapsto l (from_option I False).
End definitions.

Global Instance: Params (@inv_mapsto_own) 4 := {}.
Global Instance: Params (@inv_mapsto) 3 := {}.

Notation inv_heap_inv := (inv_heap_inv loc (option val)).
Notation "l '↦_' I □" := (inv_mapsto l I%stdpp%type)
  (at level 20, I at level 9, format "l  '↦_' I  '□'") : bi_scope.
Notation "l ↦_ I v" := (inv_mapsto_own l v I%stdpp%type)
  (at level 20, I at level 9, format "l  ↦_ I  v") : bi_scope.

Section lifting.
Context `{!heapGS_gen  Σ}.
Implicit Types P Q : iProp Σ.
Implicit Types Φ Ψ : val → iProp Σ.
Implicit Types efs : list expr.
Implicit Types σ : state.
Implicit Types v : val.
Implicit Types l : loc.

Lemma wp_lb_init s E e Φ p vp:
  TCEq (to_val e) None →
  (steps_lb 0 -∗ $p WP e @ s; E {{ Φ, vp}}) -∗
  $p WP e @ s; E {{ Φ , vp }}.
Proof.
  (** TODO: We should try to use a generic lifting lemma (and avoid [wp_unfold])
  here, since this breaks the WP abstraction. *)
  rewrite !wp_unfold /wp_pre /=. iIntros (->) "Hwp".
  iIntros (σ1 ns  m) "(Hσ  & Hsteps)".
  iDestruct (steps_lb_get with "Hsteps") as "#Hlb".
  iDestruct (steps_lb_le _ 0 with "Hlb") as "Hlb0"; [lia|].
  iSpecialize ("Hwp" with "Hlb0"). iApply ("Hwp" $! σ1 ns  m). iFrame.
Qed.

Lemma wp_lb_update s n E e Φ p vp:
  TCEq (to_val e) None →
  steps_lb n -∗
  $p WP e @ s; E {{ v, steps_lb (S n) -∗ Φ v , vp v }} -∗
  $p WP e @ s; E {{ Φ , vp }}.
Proof.
  (** TODO: We should try to use a generic lifting lemma (and avoid [wp_unfold])
  here, since this breaks the WP abstraction. *)
  rewrite !wp_unfold /wp_pre /=. iIntros (->) "Hlb Hwp".
  iIntros (σ1 ns m) "(Hσ & Hsteps)".
  iDestruct (steps_lb_valid with "Hsteps Hlb") as %?.
  iMod ("Hwp" $! σ1 ns m with "[$Hσ $Hsteps]") as "[% [%Hs Hwp]]".
  iModIntro. iSplit; [done|]. iSplit; first done.
  iIntros (D Hstep). iMod ("Hwp" with "[//]") as "Hwp".
  iIntros "!> !>". iMod "Hwp" as "Hwp". iIntros "!>".
  iApply (step_fupdN_wand with "Hwp").
  iIntros "[%pl [% Hwp]]". iExists pl. iSplit; first done.
  iApply big_andL2_impl. iFrame.
  iIntros (k d p'') "_ _ Hwp".
  iIntros "%e2 %σ2 %c %efs ->".
  iDestruct ("Hwp" $! e2 σ2 c efs with "[//]") as "(%p''' & %pl' & % & Hwp)".
  iExists p''', pl'. iSplitR; auto.
  iMod "Hwp" as "(($ & Hsteps)& Hwp & $)".
  iDestruct (steps_lb_get with "Hsteps") as "#HlbS".
  iDestruct (steps_lb_le _ (S n) with "HlbS") as "#HlbS'"; [lia|].
  iModIntro. iFrame "Hsteps".
  iApply (wp_wand with "Hwp"). iIntros (v) "HΦ". by iApply "HΦ".
Qed.

Lemma wp_step_fupdN_lb s n E1 E2 e P Φ p vp:
  TCEq (to_val e) None →
  E2 ⊆ E1 →
  steps_lb n -∗
  (|={E1∖E2,∅}=> |={∅}▷=>^(S n) |={∅,E1∖E2}=> P) -∗
  $p WP e @ s; E2 {{ v, P ={E1}=∗ Φ v, vp v }} -∗
  $p WP e @ s; E1 {{ Φ, vp }}.
Proof.
  iIntros (He HE) "Hlb HP Hwp".
  iApply wp_step_fupdN; [done|].
  iSplit; [|by iFrame].
  iIntros (σ ns nt) "(? & Hsteps)".
  iDestruct (steps_lb_valid with "Hsteps Hlb") as %Hle.
  iApply fupd_mask_intro; [set_solver|].
  iIntros "_". iPureIntro. rewrite /num_laters_per_step /=. lia.
Qed.

(** Recursive functions: we do not use this lemmas as it is easier to use Löb
induction directly, but this demonstrates that we can state the expected
reasoning principle for recursive functions, without any visible ▷. *)
Lemma wp_rec_löb s E f x e Φ Ψ p vp:
  (p >= 0)%R → 
  □ ( □ (∀ v, Ψ v -∗ $p WP (rec: f x := e)%V v @ s; E {{ Φ , vp }}) -∗
     ∀ v, Ψ v -∗ $p WP (subst' x v (subst' f (rec: f x := e) e)) @ s; E {{ Φ , vp }}) -∗
  ∀ v, Ψ v -∗ $p WP (rec: f x := e)%V v @ s; E {{ Φ, vp }}.
Proof.
  intros Hp. iIntros "#Hrec". iLöb as "IH". iIntros (v) "HΨ".
  iApply lifting.wp_pure_step_later; try done.
  iNext. iApply ("Hrec" with "[] HΨ"). iIntros "!>" (w) "HΨ".
  iApply ("IH" with "HΨ").
Qed.

(** Fork: Not using Texan triples to avoid some unnecessary [True] *)
Lemma wp_fork s E e Φ p:
  (p >= 0)%R → 
  ▷ $p WP e @ s; ⊤ {{ _, True , R0 }} -∗ ▷ Φ (LitV LitUnit) -∗ $p WP Fork e @ s; E {{ Φ , λ _, R0 }}.
Proof.
  intros Hp. iIntros "He HΦ". iApply wp_lift_head_step_fupd ; [done|].
  iIntros (σ1 ns nt) "(?&Hsteps)".
  iApply fupd_mask_intro; first set_solver. iIntros "Hclose".
  iSplit; first done.
  iSplit; first auto with lia head_step.
  iIntros "!>" (D Hstep) "!>"; inv_head_step.
   iExists [p]. iSplitR. 
  { iPureIntro. unfold expec_cost. rewrite init_single_expec; rewrite init_single_expecL; simpl. lra. }
  inv_head_step. iSplitL; auto. iIntros (e2 σ2 c efs Heq). simplify_eq.
  iExists R0, [p]. iSplitR.
  {iPureIntro. simpl. lra. }
  iMod "Hclose". iMod (steps_auth_update_S with "Hsteps") as "Hsteps"; simpl.
  iFrame. iApply wp_value; auto. lra.
Qed.

(** Heap *)

(** We need to adjust the [gen_heap] and [gen_inv_heap] lemmas because of our
value type being [option val]. *)

Lemma mapsto_valid l dq v : l ↦{dq} v -∗ ⌜✓ dq⌝.
Proof. apply mapsto_valid. Qed.
Lemma mapsto_valid_2 l dq1 dq2 v1 v2 :
  l ↦{dq1} v1 -∗ l ↦{dq2} v2 -∗ ⌜✓ (dq1 ⋅ dq2) ∧ v1 = v2⌝.
Proof. iIntros "H1 H2". iCombine "H1 H2" gives %[? [= ?]]. done. Qed.
Lemma mapsto_agree l dq1 dq2 v1 v2 : l ↦{dq1} v1 -∗ l ↦{dq2} v2 -∗ ⌜v1 = v2⌝.
Proof. iIntros "H1 H2". iCombine "H1 H2" gives %[_ [= ?]]. done. Qed.

Global Instance mapsto_combine_sep_gives l dq1 dq2 v1 v2 : 
  CombineSepGives (l ↦{dq1} v1) (l ↦{dq2} v2) ⌜✓ (dq1 ⋅ dq2) ∧ v1 = v2⌝ | 20. 
  (* We provide an instance with lower cost than the gen_heap instance
     to avoid having to deal with Some v1 = Some v2 *)
Proof.
  rewrite /CombineSepGives. iIntros "[H1 H2]".
  iCombine "H1 H2" gives %[? [=->]]. eauto.
Qed.

Lemma mapsto_combine l dq1 dq2 v1 v2 :
  l ↦{dq1} v1 -∗ l ↦{dq2} v2 -∗ l ↦{dq1 ⋅ dq2} v1 ∗ ⌜v1 = v2⌝.
Proof.
  iIntros "Hl1 Hl2". by iCombine "Hl1 Hl2" as "$" gives %[_ ->].
Qed.

Lemma mapsto_frac_ne l1 l2 dq1 dq2 v1 v2 :
  ¬ ✓(dq1 ⋅ dq2) → l1 ↦{dq1} v1 -∗ l2 ↦{dq2} v2 -∗ ⌜l1 ≠ l2⌝.
Proof. apply mapsto_frac_ne. Qed.
Lemma mapsto_ne l1 l2 dq2 v1 v2 : l1 ↦ v1 -∗ l2 ↦{dq2} v2 -∗ ⌜l1 ≠ l2⌝.
Proof. apply mapsto_ne. Qed.

Lemma mapsto_persist l dq v : l ↦{dq} v ==∗ l ↦□ v.
Proof. apply mapsto_persist. Qed.

Global Instance inv_mapsto_own_proper l v :
  Proper (pointwise_relation _ iff ==> (≡)) (inv_mapsto_own l v).
Proof.
  intros I1 I2 HI. rewrite /inv_mapsto_own. f_equiv=>-[w|]; last done.
  simpl. apply HI.
Qed.
Global Instance inv_mapsto_proper l :
  Proper (pointwise_relation _ iff ==> (≡)) (inv_mapsto l).
Proof.
  intros I1 I2 HI. rewrite /inv_mapsto. f_equiv=>-[w|]; last done.
  simpl. apply HI.
Qed.

Lemma make_inv_mapsto l v (I : val → Prop) E :
  ↑inv_heapN ⊆ E →
  I v →
  inv_heap_inv -∗ l ↦ v ={E}=∗ l ↦_I v.
Proof. iIntros (??) "#HI Hl". iApply make_inv_mapsto; done. Qed.
Lemma inv_mapsto_own_inv l v I : l ↦_I v -∗ l ↦_I □.
Proof. apply inv_mapsto_own_inv. Qed.

Lemma inv_mapsto_own_acc_strong E :
  ↑inv_heapN ⊆ E →
  inv_heap_inv ={E, E ∖ ↑inv_heapN}=∗ ∀ l v I, l ↦_I v -∗
    (⌜I v⌝ ∗ l ↦ v ∗ (∀ w, ⌜I w ⌝ -∗ l ↦ w ==∗
      inv_mapsto_own l w I ∗ |={E ∖ ↑inv_heapN, E}=> True)).
Proof.
  iIntros (?) "#Hinv".
  iMod (inv_mapsto_own_acc_strong with "Hinv") as "Hacc"; first done.
  iIntros "!>" (l v I) "Hl". iDestruct ("Hacc" with "Hl") as "(% & Hl & Hclose)".
  iFrame "%∗". iIntros (w) "% Hl". iApply "Hclose"; done.
Qed.

Lemma inv_mapsto_own_acc E l v I:
  ↑inv_heapN ⊆ E →
  inv_heap_inv -∗ l ↦_I v ={E, E ∖ ↑inv_heapN}=∗
    (⌜I v⌝ ∗ l ↦ v ∗ (∀ w, ⌜I w ⌝ -∗ l ↦ w ={E ∖ ↑inv_heapN, E}=∗ l ↦_I w)).
Proof.
  iIntros (?) "#Hinv Hl".
  iMod (inv_mapsto_own_acc with "Hinv Hl") as "(% & Hl & Hclose)"; first done.
  iFrame "%∗". iIntros "!>" (w) "% Hl". iApply "Hclose"; done.
Qed.

Lemma inv_mapsto_acc l I E :
  ↑inv_heapN ⊆ E →
  inv_heap_inv -∗ l ↦_I □ ={E, E ∖ ↑inv_heapN}=∗
    ∃ v, ⌜I v⌝ ∗ l ↦ v ∗ (l ↦ v ={E ∖ ↑inv_heapN, E}=∗ ⌜True⌝).
Proof.
  iIntros (?) "#Hinv Hl".
  iMod (inv_mapsto_acc with "Hinv Hl") as ([v|]) "(% & Hl & Hclose)"; [done| |done].
  iIntros "!>". iExists (v). iFrame "%∗".
Qed.

(** The usable rules for [allocN] stated in terms of the [array] proposition
are derived in te file [array]. *)
Lemma heap_array_to_seq_meta l vs (n : nat) :
  length vs = n →
  ([∗ map] l' ↦ _ ∈ heap_array l vs, meta_token l' ⊤) -∗
  [∗ list] i ∈ seq 0 n, meta_token (l +ₗ (i : nat)) ⊤.
Proof.
  iIntros (<-) "Hvs". iInduction vs as [|v vs] "IH" forall (l)=> //=.
  rewrite big_opM_union; last first.
  { apply map_disjoint_spec=> l' v1 v2 /lookup_singleton_Some [-> _].
    intros (j&w&?&Hjl&?&?)%heap_array_lookup.
    rewrite Loc.add_assoc -{1}[l']Loc.add_0 in Hjl. simplify_eq; lia. }
  rewrite Loc.add_0 -fmap_S_seq big_sepL_fmap.
  setoid_rewrite Nat2Z.inj_succ. setoid_rewrite <-Z.add_1_l.
  setoid_rewrite <-Loc.add_assoc.
  rewrite big_opM_singleton; iDestruct "Hvs" as "[$ Hvs]". by iApply "IH".
Qed.

Lemma heap_array_to_seq_mapsto l v (n : nat) :
  ([∗ map] l' ↦ ov ∈ heap_array l (replicate n v), gen_heap.mapsto l' (DfracOwn 1) ov) -∗
  [∗ list] i ∈ seq 0 n, (l +ₗ (i : nat)) ↦ v.
Proof.
  iIntros "Hvs". iInduction n as [|n] "IH" forall (l); simpl.
  { done. }
  rewrite big_opM_union; last first.
  { apply map_disjoint_spec=> l' v1 v2 /lookup_singleton_Some [-> _].
    intros (j&w&?&Hjl&_)%heap_array_lookup.
    rewrite Loc.add_assoc -{1}[l']Loc.add_0 in Hjl. simplify_eq; lia. }
  rewrite Loc.add_0 -fmap_S_seq big_sepL_fmap.
  setoid_rewrite Nat2Z.inj_succ. setoid_rewrite <-Z.add_1_l.
  setoid_rewrite <-Loc.add_assoc.
  rewrite big_opM_singleton; iDestruct "Hvs" as "[$ Hvs]". by iApply "IH".
Qed.

Lemma wp_allocN_seq s E v n p:
  (p >= 0)%R → 
  (0 < n)%Z →
  {{{ $p, True }}} AllocN (Val $ LitV $ LitInt $ n) (Val v) @ s; E
  {{{ l, RET LitV (LitLoc l); [∗ list] i ∈ seq 0 (Z.to_nat n),
      (l +ₗ (i : nat)) ↦ v ∗ meta_token (l +ₗ (i : nat)) ⊤ ; p}}}.
Proof.
  iIntros (Hp Hn Φ vp) "_ HΦ". iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iSplit; auto with head_step.
  iSplitR.
  {iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  iMod (gen_heap_alloc_big _ (heap_array _ (replicate (Z.to_nat n) v)) with "Hσ1")
  as "(Hσ1 & Hl & Hm)".
{ apply heap_array_map_disjoint.
  rewrite replicate_length Z2Nat.id; auto with lia. }
  iMod (steps_auth_update_S with "Hσ2") as "Hσ2".
  iModIntro. iFrame.
  iDestruct ("HΦ" $! l) as "[HΦ %]".
  iApply wp_value; first lra.
  iApply "HΦ".
  iApply big_sepL_sep. iSplitL "Hl".
  + by iApply heap_array_to_seq_mapsto.
  + iApply (heap_array_to_seq_meta with "Hm"). by rewrite replicate_length.
Qed.

Lemma wp_alloc s E v p:
  (p >= 0)%R → 
  {{{ $p, True }}} Alloc (Val v) @ s; E {{{ l, RET LitV (LitLoc l); l ↦ v ∗ meta_token l ⊤ ; p }}}.
Proof.
  iIntros (Hp Φ vp) "_ HΦ". iApply wp_allocN_seq; auto with lia.
  iIntros "!>" (l). iDestruct ("HΦ" $! l) as "[HΦ $]".
  iIntros "/=  (? & _ )". rewrite Loc.add_0. iApply "HΦ"; iFrame.
Qed.

Lemma wp_free s E l v p:
  (p >= 0)%R → 
  {{{ $p, ▷ l ↦ v }}} Free (Val $ LitV (LitLoc l)) @ s; E
  {{{ RET LitV LitUnit; True ; p }}}.
Proof.
  iIntros (Hp Φ vp) ">Hl HΦ".
  iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ1 ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  iMod (gen_heap_update with "Hσ1 Hl") as "[$ Hl]".
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iDestruct "HΦ" as "[HΦ %]". iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_load s E l dq v p:
  (p >= 0)%R → 
  {{{ $p , ▷ l ↦{dq} v }}} Load (Val $ LitV $ LitLoc l) @ s; E {{{ RET v; l ↦{dq} v ; p }}}.
Proof.
  intros Hp. iIntros (Φ vp) ">Hl HΦ". iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iFrame. iDestruct "HΦ" as "[HΦ %]". iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_store s E l v' v p:
  (p >= 0)%R → 
  {{{ $p, ▷ l ↦ v' }}} Store (Val $ LitV (LitLoc l)) (Val v) @ s; E
  {{{ RET LitV LitUnit; l ↦ v ; p }}}.
Proof.
  intros Hp. iIntros (Φ vp) ">Hl HΦ". 
  iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  iMod (gen_heap_update with "Hσ1 Hl") as "[$ Hl]".
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iDestruct "HΦ" as "[HΦ %]".
   iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_xchg s E l v' v p:
  (p >= 0)%R → 
  {{{ $p ,  ▷ l ↦ v' }}} Xchg (Val $ LitV (LitLoc l)) (Val v) @ s; E
  {{{ RET v'; l ↦ v ; p }}}.
Proof.
  intros Hp. iIntros (Φ vp) ">Hl HΦ". 
  iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  iMod (gen_heap_update with "Hσ1 Hl") as "[$ Hl]".
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iDestruct "HΦ" as "[HΦ %]".
  iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_cmpxchg_fail s E l dq v' v1 v2 p:
  (p >= 0)%R → 
  v' ≠ v1 → vals_compare_safe v' v1 →
  {{{ $p , ▷ l ↦{dq} v' }}} CmpXchg (Val $ LitV $ LitLoc l) (Val v1) (Val v2) @ s; E
  {{{ RET PairV v' (LitV $ LitBool false); l ↦{dq} v' ; p }}}.
Proof.
  intros ???. iIntros (Φ vp) ">Hl HΦ". 
  iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  rewrite bool_decide_false //.
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iFrame. iDestruct "HΦ" as "[HΦ %]".
  iModIntro. iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_cmpxchg_suc s E l v1 v2 v' p:
  (p >= 0)%R → 
  v' = v1 → vals_compare_safe v' v1 →
  {{{ $p, ▷ l ↦ v' }}} CmpXchg (Val $ LitV $ LitLoc l) (Val v1) (Val v2) @ s; E
  {{{ RET PairV v' (LitV $ LitBool true); l ↦ v2; p }}}.
Proof.
  intros ???. iIntros (Φ vp) ">Hl HΦ". 
  iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  rewrite bool_decide_true //.
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iMod (gen_heap_update with "Hσ1 Hl") as "[$ Hl]".
  iFrame. iDestruct "HΦ" as "[HΦ %]".
  iModIntro. iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_faa s E l i1 i2 p:
  (p >= 0)%R → 
  {{{ $p , ▷ l ↦ LitV (LitInt i1) }}} FAA (Val $ LitV $ LitLoc l) (Val $ LitV $ LitInt i2) @ s; E
  {{{ RET LitV (LitInt i1); l ↦ LitV (LitInt (i1 + i2)) ; p }}}.
Proof.
  intros ?. iIntros (Φ vp) ">Hl HΦ". 
  iApply wp_lift_det_head_step_no_fork; auto.
  iIntros (σ ns nt) "[Hσ1 Hσ2]".
  iDestruct (gen_heap_valid with "Hσ1 Hl") as %?.
  iSplit; eauto with head_step.
  iSplitR.
  { iIntros (D Hstep). inv_head_step. naive_solver. }
  iIntros "!> !> !>" (e2 σ2 Hstep). inv_head_step.
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iMod (gen_heap_update with "Hσ1 Hl") as "[$ Hl]".
  iFrame. iDestruct "HΦ" as "[HΦ %]".
  iModIntro. iApply wp_value; first lra.
  by iApply "HΦ".
Qed.

Lemma wp_tick s E p c Φ vp:
(p >= 0)%R → 
(c >= 0)%R → 
(p >= vp #() + c)%R → 
▷ Φ (LitV LitUnit) -∗ $p WP Tick (Val $ LitV $ LitR c) @ s;E {{ Φ , vp  }}.
Proof.
  intros ???. iIntros "HΦ". iApply wp_lift_head_step_fupd; auto.
  iIntros (σ1 ns nt) "[Hσ1 Hσ2]". iApply fupd_mask_intro; first set_solver.
  iIntros "Hclose". iSplit; first (iPureIntro; lra).
  iSplit; first eauto with head_step.
  iIntros "!>" (D Hstep) "!>".
  iExists [(p-c)%R]. inv_head_step. iSplit.
  {rewrite init_single_expecL. unfold expec_cost. rewrite init_single_expec; simpl. iPureIntro. lra. }
  iSplit; auto.
  iIntros (?????). simplify_eq.
  iExists (p-c0)%R, nil. iSplit; first (iPureIntro; simpl; lra).
  iMod "Hclose".
  iMod (steps_auth_update_S with "Hσ2") as "$".
  iModIntro. simpl. iFrame. iApply wp_value; auto.
  lra.
Qed.


(*TODO not the right place*)
Lemma zip_map_map {A B C} li (f: A → B) (g: A → C): zip (map f li) (map g li) = map (λ x, (f x, g x)) li.
Proof. induction li; auto. simpl. by f_equal. Qed.

Lemma zip_same {A} (li : list A) : zip li li = map (λ x, (x,x)) li.
Proof. induction li; auto. simpl. by f_equal. Qed.

Lemma wp_choose_weighted s E dl p Φ vp :
(p >= 0)%R → 
(dl ≠ nil) → 
Forall (λ v, ∃ (r : R) v', v = (#r, v')%V ∧ (r > 0)%R) dl → 
(R_list_sum (map (λ x, x.1 * vp x.2) (convert_prob_list dl))/
 (R_list_sum (map fst (convert_prob_list dl))) <= p)%R → 
▷ ([∧ list] x ∈ (convert_prob_list dl), Φ x.2) -∗ 
$p WP ChooseWeighted (Val $ LitListV $ dl) @ s;E {{ Φ, vp }}.
Proof.
  intros. iIntros "HΦ". iApply wp_lift_head_step_fupd; auto.
  iIntros (σ1 ns nt) "[Hσ1 Hσ2]". iApply fupd_mask_intro; first set_solver.
  iIntros "Hclose". iSplit; auto. iSplit; first eauto with head_step.
  iNext. iIntros (D Hstep). inv_head_step.
  iExists (map (λ x, vp x.2) (convert_prob_list dl)).
  iModIntro. iSplit.
  - iPureIntro. rewrite expecL_unnormalized; last by repeat rewrite map_length. unfold expec_cost. rewrite expec_unnormalized; simpl. rewrite !map_map.
  erewrite (map_ext _ (λ _, R0)).
  2: {intros [ ]. simpl. lra. }
  erewrite R_list_sum_same.
  2: {rewrite Forall_map. rewrite Forall_forall. done. }
  rewrite zip_map_map. rewrite map_map. simpl.
  setoid_rewrite (map_ext _ fst) at 4;
  last by intros [ ].
  setoid_rewrite (map_ext _ (λ x, (x.1 * vp x.2)%R)) at 3;
  last by intros [ ].
  lra.
  - rewrite dom_distr_unnormalized. rewrite map_map.
    rewrite big_andL2_fmap_l.
    rewrite big_andL2_fmap_r. 
    rewrite big_andL2_alt.
    iSplit; auto.
    rewrite zip_same. rewrite big_andL_fmap.
    iApply big_andL_impl2. iFrame.
    iIntros (? [r v] Hlook) "HΦ".
    simpl. iIntros (?????). simplify_eq.
    iExists (vp v), nil. iSplit; first (iPureIntro;simpl;lra).
    iMod "Hclose".
    iMod (steps_auth_update_S with "Hσ2") as "$".
    simpl. iFrame.
    iApply wp_value; auto. lra. 
    Unshelve. all:auto.
Qed.

Lemma wp_choose_uniform s E dl p Φ vp :
(p >= 0)%R → 
(dl ≠ nil) → 
(R_list_sum (map vp dl) / INR (length dl) <= p)%R → 
▷ ([∧ list] v ∈ dl, Φ v) -∗ 
$p WP ChooseUniform (Val $ LitListV $ dl) @ s;E {{ Φ, vp }}.
Proof.
  intros. iIntros "HΦ". iApply wp_lift_head_step_fupd; auto.
  iIntros (σ1 ns nt) "[Hσ1 Hσ2]". iApply fupd_mask_intro; first set_solver.
  iIntros "Hclose". iSplit; auto. iSplit; first eauto with head_step.
  iNext. iIntros (D Hstep). inv_head_step.
  iExists (map vp dl).
  iModIntro. iSplit.
  - iPureIntro. rewrite expecL_uniform; last by repeat rewrite map_length. unfold expec_cost. rewrite expec_uniform; simpl. rewrite !map_map.
  rewrite !map_length.
  erewrite (map_ext _ (λ _, R0));
  last done. 
  erewrite R_list_sum_same.
  2: {rewrite Forall_map. rewrite Forall_forall. done. }
  lra.
  - rewrite dom_uniform.
    rewrite big_andL2_fmap_l.
    rewrite big_andL2_fmap_r. 
    rewrite big_andL2_alt.
    iSplit; auto.
    rewrite zip_same. rewrite big_andL_fmap.
    iApply big_andL_impl2. iFrame.
    iIntros (? v Hlook) "HΦ".
    simpl. iIntros (?????). simplify_eq.
    iExists (vp v), nil. iSplit; first (iPureIntro;simpl;lra).
    iMod "Hclose".
    iMod (steps_auth_update_S with "Hσ2") as "$".
    simpl. iFrame.
    iApply wp_value; auto. lra. 
    Unshelve. auto.
Qed.

  

End lifting.
