From iris.proofmode Require Import proofmode.
From iris.program_logic Require Export weakestpre.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.heap_lang.lib Require Import array.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.

Require Import Lra.
Require Import Reals.

Section proofs.
Context `{!heapGS_gen Σ}.


Lemma ChooseUniform_elem l :
    l ≠ [] → 
    ⊢ WP ChooseUniform (LitListV l) {{v, ⌜ v ∈ l ⌝ }}.
Proof.
    intros Hl. wp_smart_apply (wp_choose_uniform); auto.
    - erewrite (R_list_sum_same 0); first lra. 
    simpl. rewrite Forall_map. rewrite Forall_forall. done.
    - rewrite big_andL_forall. iPureIntro; simpl.
    intros. by eapply elem_of_list_lookup_2.
Qed.

Lemma uniform_test:
    ⊢ $2%R WP Tick (ChooseUniform (LitListV [LitV $ LitR  1%R; LitV $ LitR 3%R])) {{v, True, R0}}.
Proof.
    wp_choose_uniform (λ v, match v with 
        | LitV (LitR r) => r 
        | _ => R0
        end
    ).
    iSplit; try iSplit; auto; by wp_tick.
Qed.

Lemma two_ticks:
    ⊢ $2%R WP let: "_" := Tick #1%R in Tick #1%R {{v, True, 0%R}}.
Proof.
    simpl. wp_tick. simpl. iNext. wp_pures. by wp_tick.
Qed.

Lemma coin_flip_uniform:
    ⊢ $2%R WP
    (rec: "flip" "v" := let: "_" := Tick #1%R in 
            if: ChooseUniform (LitListV [LitV $ LitBool true; LitV $ LitBool false]) then 
                "flip" #()
            else #())
    #()
    {{v, True, 0%R}}.
    Proof.
        wp_pures. iLöb as "IH". wp_tick. iNext. wp_pures.
        wp_choose_uniform (λ v, match v with | #true => 2%R | _ => 0%R end) .
        repeat iSplit; (auto || by wp_pures).
    Qed.

Lemma test_weighted:
    ⊢ $1.7%R WP Tick (ChooseWeighted (LitListV [PairV (LitV $ LitR 0.3%R) (LitV $ LitR 1%R) ; PairV (LitV $ LitR 0.7%R) (LitV $ LitR 2%R)]))
    {{v, True, 0%R}}.
Proof.
    wp_choose_weighted (λ v, match v with
        | LitV (LitR r) => r
        | _ => 0%R
    end).
    simpl; repeat iSplit; auto; by wp_tick.
Qed.

Definition Seq : val :=
    rec: "listrange" "i" "j" :=
        if: "i" = "j"
        then LitListV [] 
        else "i" :: "listrange" ("i" + #1) "j".

Lemma Seq_spec (i j : nat) : 
    (i <= j) → 
    ⊢ {{{True}}} Seq #i #j {{{v, RET v; ⌜ v = LitListV (map (λ (x: nat), #x) (seq i (j-i))) ⌝  }}}.
Proof.
    intros. remember (j-i) as n. iIntros "!>" (Φ) "_ HΦ ". unfold Seq. 
    iInduction n as [|n] "IH" forall (i j Heqn H Φ ).
    -  wp_pures. rewrite bool_decide_eq_true_2; last by replace j with i by lia. wp_pures. by iApply "HΦ".
    - wp_pures. rewrite bool_decide_eq_false_2.
    2 : {intros [=]. lia. }
    wp_pure. wp_pure. wp_bind (Seq _ _ ).
    rewrite Z.add_1_r -!Nat2Z.inj_succ. iApply "IH"; auto with lia. 
    iIntros "!>" (v) "->". wp_pures. by iApply "HΦ".
Qed. 

Definition ChooseRange : val := 
    λ: "i" "j", ChooseUniform (Seq "i" "j").

Lemma ChooseRange_spec (i j : nat) p vp: 
    (p >= 0)%R → 
    (i < j) → 
    (R_list_sum (map (λ (x : nat), vp (#x)) (seq i (j-i)))/ (INR (j - i)) <= p)%R → 
    ⊢ {{{$p, True}}} ChooseRange #i #j {{{n, RET #n; ⌜ i ≤  n < j ⌝ ; vp #n}}}.
Proof.
    intros. iIntros "!>" (Φ vp') "_ HΦ". unfold ChooseRange.
    wp_pures. wp_bind (Seq _ _). replace p with (0 + p)%R by lra. wp_apply (wp_pot_add).
    wp_apply (Seq_spec i j); auto with lia. iIntros (v Hv).
    iAssert (⌜ ∀ (n: nat), vp #n = vp' #n⌝ )%I as "%Hvp".
    { iIntros (n'). iDestruct ("HΦ" $! n') as "[_ $]". }
    iAssert (∀ (n : nat), ⌜ i <= n < j ⌝ -∗ Φ #n)%I with "[HΦ]" as "HΦ'".
    {iIntros (n'). iDestruct ("HΦ" $! n') as "[H _]". by iApply "H". }
    rewrite Hv. wp_choose_uniform vp.
    - by replace (j - i) with (S (j - 1 - i)) by lia.
    - rewrite map_length seq_length. replace (0 + p)%R with p by lra.
      rewrite map_map. erewrite map_ext; first apply H1.
      intros n'. simpl. by rewrite -(Hvp n'). 
    - rewrite big_andL_forall. iIntros. 
      apply map_lookup_Some in H2. destruct H2 as (n' & Hseq & ->).
      iApply "HΦ'". iPureIntro. apply lookup_seq in Hseq. lia.
Qed.

Lemma wp_eq_pot p e Φ: 
    (p >= 0)%R → 
    WP e {{Φ}} ⊢ $p WP e {{v, Φ v, p}}.
Proof.
    intros. rewrite (wp_pot_add _ _  _ _ _ p); auto.
    by replace (R0 + p)%R with p by lra.
Qed.

End proofs.