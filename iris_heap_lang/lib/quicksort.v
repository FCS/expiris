From iris.proofmode Require Import proofmode.
From iris.program_logic Require Export weakestpre.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.heap_lang.lib Require Import array prob.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.

Require Import Lra.
Require Import Reals.
Require Import Sorting.Permutation.
Require Import ZArith.

Definition swap : val :=
    λ: "a" "b",
        let: "h" := !"a" in 
        "a" <- !("b");;
        "b" <- "h".

Definition partition : val :=
  λ: "l" "n" "p",
    (rec: "partition" "i" "j" :=
        if: "j" = "n" then #0
        else 
            Tick #1%R;;
            if: !("l" +ₗ "j") ≤ "p"
            then 
                swap ("l" +ₗ "i") ("l" +ₗ "j");;
                #1 + "partition" ("i" + #1) ("j" + #1)
            else 
                "partition" "i" ("j" + #1))
    #0 #0.


Definition qsort : val := 
    rec: "qsort" "l" "n" :=
        if: "n" ≤ #1 then #() 
        else let: "p" := ChooseRange #0 "n"
        in let: "pos" := partition "l" "n" !("l" +ₗ "p") in 
        "qsort" "l" "pos";;
        "qsort" ("l" +ₗ "pos") ("n" - "pos").


Section proofs.
Context `{!heapGS_gen Σ}.

Lemma swap_spec l1 l2 v1 v2:
    {{{l1 ↦ v1 ∗ l2 ↦ v2 }}} swap #l1 #l2 {{{RET #(); l2 ↦ v1 ∗ l1 ↦ v2}}}.
Proof.
    iIntros (Φ) "[? ?] H". unfold swap. wp_pures. wp_load.
    wp_pures. wp_load. wp_store. wp_store. iApply "H".
    by iFrame.
Qed.

Lemma swap_same_spec l v:
    {{{l ↦ v}}} swap #l #l {{{RET #(); l ↦ v}}}.
Proof.
    iIntros (Φ) "? H". unfold swap. wp_pures. wp_load.
    wp_pures. wp_load. wp_store. wp_store. by iApply "H".
Qed.

(* Def: rank number of elements <= in list *)
Fixpoint rank (vs : list val) z :=
    match vs with 
    | (LitV (LitInt z'))::vs =>
        if decide (z' <= z)%Z then 1 + rank vs z
        else rank vs z
    | _ => 0
    end.

(* An integer list and the first j elements are larger than p *)
Fixpoint is_part (vs : list val) j p:=
    match vs with 
    | LitV (LitInt z)::vs => 
            (match j with 
            | 0 => is_part vs 0 p 
            | S j => (p < z)%Z ∧ is_part vs j p
            end )
    | [] => True
    | _ => False
    end.

Lemma is_part_cons_1 v vs j p :
    is_part (v::vs) (S j) p ↔ ∃ z, v = LitV (LitInt z) ∧ (p < z)%Z ∧ is_part vs j p.
Proof.
    split. 
    - intros H. unfold is_part in H. destruct v; try contradiction.
    destruct l; try contradiction. destruct H. eexists. split; eauto.
    - intros [? [-> [? ?]]] . simpl. done.
Qed.

Lemma is_part_cons_2 v vs p:
    is_part (v::vs) 0 p ↔ (∃ z, v = LitV (LitInt z)) ∧ is_part vs 0 p.
Proof.
    split.
    - intros H. destruct v; try contradiction. destruct l; try contradiction.
      simpl in H. eexists; eauto. 
    - by intros [[? ->] ?].
Qed.

Lemma is_part_full vs j p :
    j = length vs → is_part vs j p → 0 = rank vs p.
Proof.
    induction vs in j |- *; auto. intros Hlen Hpart. 
    destruct j; try by contradict Hlen.
    rewrite is_part_cons_1 in Hpart. destruct Hpart as (? & -> & ? & ?).
    simpl. rewrite decide_False; last lia. apply (IHvs j); auto.
Qed.

Lemma is_part_mono vs j j' p:
    (j >= j') → is_part vs j p → is_part vs j' p.
Proof.
    induction vs in j, j' |- *; intros; auto.
    destruct j, j'; auto.
    - contradict H; lia.
    - rewrite is_part_cons_2. rewrite is_part_cons_1 in H0.
    destruct H0 as (z & -> & ? & ?). split; first eauto.
    apply (IHvs j 0); auto. lia.
    - rewrite is_part_cons_1. rewrite is_part_cons_1 in H0.
    destruct H0 as (z & -> & ? & ?). exists z. repeat split ;auto.
    apply (IHvs j j'); auto. lia.
Qed.

Lemma is_part_app1 vs1 vs2 j p:
    j ≤ length vs1 → 
    is_part (vs1++vs2) j p ↔ is_part vs1 j p ∧ is_part vs2 0 p.
Proof.
    induction vs1 in j, vs2 |-*.
    - simpl. intros. replace j with 0 by lia. tauto.
    - intros H. simpl in H. destruct j.
        + cbn -[is_part]. rewrite !is_part_cons_2.
            rewrite IHvs1; last lia. tauto.
        + cbn -[is_part]. rewrite !is_part_cons_1.
            setoid_rewrite IHvs1; last lia.
         split.
            * intros (z & -> & Hz1 & Hz2 & Hz3). repeat split; auto. eexists; split; auto.
            * intros [[z [-> [? ?]]] ?]. eexists. repeat split; auto.
Qed.
       
Lemma is_part_app2 vs1 vs2 j p:
j = length vs1 + length vs2 → 
is_part (vs1++vs2) j p ↔ 
is_part vs1 (length vs1) p ∧ is_part vs2 (length vs2) p.
Proof.
    intros ->. induction vs1; first (simpl;tauto).
    cbn -[is_part]. rewrite !is_part_cons_1. setoid_rewrite -> IHvs1.
    split.
    - intros (z & -> & ? & ? & ? ). repeat split; eauto.
    - intros [[z [->  []]] ?]. eexists. repeat split; auto.
Qed.
            
(* Generalized partition spec: i = rank -> returns updated rank*)
Lemma partition_spec' i j k n (p: Z) l (vs : list val):
n = j + k → 
n - i = length vs → 
i ≤ j →  
is_part vs (j-i) p → 
⊢ {{{$(INR k), (l +ₗ i) ↦∗ vs}}} 
    (rec: "partition" "i" "j" :=
        if: "j" = #n then #0
        else 
            Tick #1%R;;
            if: !(#l +ₗ "j") ≤ #p
            then 
                swap (#l +ₗ "i") (#l +ₗ "j");;
                #1 + "partition" ("i" + #1) ("j" + #1)
            else 
            "partition" "i" ("j" + #1))%V #i #j
  {{{res, RET #res; ∃ vs', (l +ₗ i) ↦∗ vs' ∗ 
  ⌜ res = (rank vs' p)⌝ ∗ 
  ⌜ vs ≡ₚ vs' ⌝ ∗ 
  ⌜ length vs' = j + k - i ⌝ ∗ 
  ⌜ is_part (drop res vs') (j-i) p ⌝ ; 0%R }}}.
Proof.
    intros Hn Hle1 Hle2 Hpart. iIntros.
    iInduction k as [|k] "IH" forall (i j vs Hle1 Hle2 Hpart Hn).
    - iIntros (Φ vp) "!> Hl HΦ".
    assert (INR 0 >= 0)%R by apply pos_INR.
    wp_pures. 
    rewrite bool_decide_eq_true_2; last first.
    {repeat f_equal. lia. }
    iDestruct ("HΦ" $! 0) as "[H' %]".
    wp_if.
    { simpl. rewrite H0. replace (Z.of_nat 0) with 0%Z by lia. lra. }
     iApply "H'".
    iExists vs. simpl.
    iFrame. iPureIntro. repeat split; auto; last lia. f_equal.
    apply (is_part_full _ (j-i)); first lia. done.
    - iIntros (Φ vp) "!> Hl HΦ".
      assert (0 <= (INR (S k)) )%R by apply pos_INR.
      wp_pures.
      rewrite bool_decide_eq_false_2; last first.
      {intros [=]. lia. }
      wp_pures. rewrite S_INR. wp_tick; first by (rewrite <- S_INR; lra).
      replace (INR k + 1 - 1)%R with (INR k) by lra.
      iNext. assert (0 <= INR k)%R by apply pos_INR.
      destruct vs.
      {contradict Hle1. simpl. lia. }
      wp_pures. destruct (le_gt_dec j i).
        + assert (i=j) as <- by lia. rewrite array_cons.
        iDestruct "Hl" as "[Hli Hl]". wp_load.  
        wp_pures. replace (i-i) with 0 in Hpart by lia.
        rewrite is_part_cons_2 in Hpart.
        destruct Hpart as [[z ->] Hpart]. wp_pures.
        case_decide.
        * rewrite bool_decide_eq_true_2; last done. wp_if.
            wp_bind (swap _ _). iApply wp_eq_pot; first lra.
            wp_pures. iApply (swap_same_spec with "Hli").
            iIntros "!> Hli". do 2 wp_pure.
            wp_bind (λ _, 0%R) (App _ _). simpl in Hle1.
            iSpecialize ("IH" $! (S i) (S i) (vs) with "[] [] [] [] [Hl]"); auto with lia.
            {iPureIntro. by replace (S i - S i) with 0 by lia. }
            {by rewrite Loc.add_assoc Z.add_1_r -!Nat2Z.inj_succ. }
            do 2 wp_pure. rewrite Z.add_1_r -!Nat2Z.inj_succ.
            iApply "IH". iNext. iIntros. iSplit; auto.
            iIntros "(%vs' & Hvs' & % & % & % & %)".
            iDestruct ("HΦ" $! (1 + res)) as "[Hpost %]".
            wp_pure. 
            {rewrite H6 !Z.add_1_l -Nat2Z.inj_succ. simpl. lra. }
            rewrite !Z.add_1_l -!Nat2Z.inj_succ.
             iApply "Hpost". iExists (#z::vs'). iSplitL.
            {iApply array_cons. iFrame. by rewrite Loc.add_assoc Z.add_1_r -!Nat2Z.inj_succ. }
            rewrite Nat.sub_diag.
            iPureIntro. split.
            {simpl. rewrite decide_True; auto. }
            simpl. rewrite Nat.sub_diag in H5.
            repeat split; auto. lia. 
        * rewrite bool_decide_eq_false_2; auto. wp_if.
          iSpecialize ("IH" $! i (S i) (#z::vs) with "[] [] [] [] [Hl Hli]"); auto with lia.
          { iPureIntro. replace (S i - i) with 1 by lia. apply is_part_cons_1.
           exists z. repeat split; auto with lia. }
          { iApply array_cons. iFrame. }
          wp_pure. rewrite Z.add_1_r -!Nat2Z.inj_succ.
          iApply "IH". iIntros "!>" (res).
          iDestruct ("HΦ" $! res%Z) as "[Hpost <-]". iSplit; auto.
          iIntros "(%vs' & ? & % & % & % & %)". iApply "Hpost". iExists vs'.
          iFrame. iPureIntro. repeat split; auto; first lia.
          replace (S i - i) with 1 in H2 by lia. rewrite Nat.sub_diag.
          eapply is_part_mono; last done. lia.
    + rewrite <- (firstn_skipn (j-i-1) vs).
    rewrite <- (firstn_skipn (j-i-1) vs) in Hpart.    
    iDestruct (array_cons with "Hl") as "[Hli Hl]".
    iDestruct (array_app with "Hl") as "[Hl1 Hl2]".
    simpl in Hle1. rewrite take_length_le; last lia.
    rewrite !Loc.add_assoc.
    replace ((Z.of_nat i + 1 + Z.of_nat (j - i - 1)))%Z with (Z.of_nat j) by lia.
    assert (length (drop (j - i - 1) vs) > 0) as Hlen3.
    {rewrite drop_length. lia. }
    destruct (drop (j-i-1) vs) as [|v' vs'] eqn: Hdrop; first by (simpl in Hlen3; lia ).
    iDestruct (array_cons with "Hl2") as "[Hlj Hl2]". 
    wp_load. 
    replace (j - i) with (S (j - i - 1)) in Hpart at 2 by lia.
    rewrite is_part_cons_1 in Hpart. 
    destruct Hpart as(zi & -> & ? & Hpart).
    rewrite is_part_app1 in Hpart; last first.
    { rewrite take_length_le; auto. lia. }
    destruct Hpart as [Hpart1 Hpart2].
    rewrite is_part_cons_2 in Hpart2.
    destruct Hpart2 as [[zj  ->] Hpart2].
    wp_pures. case_decide.
        * rewrite bool_decide_eq_true_2; auto.
        wp_pures. 
        wp_bind (swap _ _). iApply wp_eq_pot; first lra.
        wp_pures. iApply (swap_spec with "[Hli Hlj]"); first iFrame.
        iNext. iIntros "[Hlj Hli]". do 2 wp_pure.
        wp_bind (λ _, 0%R) (App _ _).
        iSpecialize ("IH" $! (S i) (S j) ((take (j - i - 1) vs ++ #zi::vs')) with "[] [] [] [] [Hl1 Hl2 Hlj]"); auto with lia.
        {
            iPureIntro. simpl. rewrite app_length. rewrite take_length_le; last lia. replace (length (#zi ::vs')) with (length (#zj::vs')) by done.
            rewrite <- Hdrop. rewrite drop_length. lia. 
        }
        {
            iPureIntro. replace (S j - S i) with (j - i) by lia.
            rewrite cons_middle. rewrite app_assoc.
            apply is_part_app1.
            { rewrite app_length take_length_le; simpl; lia. }
            split; auto. 
            apply is_part_app2.
            { simpl. rewrite take_length_le; lia. }
            rewrite take_length_le; simpl; last lia.
            split; auto.
        }
        {
          iApply array_app. rewrite take_length_le; last lia.
          rewrite array_cons.
           rewrite !Loc.add_assoc !Z.add_1_r -!Nat2Z.inj_succ.
          iFrame. replace ((S i + (j - i - 1)%nat))%Z with (Z.of_nat j) by lia.
          rewrite -!Nat2Z.inj_succ. iFrame. 
        }

        do 2 wp_pure.  rewrite !Z.add_1_r -!Nat2Z.inj_succ. iApply "IH".
        iIntros (res) "!>". iSplit; last done. iIntros "(%vs'' & Hl & % & % & % & %)".
        iDestruct ("HΦ" $! (1+res)) as "[HΦ %]". wp_op.
        { simpl. rewrite !Z.add_1_l -!Nat2Z.inj_succ. simpl in *. lra. }
        rewrite !Z.add_1_l -!Nat2Z.inj_succ. simpl in *. 
        iApply "HΦ".
        iExists (#zj::vs''). iSplitL.
        {iApply array_cons.  rewrite Loc.add_assoc !Z.add_1_r -!Nat2Z.inj_succ.
        by iFrame. }
        iPureIntro. repeat split; auto.
         ++ simpl. rewrite decide_True; auto.
         ++ simpl. rewrite Permutation_middle.
         rewrite Permutation_swap. rewrite <- Permutation_middle.
         constructor. done.
         ++ simpl. lia.
        * rewrite bool_decide_eq_false_2; auto. wp_if.
          iSpecialize ("IH" $! i (S j) (#zi::(take (j - i - 1) vs ++ #zj::vs')) with "[] [] [] [] [Hli Hlj Hl1 Hl2]"); auto with lia.
        { 
         simpl. iPureIntro. rewrite app_length take_length_le; last lia.
         rewrite <- Hdrop. rewrite drop_length. lia. 
        }
        {
            iPureIntro. rewrite cons_middle. rewrite app_assoc.
            rewrite app_comm_cons.
            apply is_part_app1. 
            { simpl. rewrite app_length. simpl. rewrite take_length_le; lia. }
            split; auto. replace (S j - i) with (S (j-i)) by lia.
            rewrite is_part_cons_1. exists zi. repeat split; auto.
            apply is_part_app2.
            { simpl. rewrite take_length_le; lia. }
            rewrite take_length_le; last lia.
            split; auto. simpl. split; lia.
        }
        {
            rewrite array_cons. iFrame.  iApply array_app. rewrite take_length_le; last lia.
            rewrite array_cons.
             rewrite !Loc.add_assoc !Z.add_1_r -!Nat2Z.inj_succ.
            iFrame. replace ((S i + (j - i - 1)%nat))%Z with (Z.of_nat j) by lia.
            rewrite -!Nat2Z.inj_succ. iFrame. 
        }
        wp_pure. rewrite !Z.add_1_r -!Nat2Z.inj_succ. iApply "IH".
        iIntros (res) "!>". iDestruct ("HΦ" $! (res)) as "[HΦ %]".
        iSplit; last done. iIntros "(%vs'' & Hl & % & % & % & %)".
        iApply "HΦ".
        iExists vs''. iFrame.
        iPureIntro. repeat split; auto.
         ++ lia.
         ++ eapply is_part_mono; last done. lia.
Qed.

Definition is_int_list vs := 
    Forall (λ v, ∃ z,  v= LitV (LitInt z) ) vs.

Lemma int_list_is_part p vs:
    is_int_list vs → is_part vs 0 p.
Proof.
    intros. induction vs; first done.
    rewrite is_part_cons_2. unfold is_int_list in H.
    rewrite Forall_cons in H. destruct H. split; auto.
Qed.


(* Specific partition spec *)        
Lemma partition_spec l vs p:
is_int_list vs → 
⊢ {{{$(INR (length vs)), l ↦∗ vs}}} 
    partition #l #(length vs) #p 
  {{{res, RET #res; ∃ vs', l ↦∗ vs' ∗
  ⌜ res = (rank vs' p)⌝ ∗ 
  ⌜ vs ≡ₚ vs' ⌝ ∗ 
  ⌜ length vs' = length vs ⌝ ; 0%R }}}.
Proof.
    intros Hint. unfold partition. iIntros (Φ vp) "!> Hl HΦ".
    assert (0 <= INR (length vs))%R by apply pos_INR.
    do 6 wp_pure.
    iPoseProof ((partition_spec' 0 0 (length vs) (length vs) p l vs) with "[Hl]") as "part_spec"; auto with lia.
    {simpl. by apply int_list_is_part. }
    {by rewrite Loc.add_0.  }
    replace #0%nat with #0 by done.
    iApply "part_spec". iIntros "!>" (res).
    iDestruct ("HΦ" $! res) as "[HΦ %]". iSplit; last done.
    rewrite Loc.add_0. simpl. rewrite Nat.sub_0_r.
    iIntros "(%vs' & ? & ? & ? & ? & ?)". iApply "HΦ".
    iExists vs'. iFrame.
Qed.


Section cost_lemmata.

Require Import Coquelicot.Derive.
Require Import Coquelicot.Coquelicot.

Definition cost r : R := 2 * r *(1 + Rlog (4/3) r).

Lemma ln_pos x : 1 < x  → 0 < ln x.
Proof.
    intros. rewrite <- ln_1. apply ln_increasing; lra.
Qed.

Lemma ln_mono x y: 0 < x → x <= y → ln x <= ln y.
Proof.
    destruct 2.
    - left. apply ln_increasing; auto.
    - simplify_eq. lra.
Qed.

Lemma Rlog_increasing b x y : b > 1 → 0 < x → x < y → Rlog b x < Rlog b y.
Proof.
    unfold Rlog. intros. field_simplify; try (apply ln_neq_0; lra).
    apply Rmult_lt_compat_r; last by apply ln_increasing.
    apply Rinv_0_lt_compat. apply ln_pos; nra.
Qed.

Lemma Rlog_mono b x y : b > 1 → 0 < x → x <= y → Rlog b x <= Rlog b y.
Proof.
    unfold Rlog. intros. field_simplify; try (apply ln_neq_0; lra).
    apply Rmult_le_compat_r; last by apply ln_mono.
    left. apply Rinv_0_lt_compat. apply ln_pos; nra.
Qed.

Lemma Rlog_mult x y b : 0 < x → 0 < y → Rlog b (x * y) = Rlog b x + Rlog b y.
Proof.
    intros. unfold Rlog.  rewrite ln_mult; auto. nra.
Qed.

Lemma Rlog_inv x b : 0 < x → Rlog b (/x) = - Rlog b x.
Proof.
    intros. unfold Rlog. rewrite ln_Rinv; auto. lra.
Qed.

Lemma Rlog_div x y b : 0 < x → 0 < y → Rlog b (x/y) = - Rlog b (y/x).
Proof.
    intros. rewrite !Rlog_mult; auto; first by (rewrite !Rlog_inv; lra).
    all: by apply Rinv_0_lt_compat.
Qed.

Lemma Rlog_pow b n: 1 < b → Rlog b (b^n) = INR n.
Proof.
    intros. unfold Rlog. rewrite ln_pow; try lra.
    field_simplify; auto. apply ln_neq_0; nra.
Qed.

Lemma Rlog_id b : 1 < b →  Rlog b b = 1.
Proof.
    unfold Rlog. intros. rewrite Rdiv_diag; auto.
    apply ln_neq_0; nra.
Qed.

Lemma Rlog_pow' b x n: 1 < b → 0 < x → Rlog b (x^n) = INR n * Rlog b x.
Proof.
    intros. unfold Rlog. rewrite ln_pow; auto. nra.
Qed.

Lemma cost_pos (n : nat) : (0 < n)%nat → (0 < cost (INR n))%R.
Proof.
    intros. destruct n; first (simpl; lia). unfold cost. 
    enough (-1 < Rlog (4/3) (INR (S n))).
    - enough (0 < INR (S n)) by nra. apply INR_gt_0; lia.
    - replace (-1) with (-(1)) by done. 
    rewrite <- (Rlog_id (4/3)); last nra.
    rewrite <- Rlog_div; try lra.
    apply Rlog_increasing; try lra. rewrite S_INR.
    enough (0 <= INR n) by nra. apply pos_INR.
Qed.

Lemma cost_nonneg n: cost (INR n) >= 0.
Proof.
    intros. destruct n; unfold cost; first (simpl; nra).
    left. apply cost_pos. lia.
Qed.

Lemma cost_good_split' r: 0 < r → ((r + cost ((3/4) * r) + cost ((1/4) * r)) <= cost r - 2* r)%R.
Proof.
    intros Rpos. unfold cost. rewrite Rlog_mult; try lra.
    rewrite Rlog_mult; try lra.
    assert (Rlog (4 / 3) (1 / 4) < -4%R) .
    {
        rewrite Rlog_div; try lra. apply Ropp_lt_contravar.
        replace 4 with (Rlog (4/3) ((4/3)^4) ) at 1;
        first by (apply Rlog_increasing; nra).
        rewrite Rlog_pow; last nra. apply INR_IZR_INZ.
    }
    enough (Rlog (4/3) (3/4) = -1) as -> by nra.
    rewrite Rlog_div; try lra. rewrite Rlog_id; nra.
Qed.

Definition cost2 r r' := cost r' + cost (r - r').

Lemma cost2_deriv r r' : 0 < r' < r → 0 < Derive (cost2 r) r' ↔ r < 2 * r'.
Proof.
    intros.
    assert (0 < ln (4/3)) by (apply ln_pos; lra).
    erewrite is_derive_unique; last first.
    { 
        unfold cost2, cost, Rlog. auto_derive; first by repeat split; lra.
        field_simplify; first reflexivity. repeat split; lra.
    }
    setoid_rewrite <- Rlt_div_r; auto. split; intros Hle.
    {
        field_simplify in Hle. enough (r' > r + - r') by lra.  apply ln_lt_inv; lra.
    }
    field_simplify. assert (r' > r + - r') as ?%ln_increasing by lra; lra.
Qed.

Lemma cost2_symm n r: cost2 n r = cost2 n (n - r).
Proof.
    unfold cost2. replace (n - (n - r)) with r by lra. lra.
Qed. 

Lemma cost_perfect_split r: 0 < r →  2* r + 2* cost r <= cost (2*r) - 4*r.
Proof.
    intros. unfold cost. rewrite Rlog_mult; try lra. field_simplify.
    apply Rplus_le_compat_l. enough (3/2 <= Rlog (4/3) 2) by nra.
    enough (3 <= 2 * Rlog (4/3) 2) by lra.
    replace 3 with (Rlog (4/3) ((4/3)^(3))) at 1; last rewrite Rlog_pow; try lra;
    last apply INR_IZR_INZ.
    replace 2 with (INR 2) at 1 by done. rewrite <- Rlog_pow'; try lra.
    apply Rlog_mono; try lra.
Qed.

Lemma cost_good_split n r : 0 < n → (1/4)* n <= r <= (3/4) * n → (n + cost r + cost (n-r) <= cost n - 2 * n).
Proof.
    intros. destruct (Req_dec n (2*r)) as [-> | ?].
    { 
      replace (2 * r - r) with r by lra. specialize (cost_perfect_split r) as Hperfect.
      feed specialize Hperfect; try lra.
    }
    eapply Rle_trans; last apply cost_good_split'; auto.
    rewrite !Rplus_assoc. apply Rplus_le_compat_l. replace (1/4 * n) with (n - 3/4 * n ) by lra.
    replace (cost r + cost (n - r)) with (cost2 n r) by by unfold cost2.
    replace (cost (3 / 4 * n) + cost (n - 3 / 4 * n)) with (cost2 n (3/4*n)) by by unfold cost2.
    destruct (Rlt_dec n (2*r)).
    {
        destruct (Req_dec r (3/4*n)); first (simplify_eq; lra ).  left.
        eapply (incr_function _ (Finite (1/2*n)) (Finite n)); try (simpl; lra).
        {
            simpl. intros. eapply Derive_correct. unfold cost2, cost, Rlog. auto_derive.
            repeat split; lra.
        }
        {
            simpl. intros. apply cost2_deriv; try lra.   
        }
    }
   
    rewrite cost2_symm. destruct (Req_dec (n-r) (3/4*n)) as [-> |?]; first lra.
    left.
    eapply (incr_function _ (Finite (1/2*n)) (Finite n)); try (simpl; lra).
    {
        simpl. intros. eapply Derive_correct. unfold cost2, cost, Rlog. auto_derive.
        repeat split; lra.
    }
    {
        simpl. intros. apply cost2_deriv; try lra.   
    }
Qed.

Lemma cost_bad_split r k: 0 < k <= r → r + cost k + cost (r - k) <= cost r + r.
Proof.
    intros. unfold cost.
    destruct (Req_dec r k).
    { simplify_eq. lra. }
    assert (Rlog (4/3) k <= Rlog (4/3) r) .
    {apply Rlog_mono; lra. }
    assert (Rlog (4/3) (r-k) <= Rlog (4/3) r).
    {apply Rlog_mono; lra. }
    nra.
Qed.

End cost_lemmata.

Lemma map_nth {A} `{!Inhabited A} (xs : list A) : map (λ n, xs !!! n) (seq 0 (length xs)) = xs.
Proof.
    induction xs; auto. simpl in *. f_equal. rewrite <- IHxs at 2.
    rewrite <- seq_shift. by rewrite map_map.
Qed.

Definition to_int v := match v with | LitV (LitInt z) => z | _ => 0 end.

Lemma rank_mono x y vs : is_int_list vs →  (x ≤ y)%Z → (rank vs x ≤ rank vs y).
Proof.
    intros Hint Hleq. induction vs; simpl; first lia. rewrite /is_int_list Forall_cons in Hint. destruct Hint as [[z ->] Hint]. case_decide.
    - rewrite decide_True; last lia. apply le_n_S. by apply IHvs.
    - case_decide.
        * apply le_S. by apply IHvs.
        * by apply IHvs.
Qed.

Lemma rank_incr x y vs : is_int_list vs → (x < y)%Z → LitV (LitInt y) ∈ vs → (rank vs x < rank vs y).
Proof.
    intros Hint Hle Helem. induction vs;simpl; first set_solver.
    rewrite /is_int_list Forall_cons in Hint. destruct Hint as [[z ->] Hint].
    rewrite elem_of_cons in Helem. destruct Helem as [ [=Heq] |Helem].
    - simplify_eq. rewrite decide_False; last lia. rewrite decide_True; last lia.
    enough (rank vs x ≤ rank vs z) by lia. apply rank_mono; auto. lia.
    - case_decide.
        * rewrite decide_True; last lia. apply le_n_S. by apply IHvs.
        * case_decide; last by apply IHvs.
          apply le_S. by apply IHvs.
Qed.

Lemma rank_NoDup vs : is_int_list vs → NoDup vs → NoDup (map (λ v, rank vs (to_int v)) vs).
Proof.
    intros Hint Hdup. rewrite NoDup_alt. intros i j x H1 H2. 
    apply map_lookup_Some in H1, H2. destruct H1 as (v1 & Hlook1 & ?).
    destruct H2 as (v2 & Hlook2 & ?). simplify_eq. 
    rewrite NoDup_alt in Hdup. apply (Hdup _ _ v1); auto.
    enough (v1 = v2) by by simplify_eq.
    unfold is_int_list in Hint. 
    apply (Forall_lookup_1 _ _ j v2) in Hint as Hint1; auto.
    destruct Hint1 as [y ->].
    apply (Forall_lookup_1 _ _ i v1) in Hint as Hint2; auto.
    destruct Hint2 as [x ->].
    simpl in *.
    destruct (Z.lt_trichotomy x y).
    - enough (rank vs x < rank vs y) by lia.
        apply rank_incr; auto. unfold to_int. 
        by eapply elem_of_list_lookup_2.
    - repeat f_equal. destruct H; first done.
    enough (rank vs y < rank vs x) by lia.
    apply rank_incr; auto. unfold to_int. 
    by eapply elem_of_list_lookup_2.
Qed.
        
Lemma rank_length vs x: rank vs x ≤ length vs.
Proof.
    induction vs; auto. simpl. destruct a; try lia. destruct l; try lia.
    case_decide; lia.
Qed.

Lemma rank_pos vs v : is_int_list vs →  v ∈ vs → 1 ≤ rank vs (to_int v) .
Proof.
    intros. induction vs; first set_solver.
    inversion H; simplify_eq. destruct H3 as [z ->].
    inversion H0; simplify_eq.
    - simpl. rewrite decide_True; lia.
    - simpl. case_decide; first lia. apply IHvs; auto.
Qed.

Lemma rank_perm vs : 
NoDup vs → is_int_list vs → map (λ v, rank vs (to_int v)) vs ≡ₚ seq 1 (length vs).
Proof.
    intros. apply NoDup_Permutation_bis.
        - apply NoDup_ListNoDup. apply rank_NoDup; auto.
        - rewrite map_length. rewrite seq_length. done.
        - unfold incl. intros a. rewrite in_map_iff in_seq.
        intros (x & <- & Hin). split.
            * apply rank_pos; auto. by apply elem_of_list_In.
            * simpl. apply le_n_S. apply rank_length.
Qed.

Lemma is_int_list_perm vs vs': vs ≡ₚ vs' → is_int_list vs → is_int_list vs' .
Proof.
    induction 1; auto.
        - inversion 1; simplify_eq. 
         apply Forall_cons. split; first eauto. apply IHPermutation.
         eapply Forall_cons. done.
         - inversion 1; simplify_eq. inversion H3; simplify_eq.
           constructor; first eauto. constructor; first eauto.
           done.
Qed.

Lemma is_int_list_take k vs : is_int_list vs → is_int_list (take k vs).
Proof.
    induction vs in k |-* ; destruct k; simpl; auto.
    - intros. by apply Forall_nil.
    - unfold is_int_list. rewrite !Forall_cons. intros [].
    split; auto. apply IHvs. done.
Qed.

Lemma is_int_list_drop k vs : is_int_list vs → is_int_list (drop k vs).
Proof.
 induction vs in k |-* ; destruct k; simpl; auto.
 intros. apply IHvs. eapply Forall_cons. done.
Qed.

Lemma rank_perm2 vs vs' x: vs ≡ₚ vs' → is_int_list vs →  rank vs x = rank vs' x.
Proof.
    induction 1; auto.
    - inversion 1; simplify_eq. destruct H3 as (z & ->). simpl.
        case_decide; auto.
    - inversion 1. inversion H3. simplify_eq. destruct H2 as (z & ->).
    destruct H6 as (z' & ->). simpl.
    repeat case_decide; auto.
    -  simplify_eq. intros. rewrite IHPermutation1; last done.
       apply IHPermutation2. by eapply is_int_list_perm.
Qed.

Lemma Rdiv_bound a b : 0 < b→  (INR(a)/INR(b) - 1 < INR (a/b) <= INR(a)/INR(b))%R.
Proof.
    split.
    - specialize (Nat.div_mod a b) as Hmod; auto.
    setoid_rewrite Hmod at 1; last lia.
    specialize (Nat.mod_upper_bound a b) as Hmod'%lt_INR; last lia.
    rewrite plus_INR mult_INR. 
    apply (Rplus_lt_reg_l 1), (Rmult_lt_reg_l (INR b)); first by apply INR_gt_0.
    field_simplify; first lra. enough (0 < INR b)%R by lra. by apply INR_gt_0.
    - specialize (Nat.Div0.div_mul_le a b b) as Hle%le_INR.
    rewrite !mult_INR in Hle.
    rewrite Nat.mul_comm Nat.div_mul in Hle; last lia.
    specialize (INR_gt_0 b H) as Hpos.
    apply (Rmult_le_reg_r (INR b)); first auto.
    rewrite Rmult_comm. eapply Rle_trans; first exact Hle.
    right. rewrite Rmult_comm Rmult_div_assoc Rmult_comm.
    rewrite Rmult_div_l; lra.
Qed.
    
Definition pot_fn `{!Inhabited val} vs v : R := 
    cost (INR (rank vs (to_int (vs !!! Z.to_nat (to_int v))))) +
    cost (INR((length vs) - (rank vs (to_int (vs !!! Z.to_nat (to_int v)))) )) + INR (length vs).

Lemma qsort_spec l vs: 
    NoDup vs → 
    is_int_list vs → 
    {{{$(cost (INR (length vs))), l ↦∗ vs }}}
        qsort #l #(length vs)
    {{{RET #(); True; R0}}}.
Proof.
    intros Hdup Hint Φ vp. iLöb as "IH" forall (vs Hdup Hint Φ vp l).
    iIntros "Hl HΦ".
    assert (cost (INR (length vs)) >= 0)%R by apply cost_nonneg.
    unfold qsort. wp_pures. case_decide; wp_pures;
    iDestruct "HΦ" as "[HΦ %]"; first by (wp_if; by iApply "HΦ").
    fold qsort.
    wp_apply (ChooseRange_spec 0 (length vs) (cost (INR (length vs))) (pot_fn vs) ) use (pot_fn vs); auto with lia.
    - rewrite Nat.sub_0_r. unfold pot_fn. simpl.
      erewrite map_ext; last by (intros; erewrite Nat2Z.id).
      enough (((R_list_sum  (map
      (λ a : nat, (cost (INR a) + cost (INR (length vs - a)) + INR (length vs))%R)
      ((map (λ v, rank vs (to_int v)))   
      (map  (λ a, (vs !!! a)) (seq 0 (length vs)))))) / INR (length vs) <= 
      cost (INR (length vs)))%R) as H2 by by rewrite !map_map in H2.
      rewrite map_nth. erewrite R_list_sum_perm; last first.
        { eapply Permutation_map. apply rank_perm; auto. }
      assert (0 < INR (length vs) )%R by (apply INR_gt_0; lia).
      apply (Rmult_le_reg_r (INR (length vs))); auto.
      rewrite Rmult_assoc. setoid_rewrite Rinv_l; last lra.
      rewrite Rmult_1_r. remember (INR (length vs)) as n.
      eapply Rle_trans.
      { eapply (R_list_sum_mono2 _ _ (λ a, (cost n + n *
        (if Rle_dec (INR a) (n/4) then 1 else
        if Rle_dec (INR a) (3*n/4) then - 2 else 1)))%R).
        intros. 
        apply elem_of_seq in H3. 
        specialize (cost_bad_split n (INR a)) as Hbad;
        rewrite Heqn in Hbad; feed specialize Hbad.
           { split; first (apply INR_gt_0; lia ). apply le_INR. lia. }
        rewrite <- minus_INR in Hbad; last lia. 
        destruct (Rle_dec (INR a) (n / 4));
        last destruct (Rle_dec (INR a) (3*n / 4)).
        1,3: rewrite Heqn; lra.
        specialize (cost_good_split n (INR a)) as Hgood.
        rewrite Heqn in Hgood. feed specialize Hgood.
          {  rewrite <- Heqn. lra. }
        rewrite <- minus_INR in Hgood; last lia.
        rewrite Heqn. lra.
      }
      rewrite R_list_sum_add. erewrite R_list_sum_same; last first.
        { apply Forall_map. apply Forall_forall. intros. reflexivity. }
      rewrite map_length. rewrite seq_length.
      replace (cost n * n)%R with (n * cost n + 0)%R by lra.
      rewrite <- Heqn. apply Rplus_le_compat_l.
      erewrite map_ext; last first.
        { intros. rewrite Rmult_comm. reflexivity. }
      rewrite R_list_sum_mult. 
      apply (Rmult_le_reg_r (/n)); first by by apply Rinv_0_lt_compat.
      rewrite Rmult_inv_r_id_l; last lra. rewrite Rmult_0_l.
      specialize (seq_app ((length vs) / 4) (length vs - (length vs) / 4) 1) .
      rewrite Nat.add_sub_assoc; last first.
        { apply Nat.lt_le_incl. apply Nat.div_lt; lia. }
      rewrite Nat.sub_add'. intros ->. rewrite map_app R_list_sum_app.
      assert ((length vs) / 4 ≤ 3 * (length vs) / 4).
        { apply Nat.Div0.div_le_mono. lia. }
      assert (3 * (length vs) / 4 ≤ length vs).
        { apply Nat.Div0.div_le_upper_bound. lia. }
      erewrite R_list_sum_same; last first.
      {
        rewrite Forall_map. rewrite Forall_forall. intros x Helem%elem_of_seq.
        apply decide_True.
        specialize (Rdiv_bound (length vs) 4) as Hdiv.
        destruct Hdiv as [_ Hdiv]; first lia.
        destruct Helem as [_ Hx2].
        rewrite Nat.lt_succ_r in Hx2.
        apply le_INR in Hx2.
        eapply Rle_trans; first exact Hx2.
        simpl in Hdiv. field_simplify in Hdiv.
        simpl. by rewrite Heqn.
      }
      rewrite map_length. rewrite seq_length.
      remember (length vs) as le.
      specialize (seq_app ((3 * le) / 4 - le / 4) (le - (3 * le) / 4) (1 + le / 4)).
      assert (3 * le / 4  - le/4 + (le - 3 * le / 4 ) = le - le/4) as -> by lia.
      intros ->. rewrite map_app. rewrite R_list_sum_app. erewrite R_list_sum_same; last first.
      {
        rewrite Forall_map. rewrite Forall_forall. intros x Helem%elem_of_seq.
        rewrite <- Nat.add_assoc in Helem.
        rewrite Arith_prebase.le_plus_minus_r_stt in Helem; auto.
        rewrite Nat.lt_succ_r in Helem.
        destruct Helem as [Hx1 Hx2].
        apply le_INR in Hx1, Hx2.
        rewrite decide_False; first rewrite decide_True; first reflexivity. 
        - specialize (Rdiv_bound (3*le) 4) as [_ Hb]; first lia.
          eapply Rle_trans; first exact Hx2. eapply Rle_trans; first exact Hb.
          rewrite Heqn mult_INR. simpl. lra. 
        - specialize (Rdiv_bound le 4) as [Hb _]; first lia.
          enough (n/4  < INR x)%R by nra.
          eapply Rlt_le_trans; last apply Hx1.
          apply (Rplus_lt_reg_r (-1)). 
          rewrite S_INR. rewrite -Heqn in Hb.
          simpl in Hb. field_simplify in Hb. field_simplify.
          done.
     }
     rewrite map_length seq_length.
     erewrite R_list_sum_same; last first.
    {
      rewrite Forall_map Forall_forall. intros x Helem%elem_of_seq.
      do 2 rewrite -Nat.add_assoc in Helem.
      rewrite !Arith_prebase.le_plus_minus_r_stt in Helem; auto.
      destruct Helem as [Hx1 Hx2].
      apply le_INR in Hx1, Hx2.
      assert (3 * n/4 < INR x)%R.
      {
        specialize (Rdiv_bound (3*le) 4) as [Hb _ ]; first lia.
        eapply Rlt_le_trans; last exact Hx1. apply (Rplus_lt_reg_r (-1)). 
        rewrite S_INR. rewrite mult_INR in Hb. rewrite -Heqn in Hb.
        simpl in Hb. field_simplify in Hb. field_simplify.
        done.
       }
       rewrite !decide_False; first reflexivity; first lra.
       enough (n/4  < INR x)%R by nra.
       eapply Rle_lt_trans; last eassumption. lra.
     }
     rewrite map_length seq_length. rewrite !minus_INR; auto.
     field_simplify.
     destruct (Nat.eq_dec le 2) as [-> | ?]; first (simpl; lra).
     destruct (Nat.eq_dec le 3) as [-> | ?]; first (simpl; lra).
     destruct (Nat.eq_dec le 4) as [-> | ?]; first (simpl; lra).
     destruct (Nat.eq_dec le 5) as [-> | ?]; first (simpl; lra).
     specialize (Rdiv_bound le 4) as [_ Hb1]; first lia.
     specialize (Rdiv_bound (3*le) 4) as [Hb2 _]; first lia.
     enough ((3 * INR le / INR 4) - 3 * (INR (3 * le) / INR 4 - 1) + INR le <= 0)%R by lra. rewrite mult_INR.
     simpl. field_simplify. enough ((INR le) >= 6)%R by nra. 
     assert (le >= 6) as ?%le_INR by lia. simpl in H5. lra.
  - iIntros (i).
    iSplit; auto. iIntros "%Hn". assert (pot_fn vs #i >= 0)%R.
    {
      unfold pot_fn. apply Rplus_nneg_nneg.
      - apply Rplus_nneg_nneg; apply cost_nonneg.
      - specialize (pos_INR (length vs)). lra.
    } 
    wp_pures. 
    wp_apply (wp_load_offset with "[Hl]"); auto.
      { apply list_lookup_lookup_total_lt. lia. }
    iSplit; auto. iIntros "Hl".
    eapply (Forall_lookup_1 _ _ i (vs !!! i)) in Hint as Hint1; 
    last (apply list_lookup_lookup_total_lt; lia).
    simpl in Hint1. destruct Hint1 as (n & Hlook). rewrite Hlook.
    unfold pot_fn. 
    remember (((cost (INR (rank vs (to_int (vs !!! Z.to_nat (to_int #i))))) +
    cost (INR (length vs - rank vs (to_int (vs !!! Z.to_nat (to_int #i)))))))%R)
    as cost_rec.
    assert (cost_rec >= 0)%R.
     { simplify_eq. apply Rplus_nneg_nneg; apply cost_nonneg. }
    wp_bind (λ _, (0 + cost_rec)%R) (partition _ _ _) .
    rewrite Rplus_comm.
    wp_apply wp_pot_add.
    wp_apply (partition_spec with "Hl"); auto.
    iIntros (res). iSplit; auto. iIntros "(%vs' & Hl & -> & %Hperm & %Hlen)".
    rewrite Rplus_0_l. wp_pures.
    rewrite (rank_perm2 vs' vs n); auto; first last.
      { by eapply is_int_list_perm. }
      { by symmetry. }
    simplify_eq. simpl. rewrite Nat2Z.id. rewrite Hlook. simpl.
    wp_bind (λ _, (0 + cost (INR (length vs - rank vs n)))%R) (qsort _ _).
    wp_apply wp_pot_add; first by apply cost_nonneg.
    rewrite <- (firstn_skipn (rank vs n) vs').
    iDestruct (array_app with "Hl") as "[Hl1 Hl2]".
    assert (rank vs n = rank vs' n)  as -> by by (eapply rank_perm2).
    assert (length (take (rank vs' n) vs') = rank vs' n) as Hlen1.
     { apply take_length_le. apply rank_length. }
    setoid_rewrite <- Hlen1 at 4 5.
    wp_apply ("IH" with "[] [] Hl1").
    + iPureIntro. enough (NoDup ((take (rank vs' n) vs' ++ drop (rank vs' n) vs'))).
        * apply NoDup_app in H4. tauto.
        * rewrite firstn_skipn. apply NoDup_ListNoDup. eapply Permutation_NoDup;  first exact Hperm.
        by apply NoDup_ListNoDup.
    + iPureIntro. eapply is_int_list_take. eapply is_int_list_perm; first exact  Hperm. done.
    + iSplit; auto. iIntros "_". rewrite Rplus_0_l.
      assert (cost (INR (length vs - rank vs' n)) >= 0)%R by apply cost_nonneg.
      wp_pures. rewrite  Hlen1 -Hlen.
      assert (length (drop (rank vs' n) vs') = length vs' - rank vs' n) as Hlen3 by apply drop_length.
      rewrite <- Nat2Z.inj_sub; last by apply rank_length.
      rewrite <- Hlen3. wp_apply ("IH" with "[] [] Hl2"); auto.
      * iPureIntro. enough (NoDup ((take (rank vs' n) vs' ++ drop (rank vs' n) vs'))).
        ++ apply NoDup_app in H5. tauto.
        ++ rewrite firstn_skipn. apply NoDup_ListNoDup. eapply Permutation_NoDup; first exact Hperm.
        by apply NoDup_ListNoDup.
      * iPureIntro. eapply is_int_list_drop. eapply is_int_list_perm; first exact Hperm. done.
Qed.

End proofs.