From iris.proofmode Require Import proofmode.
From iris.program_logic Require Export weakestpre.
From iris.heap_lang Require Export lang.
From iris.heap_lang Require Import proofmode notation.
From iris.heap_lang.lib Require Import array.
From iris.prelude Require Import options.
From iris.prelude Require Import distributions.

Require Import Lra.
Require Import Reals.

Definition flip_bit : val :=
    rec: "flip" "l" "p" :=
        Tick #1%R;;
        if: ChooseWeighted 
         (("p", #true) :: ((#1%R - "p"), #false) :: (LitListV []))
        then "l" <- ~ (!"l")
        else "flip" "l" "p".


Definition init_counter : val := λ: "n", array_init "n" (λ: "_", #false ).

Definition incr_counter : val := λ: "n" "l" "p",
    (rec: "incr" "l" "i" "n" :=
        if: "i" = "n" then #()
        else  flip_bit ("l" +ₗ "i") "p" ;;
           if: !("l" +ₗ "i")
           then #()
           else "incr" "l" ("i" + #1) "n")
     "l" #0 "n".

Section proofs.
Context `{!heapGS_gen Σ}.


Lemma flip_bit_spec l p (b : bool):
   (p > 0)%R → (p < 1)%R → 
    {{{$(1%R/p)%R, l ↦ #b }}} (flip_bit #l #p) {{{RET #(); l ↦ #(negb b); R0 }}}.
Proof.
    intros. iIntros "Hl Hpost". iLöb as "IH". simpl.
    assert (1/p > 0)%R. { apply Rdiv_pos_pos; lra. }
    wp_rec. wp_pures. wp_tick. 
    assert (1/p - 1 > 0)%R. 
    { field_simplify; nra. }
    iNext. wp_pures. 
    wp_choose_weighted (λ v, match v with | #false => (1/p)%R | _ => 0%R end); simpl.
    {field_simplify; lra. }
    repeat iSplit; auto; wp_pures. 
    - wp_load. wp_pures. iDestruct "Hpost" as "[Hpost %]".
      wp_store. by iApply "Hpost".
    - iApply ("IH" with "Hl Hpost").
Qed.

(* number of bits set in the counter*)
Fixpoint n_set_bits vs :=
    match vs with 
        | #true::bs => (1%R + n_set_bits bs)%R 
        | #false::bs => n_set_bits bs
        | _ => 0%R
    end.

Lemma n_set_bits_nonneg vs :
    (n_set_bits vs >= 0)%R.
Proof.
    induction vs; simpl; first lra.
    destruct a; simpl; (try lra). destruct l; simpl; try lra.
    destruct b; lra.
Qed.

(* increment: flip all bits until encountering the first 0 *)
Fixpoint incr vs :=
    match vs with 
     | #false::vs => #true::vs 
     | #true::vs => #false::(incr vs)
     | _ => []
    end.

(* The increment operation increases the number of set bits by at most one *)
Lemma n_set_bits_incr vs:
    ((n_set_bits (incr vs)) <= 1 + n_set_bits vs)%R.
Proof.
    unfold n_set_bits. induction vs; simpl; first lra.
    destruct a; simpl; try lra. destruct l; simpl; try lra.
    destruct b; simpl; lra.
Qed.

(*l represents a counter: l points to a list of boolean values*)
Definition is_counter l vs:=
    (l ↦∗ vs ∗  ⌜ Forall (λ v, ∃ b, v = LitV (LitBool b)) vs ⌝)%I. 

(* A generalization is needed to prove the specification for incr_counter below*)
Lemma incr_counter_spec' (n : Z) (l : loc) p (i : nat) k vs:
   (p > 0)%R → (p < 1)%R → 
   (n = Z.of_nat (i + k)) →
   k = length vs  → 
   is_counter (l +ₗ i) vs -∗ 
   $ ((1/p) + ((1/p) + (1/p) * (n_set_bits vs)))%R WP
   (rec: "incr" "l" "i" "n" :=
        if: "i" = "n" then #()
        else  flip_bit ("l" +ₗ "i") #p ;;
           if: !("l" +ₗ "i")
           then #()
           else "incr" "l" ("i" + #1) "n")
     #l #i #n
    {{v, (⌜ v = #() ⌝ ∗ is_counter (l +ₗ i) (incr vs)) ,((1/p) *  (n_set_bits (incr vs)%R))%R }}.
    Proof.
        intros Hp1 Hp2 Hn Hlen . 
        assert (0 < 1/p )%R. { apply Rdiv_pos_pos; lra. }
        iIntros "H". wp_pure.
        { specialize (n_set_bits_nonneg vs) as ?. nra. }
        iInduction k as [|k] "IH" forall (i vs Hn Hlen); simplify_eq/=;
        specialize (n_set_bits_nonneg vs) as Hnonneg;
        assert (1 / p + 1 / p * (n_set_bits vs) >= 0)%R. 1,3: nra.
        - replace (i+0) with i by lia. wp_pures. 
         rewrite bool_decide_eq_true_2; auto. wp_if.
         {specialize (n_set_bits_incr vs) as ?. nra. }
        iSplitR; auto. destruct vs; auto. contradict Hlen; done.
        - wp_pures. rewrite bool_decide_eq_false_2.
        2: { intros [=H']. lia. }
        wp_pures. destruct vs; try by contradict Hlen.
        wp_bind (λ _ :val , (R0 + (1 / p + 1 / p * (n_set_bits (v :: vs))))%R) (flip_bit _ _) . iDestruct "H" as "[Hl %Hvs]".
        rewrite Forall_cons in Hvs.
        destruct Hvs as [[b ->] Hvs]. simpl.
        wp_apply (wp_pot_add ). { simpl in Hnonneg. nra. }
        iDestruct (array_cons with "Hl") as "[Hl HSl]".
        wp_smart_apply ((flip_bit_spec (l +ₗ i) p b) with "[Hl]"); auto.
        iSplit; auto. iIntros "Hl". 
        assert ((R0 + (1 / p + 1 / p * (n_set_bits (#b :: vs))) >= 0)%R) by nra.
        wp_pures. wp_load. destruct b. 
            + simpl. wp_if. wp_op.
            rewrite Loc.add_assoc. rewrite Z.add_1_r -!Nat2Z.inj_succ. 
            iSpecialize ("IH" $! (S i) vs with "[] [] [HSl]"); auto with lia; iFrame; auto. 
            iApply (wp_wand_pot with "IH").
             * iIntros (v) "[$ [HSl %Hvs']]".
               rewrite Loc.add_assoc Z.add_1_r -Nat2Z.inj_succ. iFrame.
               rewrite Forall_cons. iSplit; auto. iPureIntro. by eexists.
             * iPureIntro. lra.
             * iIntros. iPureIntro. lra.
            + simpl. wp_if. iSplitR; auto. iSplitL.
                * iCombine "Hl HSl" as "Hl".
                 by rewrite array_cons. 
                 * iPureIntro. apply Forall_cons. split; auto. by eexists.
Qed.

Lemma incr_counter_spec (n : nat) (l : loc) p vs:
    (p > 0)%R → (p < 1)%R → 
    n = length vs → 
    {{{$ ((2/p) + (1/p) * (n_set_bits vs))%R, is_counter l vs }}}
        incr_counter #n #l #p 
    {{{RET #(); is_counter l (incr vs) ;
     ((1/p) * (n_set_bits (incr vs)))%R}}}.
Proof.
    intros. iIntros "Hcnt H".
    unfold incr_counter.
    assert (2 / p + 1 / p * (n_set_bits vs) >= 0)%R. 
    { assert ((1/p) > 0)%R by (apply Rdiv_pos_pos; lra).
    specialize (n_set_bits_nonneg vs) as Hnonneg.
    nra.
     }
    wp_lam. wp_let. wp_let.
    iPoseProof ((incr_counter_spec' n l p 0 n vs) with "[Hcnt]") as "Hcnt_spec";
    auto with lia; first by rewrite Loc.add_0.
    replace (#0%nat) with #0 by done.
    iDestruct "H" as "[H1 %]".
    iApply (wp_wand_pot with "Hcnt_spec [H1]").
    + iIntros (v) "[-> ?]". iApply "H1".
        by rewrite Loc.add_0.
    + iPureIntro. lra.
    + iIntros (v) "[-> _]" . iPureIntro. lra.
Qed.

Lemma init_counter_spec (n : nat) :
     (0 < n) → 
    {{{True}}} (init_counter #n) {{{l, RET #l; ∃ vs, 
    is_counter l vs ∗ ⌜ n_set_bits vs = 0%R ⌝ ∗ ⌜ n = length vs ⌝ }}}.
Proof.
    iIntros (??) "_ H". unfold init_counter. wp_pures.
    iApply (wp_array_init (λ _ v, ⌜ v = #false ⌝ )%I _ _ n ); first lia.
    - iApply big_sepL_intro. iModIntro.
    iIntros (?? _). by wp_pures.
    - iNext. iIntros (l vs) "[% [H1 H2]]".
    iApply "H". 
    clear H.
    iExists (repeat #false n). iSplit; last first.
        { clear. iPureIntro. induction n; simpl; auto.
        destruct IHn. split; auto. }
    iAssert (⌜ vs = repeat #false n ⌝ )%I as "->"; auto.
     + iClear "H1".
        iInduction n as [|n] "IH" forall (vs H0); simpl.
        * destruct vs; try by contradict H0. done.
        * destruct vs; try by contradict H0. simpl.
          iDestruct "H2" as "[-> H]".
          iPoseProof ("IH" $! vs with "[] H") as "->"; auto.
        iPureIntro. injection H0. intros. lia.
     + iFrame. iPureIntro. clear. induction n; simpl; first done.
     apply Forall_cons. split; auto. by eexists.
Qed.

Lemma incr_len l vs:
    is_counter l vs -∗ ⌜ length vs = length (incr vs) ⌝ .
Proof.
    iIntros "[_ %Hcnt]". iPureIntro.
    induction vs; auto. simpl. apply Forall_cons in Hcnt.
    destruct Hcnt as [[[] ->] ?]; auto.
    rewrite IHvs; auto. 
Qed.

Lemma m_increments (n m:nat) p:
    (0 < n) → 
    (p > 0)%R → 
    (p < 1)%R → 
    ⊢ $((INR m)*(2/p))%R WP 
     (let: "l" := init_counter #n in 
     (rec: "incr_m" "m" :=
        if: "m" = #0 then #()
        else  incr_counter #n "l" #p;; "incr_m" ("m" - #1))
      #m
    )
    {{v, True, R0}}.
Proof.
    intros.
    assert (1/p > 0)%R by (apply Rdiv_pos_pos; lra).
    assert (0 <= INR m)%R by apply pos_INR.
    assert (INR m * (2 / p) >= 0)%R. {nra. } 
    wp_bind (init_counter _).
    replace (INR m * (2 / p))%R with (0%R + INR m * (2 / p))%R by lra.
    wp_apply (wp_pot_add).
    iApply ((init_counter_spec n) with "[//]"); auto.
    iNext. iIntros (l) "[%vs [Hcnt [%Hsetb %Hlen]]]".
    wp_let. wp_pure.
    replace 0%R with ((1/p) * 0)%R by lra.
    rewrite <- Hsetb. clear Hsetb H3 .
    iInduction m as [|m] "IH" forall (vs n H Hlen ). 
    - assert (1 / p * n_set_bits vs + INR 0 * (2 / p) >= 0)%R.
    {simpl. specialize (n_set_bits_nonneg vs). nra. }
     by wp_pures.
    - rewrite S_INR.
     assert (1 / p * n_set_bits vs + (INR m + 1) * (2 / p) >= 0)%R.
     { simpl. specialize (n_set_bits_nonneg vs) as ?.
      enough (0 <= INR m)%R by nra. by apply pos_INR. }
     wp_pures. wp_bind
     (λ _, (1/p) * (n_set_bits (incr vs)) + INR m * (2 / p))%R
     (incr_counter _ _ _).
    replace ((INR m + 1) * (2/p))%R with (2/p + INR m * (2/p))%R by lra.
    rewrite <- Rplus_assoc.
    assert ((INR m * (2 / p) >= 0))%R. {assert (0 <= INR m)%R by apply pos_INR. nra. }
    wp_apply (wp_pot_add).
    rewrite Rplus_comm.
    iPoseProof (incr_len with "Hcnt") as "%".
    iApply ((incr_counter_spec n l p vs) with "Hcnt"); auto.
    iSplit; auto.
    iIntros "!> Hcnt".
    assert (1 / p * n_set_bits (incr vs) + INR m * (2 / p) >= 0)%R.
    { specialize (n_set_bits_nonneg (incr vs)). nra. }
    wp_pure. wp_pure. wp_op.
    replace (Z.of_nat (S m) - 1)%Z with (Z.of_nat m) by lia. 
    iApply "IH"; auto. by simplify_eq.
Qed.

End proofs.